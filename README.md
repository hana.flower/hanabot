# ⚠️ This version is deprecated ⚠️
This version of Hanabot is being deprecated in favour of [a rewrite written in Rust](https://gitlab.com/hana.flower/hanabot-rust). When that version is ready, this one will be renamed to Hanabot Legacy.

This version will not be updated or maintained any more in order to shift focus fully to the Rust version, so any unfinished things or bugs will be left in.

Previous README follows.

# Hanabot
The best utility bot, primarily for NSFW Discord servers.

The library for interacting with the API, or the bot's features, work for a general public bot. Feel free to self-host or make your own public instance.

Profile pic and name chosen by my friend Sophie, who also pays for bot hosting <3

[You can view more documentation here.](https://gitlab.com/hana.flower/hanabot/-/wikis/home)

## How to run
[Check here!](https://gitlab.com/hana.flower/hanabot/-/wikis/Self-Hosting)

## Features
[Check here!](https://gitlab.com/hana.flower/hanabot/-/wikis/About)

## Terms of service
Feel free to use any of this, but check [LICENSE](LICENSE) I guess.

Put a link to the bot's source code in the bot's description if you use it.

For privacy info, see [here](https://gitlab.com/hana.flower/hanabot/-/wikis/Privacy).

*Originally made by 花 <3#0676 (hana) with love and passion <3*