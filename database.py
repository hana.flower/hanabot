# i still don't think this is how you use couchdb but whatever
import couchdb
import asyncio
from config import *
from time import time
from math import floor
import cProfile
import update_db_docs

from api import claims as claims_api
from api import claims_waitlist as claims_waitlist_api
from api import guilds as guilds_api
from api import members as members_api

# connect to the CouchDB server
server = couchdb.Server(Credentials.COUCHDB_CREDS.URL)
server.resource.credentials = (Credentials.COUCHDB_CREDS.USERNAME, Credentials.COUCHDB_CREDS.PASSWORD)

# get any document
def get_doc(database, mango_query, retry_count=0, max_retries=5, skip_version_check=False):
    print("Getting doc")
    print(mango_query)
    db = server[database]
    try:
        results = list(db.find(mango_query))

        if results:
            if skip_version_check == False:
                for doc in results:
                    version = doc.get("version", None)

                    if version == None:
                        continue

                    db_version = getattr(DBVersions, database, 1) # this checks the version number the document should be at
                    if version < db_version:
                        print(f"Need to update {database}")
                        # logic for updating the doc here
                        update_function = getattr(update_db_docs, f'update_{database}_version')
                        updated_doc = update_function(doc)
                        # Retry by calling the same function with an incremented retry_count
                        if retry_count < max_retries:
                            return get_doc(database, mango_query, retry_count=retry_count + 1)
                        else:
                            print("Max retries reached")
                            return "Max retries reached"
            return results
        else:
            print("No documents found")
            return "No documents found"
    except couchdb.http.ResourceNotFound:
        print("No documents found")
        return "No documents found" # to-do: make this return an empty list instead and change the checks to account for that

# insert any doc
def insert_doc(database, values):
    db = server[database]
    doc_id = db.save(values)[0]
    return db.get(doc_id)

# update any document
def update_doc(database, mango_query, update_dict, retry_count=0, max_retries=5):
    db = server[database]
    # force the query limit to 1
    mango_query["limit"] = 1

    try:
        results = list(db.find(mango_query))

        if results:
            doc = results[0] # we only want to update one document at a time

            # we still need to update the doc version before updating its values
            # so this block of code is for updating the document version
            version = doc.get('version', None) # every document SHOULD have a version number so no error checking here. if it doesn't then you probably screwed up tbh (or you're getting a claim from a REALLY old version)
            db_version = getattr(DBVersions, database, 1) # this checks the version number the document should be at
            if version < db_version:
                print(f"Need to update {database}")
                # logic for updating the doc here
                update_function = getattr(update_db_docs, f'update_{database}_version')
                update_function(doc)
                # Retry by calling the same function with an incremented retry_count
                if retry_count < max_retries:
                    return update_doc(database, mango_query, update_dict, retry_count=retry_count + 1)
                else:
                    print("Max retries reached")
                    return "Max retries reached"

            # update the doc values
            for key, value in update_dict.items():
                nested_keys = key.split('.')
                current_dict = doc
                for nested_key in nested_keys[:-1]:
                    # Navigate through the nested structure
                    if nested_key not in current_dict:
                        print(f"Key '{nested_key}' not found in the document")
                        break  # Handle the case where the nested key is not present
                    current_dict = current_dict[nested_key]

                # Update the nested key if it exists
                if nested_keys[-1] in current_dict:
                    current_dict[nested_keys[-1]] = value
                else:
                    print(f"Key '{nested_keys[-1]}' not found in the document")
            db[doc["_id"]] = doc  # Save the updated document
            return doc  # Return the updated dictionary

        else:
            print("No documents found")
            return "No documents found"
    except couchdb.http.ResourceNotFound:
        print("No documents found")
        return "No documents found"

# delete any document
def delete_doc(database, mango_query):
    deleted_documents = []
    db = server[database]

    try:
        results = db.find(mango_query)
        deleted_count = 0

        for result in results:
            db.delete(result)
            deleted_documents.append(result)

        if deleted_documents:
            print(deleted_documents)
            return deleted_documents
        else:
            return "No documents found"
    except couchdb.http.ResourceNotFound:
        return "No documents found"


# onboarding I probably won't bother with an API for
# init onboarding
def init_onboarding(guild_id):
    db = server["makeshift_onboarding"]
    values = {
        "guild_id": guild_id,
        "version": DBVersions.makeshift_onboarding,
        "questions": {}
    }
    db.save(values)

# get onboarding
def get_onboarding(guild_id):
    print(f"Getting guild onboarding. guild_id: {guild_id}")
    db = server["makeshift_onboarding"]
    try:
        mango_query = {
            "selector": {
                "guild_id": guild_id
            },
            "limit": 1
        }

        results = list(db.find(mango_query))

        # Check if any documents were found
        if results:
            onboarding_object = results[0]
            version = onboarding_object.get('version', None)  # Get the "version" key from the claim_object
            if version < DBVersions.makeshift_onboarding:
                print("The onboarding needs to be updated")
                update_db_docs.update_guilds_version(onboarding_object)
                return "Retry"
        else:
            #print(f"The guild {guild_id} is not registered, time to do that")
            return "Guild onboarding not found"

        return onboarding_object

    except couchdb.http.ResourceNotFound:
        #print(f"The guild {guild_id} is not registered, time to do that")
        return "Guild onboarding not found"

# update onboarding
def update_onboarding(guild_id, update_dict):
    db = server["makeshift_onboarding"]
    try:
        mango_query = {
            "selector": {
                "guild_id": str(guild_id)
            },
            "limit": 1
        }

        results = list(db.find(mango_query))

        if results:
            onboarding_object = results[0]
            version = onboarding_object.get("version", None)  # Get the "version" key from the claim_object
            if version < DBVersions.makeshift_onboarding:
                print("The onboarding needs to be updated")
                update_db_docs.update_guilds_version(onboarding_object)
                return "Retry"
            for key, value in update_dict.items():
                if key in onboarding_object:
                    onboarding_object[key] = value
                else:
                    print(f"Key '{key}' not found in the document")
            db[onboarding_object["_id"]] = onboarding_object  # Save the updated document
        else:
            print("Guild onboarding not registered")
            return "Retry"
    except couchdb.http.ResourceNotFound:
        print("Guild onboarding not registered")
        return "Retry"
    return onboarding_object


# keeping the connection alive
# this may not be necessary. only enable this if queries can sometimes be much slower than they should be.
async def ping_server():
    while True:
        print("Pinging the couchdb server...")
        # some random lightweight operation
        # it gets the document for The Lab
        # to-do: change this because leaking couchDB IDs is a terrible fucking idea lmfao but this is the document for my test server so who cares
        # although I do want to do it with the library
        db = server["guilds"]
        doc = db.get("345b19e3f425ec7fc7d51761be00fdd0")
        print("Pinged the couchdb server.")
        await asyncio.sleep(60)

# add doc to delete queue
def schedule_doc_for_deletion(doc_id, database, reason, data={}):
    current_time = int(time()) # in seconds

    # change delete at time based on the doc type
    if database == "claiming":
        delete_at_time = current_time + 2592000 # 30 days in seconds
    elif database == "claims_waitlist":
        delete_at_time = current_time + 86400 # 1 day in seconds
    elif database == "guilds":
        delete_at_time = current_time + 2592000 # 30 days in seconds
    elif database == "members":
        delete_at_time = current_time + 2592000 # 30 days in seconds
    elif database == "cross_verify_requests":
        delete_at_time = current_time + 86400 # 1 day in seconds

    # data is a dict with some metadata, for example guild_id
    # that way we can cancel the deletion process if we need

    # reason values are not implemented yet
    # I don't really know how important it is to do or where I would use it
    # 0 - Unknown
    # 1 - Inactive
    # 2 - Requested deletion
    # 3 - Member left guild
    # 4 - Expires at the time it's deleted
    # is this how you do enum values

    values = {
        "doc_id": doc_id,
        "database": database,
        "reason": reason,
        "data": data,
        "queued_at": current_time,
        "delete_at": delete_at_time
    }

    insert_doc("delete_queue", values)

# opposite of above
# can also delete multiple docs at once
async def unschedule_doc_for_deletion(doc_id=None, data=None):
    if doc_id != None:
        mango_query = {
            "selector": {
                "doc_id": doc_id
            }
        }
    elif data != None:
        mango_query = {
            "selector": {
                "data": data
            }
        }
    else:
        return "No valid info provided for what to unschedule"

    # I think couchdb has a bulk delete endpoint so maybe use that later idk
    docs = get_doc("delete_queue", mango_query, skip_version_check=True)

    for doc in docs:
        delete_doc("delete_queue", {"selector": {"_id": doc["_id"]}})

# delete queue
async def delete_queue_task():
    print("Delete queue task started")
    db = server["delete_queue"]

    while True:
        await asyncio.sleep(60)

        # check every doc in the DB
        for doc_id in db:
            doc = db[doc_id]

            doc_id_to_delete = doc["doc_id"]

            if doc["delete_at"] < int(time()):
                # claims
                if doc["database"] == "claiming":
                    await claims_api.delete_claim(couchdb_id=doc_id_to_delete)

                elif doc["database"] == "claims_waitlist":
                    await claims_waitlist_api.delete_waitlist_claim_by_couchdb_id(doc_id_to_delete)

                elif doc["database"] == "guilds":
                    await guilds_api.delete_guild_by_couchdb_id(doc_id_to_delete)

                elif doc["database"] == "members":
                    await members_api.delete_member(couchdb_id=doc_id_to_delete)

                db.delete(doc)
