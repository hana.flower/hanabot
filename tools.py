import requests
import json
from config import *
import urllib.parse
from requests_toolbelt import MultipartEncoder

# format message
def format_message(json_object, string_to_replace, what_to_replace_with):
    json_str = json.dumps(json_object)  # Convert the JSON object to a string
    replaced_json_str = json_str.replace(str(string_to_replace), str(what_to_replace_with))
    replaced_json_object = json.loads(replaced_json_str)  # Convert the modified string back to a dictionary
    return replaced_json_object

# percent encoding
def percent_encode(original_string):
    encoded_string = urllib.parse.quote(original_string)
    return encoded_string

# permissions integer to permissions
def check_permission(permissions_integer, bit_to_check):
    permissions_integer = int(permissions_integer)
    permissions_binary = bin(permissions_integer)[2:] # remove 0b prefix
    permissions_binary = permissions_binary.zfill(53) # the discord permission bitfield is 53 bits long, so fill the binary number to match that
    is_set = permissions_binary[-(bit_to_check)] == '1' # the bit will be 1 if it is set
    return is_set

# check if a user is a bot owner from the raw data
async def is_bot_owner(data):
    if "member" in data.keys():
        user_id = data["member"]["user"]["id"]
    else:
        user_id = data["user"]["id"]
    return user_id in BotSettings.owner_ids  

# thanks chatgpt
async def integer_to_bitwise_flags(integer):
    flags = []
    bit_position = 0

    while integer > 0:
        if integer & 1:
            flags.append(2**bit_position)

        integer >>= 1
        bit_position += 1

    return flags

# get flags from flags integer
async def get_flags(integer, enum_class):
    flags_int = await integer_to_bitwise_flags(integer)
    flags = []
    
    for flag in flags_int:
        for member in enum_class:
            if member.value == flag:
                flags.append(member)
                continue

    return flags

# encode multipart
def encode_multipart(message_data):
    # json data
    fields = {
        'payload_json': (None, json.dumps(message_data), 'application/json')
    }

    # attachments
    for attachment in message_data["attachments"]:
        attachment_id = str(attachment["id"])
        filename = attachment["filename"]
        fields.update({f"files[{attachment_id}]": (filename, open(filename, "rb"), "image/png")}) # cry about it if it's not a png image xd

    m = MultipartEncoder(fields, boundary = "boundary")

    return m

# format options
async def format_options(options):
    # get the options
    print(f"Options are {options}")
    subcommand_name = None
    subcommand_group_name = None
    options_dict = {}
    # format the options into an easier format to work with
    for option in options:
        print(f"Checking an option {option}")
        if option["type"] == 1: # subcommand
            subcommand_name = option["name"]
            print(f"Subcommand name is {subcommand_name}")
            
            print("Checking subcommand options")
            subcommand_options = await format_options(option["options"])
            #print(subcommand_options)
            options_dict = subcommand_options[0]

        elif option["type"] == 2: # subcommand group
            subcommand_group_name = option["name"]
            print(f"Subcommand group name is {subcommand_group_name}")
            
            # call this function again to get the group options
            subcommand_group_options = await format_options(option["options"])
            options_dict = subcommand_group_options[0]
            subcommand_name = option["options"][0]["name"]
            print(f"Subcommand name is {subcommand_name}")

        elif option["type"] in [3, 4, 6]: # string, integer or user
            options_dict[option["name"]] = option["value"]

        #print(f"Options dict {options_dict}")

    print(f"Final return value for options dict: {options_dict}")
    print(f"Final return value for subcommand name: {subcommand_name}")
    print(f"Final return value for subcommand group name: {subcommand_group_name}")
    return options_dict, subcommand_name, subcommand_group_name

# format username to remove formatting characters like _
async def format_username(user):
    username = user["username"]

    # repeat for any characters that need to be escaped
    username = username.replace("_", "\_")
    username = username.replace("*", "\*")

    formatted_username = f"{username}"
    return formatted_username

# for logging mainly
async def format_command(command, formatted_options):
    print(formatted_options)
    command_name = command["name"]

    formatted_command = f"/{command_name}"

    if formatted_options != None:
        options = formatted_options[0]
        subcommand_group_name = formatted_options[2] # same comment as below
        subcommand_name = formatted_options[1] # will be None if no subcommand is present

        if subcommand_group_name: formatted_command += f" {subcommand_group_name}"
        if subcommand_name: formatted_command += f" {subcommand_name}"
        
        if options != {}:
            options_list = []
            for option in options.keys():
                option_value = options[option]
                formatted_option = f" {option}: {option_value}"
                formatted_command += (formatted_option)

    return formatted_command
