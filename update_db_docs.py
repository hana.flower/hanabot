# credit to chatGPT lmao
import couchdb
from config import *
import sys

# connect to the CouchDB server
server = couchdb.Server(Credentials.COUCHDB_CREDS.URL)
server.resource.credentials = (Credentials.COUCHDB_CREDS.USERNAME, Credentials.COUCHDB_CREDS.PASSWORD)

# this system needs some updating to be more robust and less error-prone, but that's a problem for future me
# i should probably use doc.get("features") for example

# members
def update_members_version(doc):
    update_keys = {} # dictionary of keys to insert
    final_version = "0" # the final version

    # now we repeat this for every update, which should give us an up to date member
    if "warns" not in doc:
        update_keys["warns"] = {"count": 0, "list": []}
        # this is here to avoid a keyerror when updating v1 members
        # to-do: find a better way to avoid that
        doc["warns"] = {"count": 0, "list": []}
        final_version = "2"
    
    if "count" in doc["warns"]:
        update_keys["warns"] = doc["warns"]["list"]
        update_keys["claimed_ids"] = doc["claims"]["claimed_ids"]
        final_version = "3"

    if "verification_status" not in doc:
        update_keys["verification_status"] = {}
        final_version = "4"

    return update_specific_doc_with_keys("members", final_version, update_keys, doc["_id"])

# guilds
def update_guilds_version(doc):
    update_keys = {} # dictionary of keys to insert
    final_version = "0" # the final version

    # now we repeat this for every update, which should give us an up to date guild
    if "warn_list" not in doc["important_channel_ids"]:
        update_keys = {"important_channel_ids": {"warn_list": "0"}}
        final_version = "2"

    if "features" not in doc:
        # default, hardcoded values before this system
        update_keys["features"] = { 
            "claiming": True,
            "accountagecheck": True,
            "welcoming": True,
            "banlist": True
        }
        update_keys["min_acc_age"] = 24
        update_keys["reinvite_url"] = ""
        update_keys["warn_limit"] = 3
        final_version = "3"

    if "claim_limit" not in doc:
        update_keys["claim_limit"] = 3
        final_version = "4"

    if "member_age_broadcast" not in doc["important_channel_ids"]:
        update_keys = {"important_channel_ids": {"member_age_broadcast": "0"}}
        final_version = "5"

    if "memberagecheck" not in doc["features"]: # only need to check one thing this time
        if "features" not in update_keys:
            update_keys["features"] = {}
        update_keys["features"].update({"memberagecheck": False, "onboarding": False})
        update_keys["rules_buttons_swapped"] = False
        update_keys["rules_message_link"] = ""
        final_version = "6"

    if "crossverify" not in doc["features"]: # only need to check one thing this time
        if "features" not in update_keys:
            update_keys["features"] = {}
        if "important_channel_ids" not in update_keys:
            update_keys["important_channel_ids"] = {}
        if "important_role_ids" not in update_keys:
            update_keys["important_role_ids"] = {}
        update_keys["features"].update({"crossverify": False})
        update_keys["important_channel_ids"].update({"cross_verify_log": "0"}),
        update_keys["important_role_ids"].update({"major_verified": "0"}),
        update_keys["cross_verify_guilds"] = []

        final_version = "7"
    
    # insert everything into the document by couchDB ID
    return update_specific_doc_with_keys("guilds", final_version, update_keys, doc["_id"])

# claiming
# I hate how the db name is claiming and not claims but whatever it'll stick for now
def update_claiming_version(doc, ID="-1"):
    update_keys = {} # dictionary of keys to insert
    final_version = "0" # the final version

    if "users" not in doc:
        # default, hardcoded values before this system
        update_keys["id"] = ID # the function may pass an ID to insert if not already done
        update_keys["users"] = {
            "master": doc.get("master_id"),
            "claimed": doc.get("claimed_id")
        }
        update_keys["note"] = ""
        final_version = "2"

    # insert everything into the document by couchDB ID
    return update_specific_doc_with_keys("claiming", final_version, update_keys, doc["_id"])

# waitlist
def update_claims_watlist_version(doc):
    update_keys = {} # dictionary of keys to insert
    final_version = "0" # the final version

    if "users" not in doc:
        update_keys["users"] = {
            "master": doc.get("master_id"),
            "claimed": doc.get("claimed_id")
        }
        final_version = "3"

    # insert everything into the document by couchDB ID
    return update_specific_doc_with_keys("claims_waitlist", final_version, update_keys, doc["_id"])

# update a specific document by couchhdb ID
def update_specific_doc_with_keys(doc_type, final_version, key_value_pairs, doc_id):
    # get the document from the db
    db = server[doc_type]

    doc = db[doc_id]

    # actually update the document
    for key, value in key_value_pairs.items():
        if isinstance(value, dict):
            doc[key] = {**doc.get(key, {}), **value}
        else:
            doc[key] = value

    # clean up unneeded keys
    cleanup_doc(doc, doc_type, final_version)

    # update the version number
    doc["version"] = final_version

    db[doc_id] = doc

    return doc

# update an entire database
def update_entire_db(doc_type):
    db = server[doc_type]

    for doc_id in db:
        doc = db[doc_id]
        update_function = getattr(sys.modules[__name__], f'update_{doc_type}_version')
        updated_doc = update_function(doc)

# remove unneeded keys
def cleanup_doc(doc, doc_type, final_version):
    # it's stored as a string in the DB for compatibility or something
    # doesn't need to be an int basically, it's just an arbitrary counter
    final_version = int(final_version)

    print(final_version)

    try:
        if doc_type == "claiming":
            if final_version >= 2:
                doc.pop("claimed_id")
                doc.pop("master_id")

        elif doc_type == "members":
            if final_version >= 3:
                doc.pop("claims") # the warns dict gets replaced, claims turns into claimed_ids

        elif doc_type == "claims_waitlist":
            if final_version >= 3:
                doc.pop("claimed_id")
                doc.pop("master_id")
    except:
        pass # nothing needs to be done because the doc is clean already

    return doc

#update_entire_db("claiming")

'''
# Example usage:
update_keys = {
    'important_channel_ids': {'warn_list': '0'},
}

update_docs_with_keys('guilds', 2, update_keys)
'''

# fucking chatgpt, this code does not work
'''
# update documents to a newer version
def update_docs_with_keys(doc_type, version, key_value_pairs):
    version = str(version)
    db = server[doc_type]

    for doc_id in db:
        doc = db[doc_id]

        if doc.get('version', 0) < version:
            important_channel_ids = doc.setdefault('important_channel_ids', {})
            important_channel_ids['warn_list'] = '0'

            # Update the "version" key
            doc['version'] = str(version)

            # Save the updated document back to the database
            db.save(doc)

            # Print a message indicating the update
            guild_id = doc.get('guild_id', 'Unknown')
            print(f"Updated document with guild_id {guild_id} to version {version}.")
'''
