# custom libraries
import database
import tools
from config import *
import http_api
from logger import *

# not custom libraries
import asyncio
import json
from math import floor
from time import time

# classes
class Channels:
    def __init__(self, channels):
        self.ban_list = channels["ban_list"]
        self.welcome = channels["welcome"]
        self.warn_list = channels["warn_list"]
        self.claim_list = channels["claim_list"]
        self.claiming = channels["claiming"]
        self.member_age_broadcast = channels["member_age_broadcast"]
        self.cross_verify_log = channels["cross_verify_log"]

    def to_dict(self):
        return {
            "ban_list": self.ban_list,
            "welcome": self.welcome,
            "warn_list": self.warn_list,
            "claim_list": self.claim_list,
            "claiming": self.claiming,
            "member_age_broadcast": self.member_age_broadcast,
            "cross_verify_log": self.cross_verify_log
        }
    
class Roles:
    def __init__(self, roles):
        self.claimed = roles["claimed"]
        self.master = roles["master"]
        self.member = roles["member"]
        self.major_verified = roles["major_verified"]

    def to_dict(self):
        return {
            "claimed": self.claimed,
            "master": self.master,
            "member": self.member,
            "major_verified": self.major_verified
        }

class Messages:
    def __init__(self, messages):
        self.welcome = messages["welcome"]

    def to_dict(self):
        return {
            "welcome": self.welcome
        }

class Features:
    def __init__(self, features):
        self.claiming = features["claiming"]
        self.accountagecheck = features["accountagecheck"]
        self.welcoming = features["welcoming"]
        self.banlist = features["banlist"]
        self.memberagecheck = features["memberagecheck"]
        self.onboarding = features["onboarding"]
        self.crossverify = features["crossverify"]

    def to_dict(self):
        return {
            "claiming": self.claiming,
            "accountagecheck": self.accountagecheck,
            "welcoming": self.welcoming,
            "banlist": self.banlist,
            "memberagecheck": self.memberagecheck,
            "onboarding": self.onboarding,
            "crossverify": self.crossverify
        }

class Guild:
    def __init__(self, guild_info):
        self.version = guild_info["version"]
        self.guild_id = guild_info["guild_id"]

        self.channels = Channels(guild_info["important_channel_ids"])
        self.roles = Roles(guild_info["important_role_ids"])
        self.messages = Messages(guild_info["messages"])
        self.features = Features(guild_info["features"])

        self.min_acc_age = guild_info["min_acc_age"]
        self.reinvite_url = guild_info["reinvite_url"]
        self.warn_limit = guild_info["warn_limit"]
        self.claim_limit = guild_info["claim_limit"]
        self.rules_buttons_swapped = guild_info["rules_buttons_swapped"]
        self.rules_message_link = guild_info["rules_message_link"]
        self.cross_verify_guilds = guild_info["cross_verify_guilds"]

    def to_dict(self):
        return {
            "version": self.version,
            "guild_id": self.guild_id,
            "important_channel_ids": self.channels.to_dict(),
            "important_role_ids": self.roles.to_dict(),
            "messages": self.messages.to_dict(),
            "features": self.features.to_dict(),
            "min_acc_age": self.min_acc_age,
            "reinvite_url": self.reinvite_url,
            "warn_limit": self.warn_limit,
            "claim_limit": self.claim_limit,
            "rules_buttons_swapped": self.rules_buttons_swapped,
            "rules_message_link": self.rules_message_link,
            "cross_verify_guilds": self.cross_verify_guilds
        }

    # update self
    async def update(self, update_dict):
        for key, value in update_dict.items():
            setattr(self, key, value)

        await update_guild_cache(self.guild_id, guild_info=self.to_dict(), logger=Logger(self.guild_id))
        database.update_doc("guilds", {"selector": {"guild_id": self.guild_id}}, update_dict)
        return self

# get a guild
async def get_guild(guild_id, create_if_not_found=True, return_doc=False):
    mango_query = {
        "selector": {
            "guild_id": guild_id
        },
        "limit": 1
    }
    guild_json = database.get_doc("guilds", mango_query)
        
    if guild_json == "No documents found": # register the new guild
        # if needed
        if create_if_not_found == False:
            return None
            
        return await init_guild(guild_id)

    guild_json = json.dumps(guild_json[0])

    # Function to create a Guild instance from JSON data
    # I don't understand this, thanks chatgpt
    data_dict = json.loads(guild_json)

    if return_doc == True: return data_dict # return the raw document's JSON data

    return Guild(data_dict)

# initialise guild
async def init_guild(guild_id):
    # default values are empty
    values = {
        "guild_id": guild_id,
        "version": DBVersions.guilds,
        "important_channel_ids": {
            "claiming": "0",
            "claim_list": "0",
            "ban_list": "0",
            "welcome": "0",
            "warn_list": "0",
            "member_age_broadcast": "0",
            "cross_verify_log": "0"
        },
        "important_role_ids": {
            "claimed": "0",
            "master": "0",
            "member": "0",
            "major_verified": "0"
        },
        "messages": {
            "welcome": {}
        },
        "features": { # disable all by default
            "claiming": False,
            "accountagecheck": False,
            "welcoming": False,
            "banlist": False,
            "memberagecheck": False,
            "onboarding": False,
            "crossverify": False
        },
        "min_acc_age": 24,
        "reinvite_url": "",
        "warn_limit": 3,
        "claim_limit": 3,
        "rules_buttons_swapped": False,
        "rules_message_link": "",
        "cross_verify_guilds": []
    }
    database.insert_doc("guilds", values)
    await update_guild_cache(guild_id, guild_info=values, logger=Logger(guild_id))

    return Guild(values)

# update guild without the object
async def update_guild(guild_id, update_dict):
    guild = database.update_doc("guilds", {"selector": {"guild_id": guild_id}, "limit": 1}, update_dict)
    await update_guild_cache(guild_id, guild_info=guild, logger=Logger(guild_id))
    
    # return a guild object
    return Guild(guild)

# delete a guild
async def delete_guild(guild_id):
    mango_query = {
        "selector": {
            "guild_id": guild_id
        },
        "limit": 1
    }

    database.delete_doc("guilds", mango_query)
    
    return "Guild deleted."

# delete guild by couchdb id
async def delete_guild_by_couchdb_id(couchdb_id):
    guild_object = database.delete_doc("guilds", {"selector": {"_id": couchdb_id}, "limit": 1})

    if guild_object == "No documents found":
        return "Guild does not exist."

    return "Guild deleted."

# guild cache
# note that this currently doesn't work for sharding
# I will figure that out later, after cross verifying is complete and after sharding is done properly

guild_cache = []

# guild cache object
class CachedGuild:
    # I have never heard of inheritance, clearly
    def __init__(self, guild_id, name, icon, guild_info=None, onboarding=None):
        self.guild_id = guild_id
        self.name = name
        self.icon = icon
        # None will just be the default values if nothing is passed in
        self.hanabot = None
        self.onboarding = None # this is not an object and it has no API and it probably never will lmao
        if guild_info != None:
            self.hanabot = Guild(guild_info)
    
    def to_dict(self):
        return {
            "guild_id": self.guild_id,
            "name": self.name,
            "icon": self.icon,
            "hanabot": self.hanabot.to_dict(),
            "onboarding": self.onboarding
        }

# get a guild from cache
async def get_guild_from_cache(guild_id):
    for guild in guild_cache:
        if guild.guild_id == guild_id:
            return guild
    return None

# update cache
async def update_guild_cache(guild_id, name=None, icon=None, guild_info=None, onboarding=None, logger=None):
    asyncio.create_task(log(logger, ContextType.GUILD, "Updating guild cache...", HandlerType.API_GUILDS))
    if guild_info == None:
        guild_info = await get_guild(guild_id, create_if_not_found=False, return_doc=True)
        pass

    # empty cache
    '''
    if guild_cache == []:
        guild_cache.append(new_guild)
        return new_guild
    '''

    guild_found = False

    for i, guild in enumerate(guild_cache):
        # update an existing guild
        if guild.guild_id == guild_id:
            guild_found = True
            break

    if guild_found == True:
        asyncio.create_task(log(logger, ContextType.GUILD, "The guild exists in the cache already and is being updated.", HandlerType.API_GUILDS))
        if name == None: name = guild_cache[i].name
        if icon == None: icon = guild_cache[i].icon

        new_guild = CachedGuild(guild_id, name, icon, guild_info, onboarding)
        guild_cache[i] = new_guild
    else:
        asyncio.create_task(log(logger, ContextType.GUILD, "The guild does not exist in the cache and is being added to the cache.", HandlerType.API_GUILDS))
        new_guild = CachedGuild(guild_id, name, icon, guild_info, onboarding)
        guild_cache.append(new_guild)
    return new_guild
