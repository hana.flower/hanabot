# custom libraries
import database
import tools
from config import *
import http_api

from api.cross_verify import *
from api import claims as claims_api

# not custom libraries
import json
from math import floor
from time import time

# classes
class Warn:
    def __init__(self, reason, timestamp, message_id, warn_id):
        self.reason = reason
        self.timestamp = timestamp
        self.message_id = message_id
        self.warn_id = warn_id

    def to_dict(self):
        return {
            "reason": self.reason,
            "timestamp": self.timestamp,
            "message_id": self.message_id,
            "warn_id": self.warn_id
        }

class Member:
    def __init__(self, version, user_id, guild_id, claimed_ids, warns, verification_status):
        self.version = version
        self.user_id = user_id
        self.guild_id = guild_id
        self.claimed_ids = claimed_ids
        self.warns = [Warn(**warn_data) for warn_data in warns]
        if verification_status != {}:
            self.verification_status = VerificationStatus(user_id, guild_id, verification_status["timestamp"], verification_status["flags"])
        else:
            self.verification_status = None

    def to_dict(self):
        return {
            "version": self.version,
            "user_id": self.user_id,
            "guild_id": self.guild_id,
            "claimed_ids": self.claimed_ids,
            "warns": self.warns_to_dict_list(),
            "verification_status": self.verification_status
        }

    def warns_to_dict_list(self):
        warns = []
        for warn in self.warns:
            warns.append(warn.to_dict())
        return warns

    # update self
    def update(self, update_dict):
        for key, value in update_dict.items():
            setattr(self, key, value)

        database.update_doc("members", {"selector": {"user_id": self.user_id, "guild_id": self.guild_id}}, update_dict)
        return self

# get a member
async def get_member(user_id, guild_id, return_doc=False, create_if_not_found=True):
    mango_query = {
        "selector": {
            "user_id": user_id,
            "guild_id": guild_id
        },
        "limit": 1
    }
    member_json = database.get_doc("members", mango_query)
        
    if member_json == "No documents found": # register the new member
        # if needed
        if create_if_not_found == False:
            return None

        member_json = [await create_member(user_id, guild_id, return_doc=True)] # hacky workaround

    member_json = member_json[0]

    print(member_json)
    member_json = json.dumps(member_json)

    # Function to create a Member instance from JSON data
    # I don't understand this, thanks chatgpt
    data_dict = json.loads(member_json)
    
    if return_doc == True: return data_dict # return the raw document's JSON data

    return Member(data_dict["version"], data_dict["user_id"], data_dict["guild_id"], data_dict["claimed_ids"], data_dict["warns"], data_dict["verification_status"])

# initialise guild member
async def create_member(user_id, guild_id, claimed_ids=[], warns=[], verification_status={}, return_doc=False):
    # default values are empty
    values = {
        "user_id": user_id,
        "version": DBVersions.members,
        "guild_id": guild_id,
        "claimed_ids": claimed_ids, 
        "warns": warns,
        "verification_status": verification_status
    }
    member_doc = database.insert_doc("members", values)

    print(member_doc)
    member_json = json.dumps(member_doc)
    
    if return_doc == True:
        return json.loads(member_json)
    else:
        return Member(DBVersions.members, user_id, guild_id, claimed_ids, warns, verification_status)

# update guild member without the object
async def update_member(user_id, guild_id, update_dict, create_if_not_found=True):
    member = database.update_doc("members", {"selector": {"user_id": user_id, "guild_id": guild_id}, "limit": 1}, update_dict)

    if member == "No documents found": # register the new member
        # if needed
        if create_if_not_found == False:
            return None

        member = await create_member(user_id, guild_id, return_doc=True)
    
    # return a member object
    return Member(member["version"], member["user_id"], member["guild_id"], member["claimed_ids"], member["warns"], member["verification_status"])

# delete a member
async def delete_member(user_id=None, guild_id=None, couchdb_id=None):
    if couchdb_id == None:
        mango_query = {
            "selector": {
                "user_id": user_id,
                "guild_id": guild_id
            },
            "limit": 1
        }
    else:
        mango_query = {"selector": {"_id": couchdb_id}, "limit": 1}

    member = get_member(user_id, guild_id, return_doc=False, create_if_not_found=False)

    if member == "No documents found":
        return "Member does not exist."

    database.delete_doc("members", mango_query)

    # to-do: get claim relationships, and then delete those docs
    await unverify_member(guild_id, user_id, reason="Member is no longer in guild.")
    
    return "Member deleted."
