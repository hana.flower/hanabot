# custom libraries
import database
import tools
from config import *
import http_api
from logger import *

from api import guilds as guilds_api
from api import members as members_api

# not custom libraries
import json
import asyncio
from math import floor
from time import time

# classes
class Users:
    def __init__(self, master_id, claimed_id):
        self.master = master_id
        self.claimed = claimed_id

    def to_dict(self):
        return {
            "claimed": self.claimed,
            "master": self.master
        }

    def get_user(self, user_id):
        if user_id == self.master:
            return "master"
        elif user_id == self.claimed:
            return "claimed"
        else:
            return None  # Return None if the user is not found in the class

class Claim:
    def __init__(self, version, interaction_id, master_id, claimed_id, guild_id, message_id, timestamp, note):
        self.version = version
        self.id = interaction_id
        self.users = Users(master_id, claimed_id)
        self.guild_id = guild_id
        self.message_id = message_id
        self.timestamp = timestamp
        self.note = note

    def to_dict(self):
        return {
            "version": self.version,
            "guild_id": self.guild_id,
            "message_id": self.message_id,
            "timestamp": self.timestamp,
            "id": self.id,
            "users": self.users.to_dict(),
            "note": self.note
        }

    async def delete(self, logger=None):
        await delete_claim(mode="couchdb_id_unknown", guild_id=self.guild_id, users=self.users.to_dict(), logger=logger)

# get a claim
async def get_claim(guild_id, mode=None, users=None, logger=None):
    if mode == "known_users_not_positions": # users is a list
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Getting claims in guild {guild_id} between ({users[0]} and {users[1]}).", HandlerType.API_CLAIMS))
        
        mango_query = {
            "selector": {
                "guild_id": guild_id,
                "$or": [{
                    "users.master": users[0],
                    "users.claimed": users[1]
                },
                {
                    "users.master": users[1],
                    "users.claimed": users[0]
                }]
            },
            "limit": 1
        }

        claim_json = database.get_doc("claiming", mango_query)

    elif mode == "known_users_known_positions": # users is a dict
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Getting claims in guild {guild_id} between (master {users['master']} and claimed {users['claimed']}).", HandlerType.API_CLAIMS))

        mango_query = {
            "selector": {
                "guild_id": guild_id,
                "users.master": users["master"],
                "users.claimed": users["claimed"]
            },
            "limit": 1
        }

        claim_json = database.get_doc("claiming", mango_query)

    elif mode == "check_if_valid_claim": # users is a dict
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Checking if (master {users['master']} and claimed {users['claimed']}) in guild {guild_id} is a valid claim.", HandlerType.API_CLAIMS))

        mango_query = {
            "selector": {
                "guild_id": guild_id,
                "$or": [
                    {
                        "users.master": users["claimed"], # if the claimed is a master
                    },
                    {
                        "users.claimed": users["claimed"] # if the claimed is claimed
                    },
                    {
                        "users.claimed": users["master"] # if you are claimed
                    }
                ]
            },
            "limit": 1
        }

        claim_json = database.get_doc("claiming", mango_query)
        
    if claim_json == "No documents found":
        asyncio.create_task(log(logger, ContextType.INTERACTION, "No claims were found.", HandlerType.API_CLAIMS))
        return None

    claim_json = json.dumps(claim_json[0])

    # Function to create a Claim instance from JSON data
    data_dict = json.loads(claim_json)

    return Claim(data_dict["version"], data_dict["id"], data_dict["users"]["master"], data_dict["users"]["claimed"], data_dict["guild_id"], data_dict["message_id"], data_dict["timestamp"], data_dict["note"])

# create a claim
async def create_claim(claimed, master, guild_id, interaction_id, note, message_id="0", logger=None):
    asyncio.create_task(log(logger, ContextType.INTERACTION, f"Creating a claim in guild {guild_id} between (master: {master} and claimed: {claimed}).", HandlerType.API_CLAIMS))

    # get guild info
    guild_info = await guilds_api.get_guild(guild_id)
    
    guild_claimed_role_id = guild_info.roles.claimed
    guild_master_role_id = guild_info.roles.master
    guild_claim_list_channel_id = guild_info.channels.claim_list

    # update member
    member = await members_api.get_member(master, guild_id)
    claimed_ids = member.claimed_ids
    claimed_ids.append(claimed)

    update_dict = {
        "claimed_ids": claimed_ids
    }
    await members_api.update_member(master, guild_id, update_dict)

    # add roles
    http_api.add_role(guild_id, claimed, guild_claimed_role_id, "Joined claim")
    http_api.add_role(guild_id, master, guild_master_role_id, "Joined claim")

    try:
        if note == "":
            message_data = {
                "content": f"<@{claimed}> is owned by <@{master}>.",
                "allowed_mentions": {
                    "users": [master, claimed]
                },
                "flags": 4
            }
        else:
            message_data = {
                "content": f"<@{claimed}> is owned by <@{master}>.\n\n**__Note from the master:__**\n{note}",
                "allowed_mentions": {
                    "users": [master, claimed]
                },
                "flags": 4
            }
        claim_list_message_id = json.loads(http_api.send_message(guild_claim_list_channel_id, message_data).text)["id"]
    except KeyError:
        claim_list_message_id = "0"

    if message_id == "0": message_id = claim_list_message_id

    # add to DB
    timestamp = str(floor(time()))
    values = {
        "version": DBVersions.claiming,
        "id": interaction_id,
        "users": {
            "master": master,
            "claimed": claimed
        },
        "guild_id": guild_id,
        "message_id": message_id,
        "timestamp": timestamp,
        "note": note
    }
    database.insert_doc("claiming", values)
    asyncio.create_task(log(logger, ContextType.INTERACTION, "Success!", HandlerType.API_CLAIMS))

    return Claim(DBVersions.claiming, interaction_id, claimed, master, guild_id, message_id, timestamp, note)

# update claim without the object
async def update_claim(users, guild_id, update_dict, logger=None): # we will always know the positions, so users is a dict
    asyncio.create_task(log(logger, ContextType.INTERACTION, f"Updating a claim in guild {guild_id} between (master: {users['master']} and claimed: {users['claimed']}) without the object.", HandlerType.API_CLAIMS))

    mango_query = {
        "selector": {
            "guild_id": guild_id,
            "users.master": users["master"],
            "users.claimed": users["claimed"]
        },
        "limit": 1
    }

    claim = database.update_doc("claiming", mango_query, update_dict)
    
    # return a member object
    return Claim(claim["version"], claim["id"], claim["users"]["master"], claim["users"]["claimed"], claim["guild_id"], claim["message_id"], claim["timestamp"], claim["note"])

# delete a claim
# currently somewhat messy
async def delete_claim(couchdb_id=None, mode=None, guild_id=None, users=None, guild_info=None, logger=None):
    master = users["master"]
    claimed = users["claimed"]
    if mode == "couchdb_id_unknown":
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Deleting a claim in guild {guild_id} between (master: {users['master']} and claimed: {users['claimed']}).", HandlerType.API_CLAIMS))
        mango_query = {
            "selector": {
                "guild_id": guild_id,
                "$or": [{
                    "users.master": master,
                    "users.claimed": claimed
                },
                {
                    "users.master": claimed,
                    "users.claimed": master
                }]
            },
            "limit": 1
        }

        # this one needs to be the json
        claim_object = database.get_doc("claiming", mango_query)

    # same as another mode
    if couchdb_id != None:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Deleting a claim by CouchDB ID.", HandlerType.API_CLAIMS))
        claim_object = database.get_doc("claiming", {"selector": {"_id": couchdb_id}, "limit": 1})

    if claim_object == "No documents found":
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Delete claim failed because the claim does not exist.", HandlerType.API_CLAIMS))
        return "Claim does not exist."

    claim_object = claim_object[0]
    
    couchdb_id = claim_object["_id"]
    database.delete_doc("claiming", {"selector": {"_id": couchdb_id}, "limit": 1})

    users = claim_object["users"]
    guild_id = claim_object["guild_id"]

    member = await members_api.get_member(users["master"], guild_id)
    claimed_ids = member.claimed_ids
    try:
        claimed_ids.remove(users["claimed"])
    except ValueError:
        pass

    # remove from member
    update_dict = {
        "claimed_ids": claimed_ids
    }
    member = await members_api.update_member(users["master"], guild_id, update_dict)

    # get ids
    if guild_info == None:
        guild_info = await guilds_api.get_guild(guild_id)
        
    guild_claimed_role_id = guild_info.roles.claimed
    guild_master_role_id = guild_info.roles.master
    guild_claim_list_channel_id = guild_info.channels.claim_list

    # remove roles
    http_api.remove_role(guild_id, users["claimed"], guild_claimed_role_id, "Left claim")
    if len(claimed_ids) <= 1: # master, if they don't have enough claims
        http_api.remove_role(guild_id, users["master"], guild_master_role_id, "Left claim")

    # delete the message
    if claim_object["message_id"] != "0": http_api.delete_message(guild_claim_list_channel_id, claim_object["message_id"], "Removing a claim")
    
    asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim successfully deleted.", HandlerType.API_CLAIMS))
    return "Claim deleted."


