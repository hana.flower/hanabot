# custom libraries
import database
import tools
from config import *
import http_api

from api import guilds as guilds_api
from api import members as members_api
from api import users as users_api

# not custom libraries
import json
from enum import Enum
from math import floor
from time import time

# verification flags
class VerificationFlags(Enum):
    REVOKED = 1
    USER_LEFT = 2
    CROSS_VERIFIED = 4
    HANABOT_LEFT = 8

class VerificationStatus:
    def __init__(self, user_id, guild_id, timestamp, flags, revoked_reason=None):
        self.user_id = user_id
        self.guild_id = guild_id
        self.timestamp = timestamp
        self.flags = flags
        self.revoked_reason = revoked_reason

    def to_dict(self, include_guild_id=True, include_user_id=True):
        # required fields only
        status_dict = {
            "timestamp": self.timestamp,
            "flags": self.flags,
            "revoked_reason": self.revoked_reason
        }
        if include_guild_id == True: status_dict["guild_id"] = self.guild_id
        if include_user_id == True: status_dict["user_id"] = self.user_id
        return status_dict

# verification requests
class VerificationRequest:
    def __init__(self, guild_id, requesting_guild_id):
        self.guild_id = guild_id
        self.requesting_guild_id = requesting_guild_id

    def to_dict(self):
        return {
            "guild_id": self.guild_id,
            "requesting_guild_id": self.requesting_guild_id
        }

# get verification request
async def get_verification_request(guild_id, requesting_guild_id, create_if_not_found=True, return_doc=False):
    mango_query = {
        "selector": {
            "guild_id": guild_id,
            "requesting_guild_id": requesting_guild_id
        },
        "limit": 1
    }
    request_json = database.get_doc("cross_verify_requests", mango_query)

    if request_json == "No documents found": # register the new request
        # if needed
        if create_if_not_found == False:
            return None

        request_json = await create_verification_request(guild_id, requesting_guild_id)

    print(request_json)
    request_json = json.dumps(request_json[0])

    # Function to create a request instance from JSON data
    # I don't understand this, thanks chatgpt
    data_dict = json.loads(request_json)

    if return_doc == True: return data_dict # return the raw document's JSON data

    return VerificationRequest(data_dict["guild_id"], data_dict["requesting_guild_id"])

# create verification request
async def create_verification_request(guild_id, requesting_guild_id, return_doc=False):
    values = {
        "guild_id": guild_id,
        "requesting_guild_id": requesting_guild_id
    }

    request_doc = database.insert_doc("cross_verify_requests", values)
    database.schedule_doc_for_deletion(request_doc["_id"], "cross_verify_requests", 4)

    print(request_doc)
    request_doc = json.dumps(request_doc)

    if return_doc == True:
        return json.loads(request_doc)
    else:
        return VerificationRequest(guild_id, requesting_guild_id)

# accept a request
async def accept_verification_request(guild_id, requesting_guild_id):
    request = await get_verification_request(guild_id, requesting_guild_id, create_if_not_found=False)

    if request == None: return "Request does not exist"

    # add to guild objects
    guild = await guilds_api.get_guild(guild_id)
    requesting_guild = await guilds_api.get_guild(requesting_guild_id)

    cross_verify_guilds_requested_list = guild.cross_verify_guilds
    cross_verify_guilds_requested_list.append(requesting_guild_id)

    cross_verify_guilds_requesting_list = requesting_guild.cross_verify_guilds
    cross_verify_guilds_requesting_list.append(guild_id)

    cross_verify_guilds_requested = {"cross_verify_guilds": cross_verify_guilds_requested_list}
    guild.update(cross_verify_guilds_requested)

    cross_verify_guilds_requesting = {"cross_verify_guilds": cross_verify_guilds_requesting_list}
    requesting_guild.update(cross_verify_guilds_requesting)

    http_api.send_message(guild.channels.cross_verify_log, {"content": f"You **accepted** a partnership from {requesting_guild_id}."})
    http_api.send_message(requesting_guild.channels.cross_verify_log, {"content": f"Your partnership with the server {guild_id} was **accepted** by the other server."})

    # delete request doc
    mango_query = {
        "selector": {
            "guild_id": guild_id,
            "requesting_guild_id": requesting_guild_id
        },
        "limit": 1
    }
    database.delete_doc("cross_verify_requests", mango_query)

    return True # status

# decline a verification request
async def decline_verification_request(guild_id, requesting_guild_id):
    mango_query = {
        "selector": {
            "guild_id": guild_id,
            "requesting_guild_id": requesting_guild_id
        },
        "limit": 1
    }

    request_object = database.delete_doc("cross_verify_requests", mango_query)

    if request_object == "No documents found":
        return "Request does not exist."

    return "Request deleted."

# delete request by couchdb id
async def delete_request_by_couchdb_id(couchdb_id):
    request_object = database.delete_doc("cross_verify_requests", {"selector": {"_id": couchdb_id}, "limit": 1})

    if request_object == "No documents found":
        return "Request does not exist."

    return "Request deleted."

# remove an existing partnership between two guilds
async def remove_verification_partnership(guild_id, requesting_guild_id):
    # remove from guild objects
    # let's just keep variable names the same it shouldn't matter
    guild = await guilds_api.get_guild(guild_id)
    requesting_guild = await guilds_api.get_guild(requesting_guild_id)

    cross_verify_guilds_requested_list = guild.cross_verify_guilds
    cross_verify_guilds_requested_list.remove(requesting_guild_id)

    cross_verify_guilds_requesting_list = requesting_guild.cross_verify_guilds
    cross_verify_guilds_requesting_list.remove(guild_id)

    cross_verify_guilds_requested = {"cross_verify_guilds": cross_verify_guilds_requested_list}
    guild.update(cross_verify_guilds_requested)

    cross_verify_guilds_requesting = {"cross_verify_guilds": cross_verify_guilds_requesting_list}
    requesting_guild.update(cross_verify_guilds_requesting)

    http_api.send_message(guild.channels.cross_verify_log, {"content": f"You **removed** your partnership with the server {requesting_guild_id}"})
    http_api.send_message(requesting_guild.channels.cross_verify_log, {"content": f"Your partnership with the server {guild_id} was **removed** by the other server."})

    return "Partnership removed."

# checks if a user is eligible to cross-verify
async def check_verification_eligibility(guild_id, user_id, guild_info=None, user=None):
    # allow passing predetermined user and guild info objects
    if guild_info == None: guild_info = await guilds_api.get_guild(guild_id)
    if user == None: user = await users_api.get_user(user_id)

    # can_verify structure: (is_eligible, is_cross_verification, has_previously_verified)
    # note to self: update the comment in hb_cross_verify.py claim_verification() too
    can_verify = [False, False, False]

    for guild in user.verified_guilds:
        flags = await tools.get_flags(guild.flags, VerificationFlags)

        # verifying from the same guild
        if guild.guild_id == guild_id:
            # these flags will result in an invalid verification
            # currently only the REVOKED flag
            # to-do: HANABOT_LEFT flag (will be implemented alongside guild deletion)
            flags_to_check = [VerificationFlags.REVOKED]

            if any(flag in flags for flag in flags_to_check):
                # this is to avoid verification revoked members verifying themselves
                return (False, False, False)
            else:
                can_verify[0] = True
                can_verify[1] = VerificationFlags.CROSS_VERIFIED in flags
                can_verify[2] = True
            break

        # cross-verifying from another guild
        if guild.guild_id in guild_info.cross_verify_guilds:
            # these flags will result in an invalid verification
            # currently only the REVOKED flag
            # to-do: HANABOT_LEFT flag (will be implemented alongside guild deletion)
            flags_to_check = [VerificationFlags.REVOKED]

            if not(any(flag in flags for flag in flags_to_check)):
                print("This is valid from other guild")
                can_verify[0] = True
                can_verify[1] = True

    return (can_verify[0], can_verify[1], can_verify[2])

# verify a member
# this assumes the member can be verified
# use check verification eligibility to check if the user can be verified
async def verify_member(guild_id, user_id, initial_flags_int, remove_cross_verified=False, eligibility=None):
    guild_info = await guilds_api.get_guild(guild_id)

    verification_status = {
        "guild_id": guild_id,
        "timestamp": int(round(time())),
        "flags": initial_flags_int,
        "revoked_reason": None
    }

    final_flags_int = initial_flags_int
    initial_flags = await tools.get_flags(initial_flags_int, VerificationFlags)
    final_flags = await tools.get_flags(final_flags_int, VerificationFlags)

    # update the global user object
    user = await users_api.get_user(user_id, create_if_not_found=True)
    verified_guilds = user.verified_guilds

    # we need to convert from VerificationStatus, which the internal API returns
    verified_guilds = [verified_guild.to_dict(include_guild_id=True, include_user_id=False) for verified_guild in verified_guilds]

    # don't register the guild twice
    original_verification = False
    cross_verify_removed = False

    for entry in verified_guilds:
        # original verification
        if entry.get("guild_id") == guild_id:
            original_verification = True

            entry_flags = await tools.get_flags(entry["flags"], VerificationFlags)

            # only remove the REVOKED flag if it is set in both the entry and initial flags
            if VerificationFlags.REVOKED in entry_flags:
                entry["flags"] -= VerificationFlags.REVOKED.value

            if remove_cross_verified == True:
                # only remove the CROSS_VERIFIED flag if it is set in both the entry and initial flags
                if VerificationFlags.CROSS_VERIFIED in entry_flags:
                    entry["flags"] -= VerificationFlags.CROSS_VERIFIED.value
                    cross_verify_removed = True

            entry["revoked_reason"] = None

            break

    verification_status["flags"] = final_flags_int

    if original_verification == False:
        verified_guilds.append(verification_status)

    update_dict = {"verified_guilds": verified_guilds}
    await users_api.update_user(user_id, update_dict)

    # update the per-guild member object
    update_dict = {
        "verification_status": {
            "timestamp": verification_status["timestamp"],
            "flags": verification_status["flags"],
            "revoked_reason": None
        }
    }
    await members_api.update_member(user_id, guild_id, update_dict)

    # add the role
    http_api.add_role(guild_id, user_id, guild_info.roles.major_verified, "Verifying a member")

    # send message to the log channel

    # conditions:
    # *for verified in server msg: either original verification, or cross verify flag was removed
    # *for cross-verified msg: only if this is a cross verification

    if eligibility != None:
        # if you have not previously verified
        if eligibility[2] == False:
            if eligibility[1] == True:
                pass#http_api.send_message(guild_info.channels.cross_verify_log, {"content": f"<@{user_id}> cross-verified from the servers: TO-DO: List guilds here, or just say to use a command for it."})
            else:
                pass#http_api.send_message(guild_info.channels.cross_verify_log, {"content": f"<@{user_id}> verified in this server."})
        # already cross-verified, but just original verified
        elif cross_verify_removed == True:
            pass#http_api.send_message(guild_info.channels.cross_verify_log, {"content": f"<@{user_id}> verified in this server."})

    return (True, guild_info.channels.cross_verify_log, cross_verify_removed, original_verification)

# unverify a member
# this can most likely be merged into the below function
async def unverify_member(guild_id, user_id, reason="No reason provided"):
    guild_info = await guilds_api.get_guild(guild_id)

    # update the global user object
    user = await users_api.get_user(user_id, create_if_not_found=True)
    verified_guilds = user.verified_guilds

    # we need to convert from VerificationStatus, which the internal API returns
    verified_guilds = [verified_guild.to_dict(include_guild_id=True, include_user_id=False) for verified_guild in verified_guilds]

    verification_status = {}

    for entry in verified_guilds:
        if entry.get("guild_id") == guild_id:
            entry_flags = await tools.get_flags(entry["flags"], VerificationFlags)
            if VerificationFlags.REVOKED not in entry_flags:
                updated_flags = entry["flags"] + VerificationFlags.REVOKED.value
                entry["flags"] = updated_flags
                entry["revoked_reason"] = reason
                verification_status = {
                    "guild_id": guild_id,
                    "timestamp": entry["timestamp"],
                    "flags": updated_flags,
                    "revoked_reason": reason
                }
                break
            else:
                return (False, guild_info.channels.cross_verify_log, "Already unverified")

    # not found
    if verification_status == {}:
        return (False, guild_info.channels.cross_verify_log, "Verification not found")

    update_dict = {"verified_guilds": verified_guilds}
    await users_api.update_user(user_id, update_dict)

    # update the per-guild member object
    update_dict = {
        "verification_status": {
            "timestamp": verification_status["timestamp"],
            "flags": verification_status["flags"],
            "revoked_reason": reason
        }
    }
    await members_api.update_member(user_id, guild_id, update_dict)

    # remove the role
    http_api.remove_role(guild_id, user_id, guild_info.roles.major_verified, "Unverifying a member")

    # send message to the log channel
    #http_api.send_message(guild_info.channels.cross_verify_log, {"content": f"<@{user_id}> was unverified for reason:\n`{reason}`"})

    return (True, guild_info.channels.cross_verify_log)

# add or remove a flag from a verification
async def update_verification_flags(guild_id, user_id, flags):
    user = await users_api.get_user(user_id, create_if_not_found=True)
    verified_guilds = user.verified_guilds

    # we need to convert from VerificationStatus, which the internal API returns
    verified_guilds = [verified_guild.to_dict(include_guild_id=True, include_user_id=False) for verified_guild in verified_guilds]

    verification_status = {}
    for entry in verified_guilds:
        if entry.get("guild_id") == guild_id:
            entry_flags = await tools.get_flags(entry["flags"], VerificationFlags)
            updated_flags = entry["flags"]
            for flag in flags:
                if flag not in entry_flags:
                    updated_flags += flag.value
            entry["flags"] = updated_flags
            verification_status = {
                "guild_id": guild_id,
                "timestamp": entry["timestamp"],
                "flags": updated_flags,
                "revoked_reason": entry["revoked_reason"]
            }
            break

    # not found
    if verification_status == {}:
        return (False, "Verification not found")

    update_dict = {"verified_guilds": verified_guilds}
    await users_api.update_user(user_id, update_dict)

    # update the per-guild member object
    update_dict = {
        "verification_status": {
            "timestamp": verification_status["timestamp"],
            "flags": verification_status["flags"],
            "revoked_reason": verification_status["revoked_reason"]
        }
    }
    await members_api.update_member(user_id, guild_id, update_dict)
