# custom libraries
import database
import tools
from config import *
import http_api
from api import cross_verify as cross_verify_api

# not custom libraries
import json
from enum import Enum
from math import floor
from time import time

class Gender(Enum):
    UNKNOWN = 0
    MALE = 1
    FEMALE = 2
    TRANS_MTF = 3
    TRANS_FTM = 4
    NON_BINARY = 5
    GENDERFLUID = 6
    OTHER = 7

class User:
    def __init__(self, version, user_id, gender, verified_guilds):
        self.version = version
        self.user_id = user_id
        self.gender = Gender(gender) # I believe we can just always pass the gender enum to this class and have it work fine
        self.verified_guilds = [cross_verify_api.VerificationStatus(user_id, guild_data["guild_id"], guild_data["timestamp"], guild_data["flags"], guild_data["revoked_reason"]) for guild_data in verified_guilds]

    def to_dict(self):
        return {
            "version": self.version,
            "user_id": self.user_id,
            "gender": self.gender.value,
            "verified_guilds": self.verified_guilds
        }

# get a user
async def get_user(user_id, return_doc=False, create_if_not_found=True):
    mango_query = {
        "selector": {
            "user_id": user_id
        },
        "limit": 1
    }
    user_json = database.get_doc("users", mango_query)
        
    if user_json == "No documents found": # register the new user
        # if needed
        if create_if_not_found == False:
            return None

        user_json = [await create_user(user_id, return_doc=True)]

    print(user_json)
    user_json = json.dumps(user_json[0])

    # Function to create a user instance from JSON data
    # I don't understand this, thanks chatgpt 
    data_dict = json.loads(user_json)
    
    if return_doc == True: return data_dict # return the raw document's JSON data

    return User(data_dict["version"], data_dict["user_id"], Gender(data_dict["gender"]), data_dict["verified_guilds"])

# initialise user
async def create_user(user_id, gender=Gender.UNKNOWN, verified_guilds=[], return_doc=False):
    # default values are empty
    values = {
        "user_id": user_id,
        "version": DBVersions.users,
        "gender": gender.value, # to-do potentially: make sure this is an integer value and not a reference to the enum or something idk I'll figure it out later
        "verified_guilds": verified_guilds # and maybe same as above
    }
    user_doc = database.insert_doc("users", values)

    print(user_doc)
    user_json = json.dumps(user_doc)
    
    if return_doc == True:
        return json.loads(user_json)
    else:
        return User(DBVersions.users, user_id, gender, verified_guilds) # same thing with gender as above, look I will work on it later I just want to get some boring groundwork done right now

# update user without the object
async def update_user(user_id, update_dict):
    user = database.update_doc("users", {"selector": {"user_id": user_id}, "limit": 1}, update_dict)
    
    # return a user object
    return User(user["version"], user["user_id"], Gender(user["gender"]), user["verified_guilds"])

# delete a user
async def delete_user(user_id):
    mango_query = {
        "selector": {
            "user_id": user_id,
        },
        "limit": 1
    }

    database.delete_doc("users", mango_query)
    
    return "User deleted."
