# custom libraries
import database
import tools
from config import *
import http_api
from logger import *

from api import guilds as guilds_api
from api import members as members_api

# not custom libraries
import json
from math import floor
from time import time

# classes
class Users:
    def __init__(self, master_id, claimed_id):
        self.master = master_id
        self.claimed = claimed_id

    def to_dict(self):
        return {
            "claimed": self.claimed,
            "master": self.master
        }

    def get_user(self, user_id):
        if user_id == self.master:
            return "master"
        elif user_id == self.claimed:
            return "claimed"
        else:
            return None  # Return None if the user is not found in the class

class WaitlistClaim:
    def __init__(self, version, master_id, claimed_id, guild_id, note, accepted):
        self.version = version
        self.users = Users(master_id, claimed_id) # possible to-do: implement users class instead
        self.guild_id = guild_id
        self.note = note
        self.accepted = accepted

    def to_dict(self):
        return {
            "version": self.version,
            "users": self.users.to_dict(),
            "guild_id": self.guild_id,
            "accepted": self.accepted,
            "note": self.note
        }

    def delete(self):
        pass
        #await delete_claim(mode="couchdb_id_unknown", guild_id=self.guild_id, users=self.users.to_dict())

# get a waitlist claim
async def get_waitlist_claim(guild_id, users=None, create_if_none_found=True, logger=None):
    mango_query = {
        "selector": {
            "guild_id": guild_id,
            "users": {}
        },
        "limit": 1
    }

    if "claimed" in users:
        mango_query["selector"]["users"]["claimed"] = users["claimed"]

    if "master" in users:
        mango_query["selector"]["users"]["master"] = users["master"]

    log_message = f"Getting waitlist claim in guild {guild_id} between ({users})." # too lazy to format this one lol
    asyncio.create_task(log(logger, ContextType.INTERACTION, log_message, HandlerType.API_CLAIMS_WAITLIST))

    print(mango_query)

    claim_json = database.get_doc("claims_waitlist", mango_query)
    print(claim_json)
        
    if claim_json == "No documents found":
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"No waitlist claims were found.", HandlerType.API_CLAIMS_WAITLIST))
        return None

    claim_json = json.dumps(claim_json[0])

    # Function to create a Claim instance from JSON data
    data_dict = json.loads(claim_json)

    return WaitlistClaim(data_dict["version"], data_dict["users"]["master"], data_dict["users"]["claimed"], data_dict["guild_id"], data_dict["note"], data_dict["accepted"])

# create a waitlist claim
async def create_waitlist_claim(users, guild_id, note="", accepted=[], logger=None):
    asyncio.create_task(log(logger, ContextType.INTERACTION, f"Creating waitlist claim in guild {guild_id} for users (master: {users['master']} and claimed: {users['claimed']}).", HandlerType.API_CLAIMS_WAITLIST))

    values = {
        "version": DBVersions.claims_waitlist,
        "users": users,
        "guild_id": guild_id,
        "note": note,
        "accepted": accepted
    }
    doc = database.insert_doc("claims_waitlist", values)
    database.schedule_doc_for_deletion(doc["_id"], "claims_waitlist", 4)

    return WaitlistClaim(DBVersions.claims_waitlist, users["master"], users["claimed"], guild_id, note, accepted)

# update waitlist claim without the object
async def update_waitlist_claim(users, guild_id, update_dict):
    waitlist_claim = database.update_doc("claims_waitlist", {"selector": {"users": users, "guild_id": guild_id}, "limit": 1}, update_dict)
    
    # return a member object
    return WaitlistClaim(waitlist_claim["version"], waitlist_claim["users"]["master"], waitlist_claim["users"]["claimed"], waitlist_claim["guild_id"], waitlist_claim["note"], waitlist_claim["accepted"])

# delete a waitlist claim
async def delete_waitlist_claim(users, guild_id):
    mango_query = {
        "selector": {
            "users": users,
            "guild_id": guild_id
        },
        "limit": 1
    }

    database.delete_doc("claims_waitlist", mango_query)
    
    return "Waitlist claim deleted."

# delete guild by couchdb id
async def delete_waitlist_claim_by_couchdb_id(couchdb_id):
    waitlist_claim = database.delete_doc("claims_waitlist", {"selector": {"_id": couchdb_id}, "limit": 1})

    if waitlist_claim == "No documents found":
        return "Waitlist claim does not exist."

    return "Waitlist claim deleted."
