# i don't think this is how you use couchdb but whatever
import couchdb
import os
import argparse
import json
from config import *
import update_db_docs
from time import time
from time import sleep
from math import floor
from os.path import exists
from shutil import copy
global version
version = 1

# register cmds
# if they are already registered, they will not be changed if this file is run again
def registercmds(include_test_cmds):
    # very messy but it's ok
    commands = []

    claiming_cmd = {
        "name": "claiming",
        "description": "Assert your dominance over your pets!",
        "dm_permission": False,
        "options": [{
            "type": 1,
            "name": "claim",
            "description": "Claim someone like a little pet!",
            "options": [{
                "type": 6,
                "name": "claimant",
                "description": "Who do you want to claim? This person will be your sub.",
                "required": True
            },{
                "type": 3,
                "name": "note",
                "description": "Add extra info about your claim!",
                "max_length": 250
            }]
        },{
            "type": 1,
            "name": "edit",
            "description": "Modify info about one of your claims!",
            "options": [{
                "type": 6,
                "name": "member",
                "description": "The person whose claim you want to modify info about",
                "required": True
            },{
                "type": 3,
                "name": "note",
                "description": "Add extra info about your claim! You can delete the note by inputting \"Delete\"",
                "max_length": 250
            }]
        },{
            "type": 1,
            "name": "leave",
            "description": "Want to leave your claim? That's fine!",
            "options": [{
                "type": 6,
                "name": "member",
                "description": "Who do you want to leave? If the user is not in the server, you must use their user ID",
                "required": True
            }]
        }],
        "nsfw": False # claiming should be NSFW, but a lot of issues occur when you mark it as NSFW because discord is a shitty service
    }

    warnings_cmd = {
        "name": "warnings",
        "description": "Someone's being a naughty member~",
        "dm_permission": False,
        "options": [{
            "type": 1,
            "name": "warn",
            "description": "Warn someone.",
            "options": [{
                "type": 6,
                "name": "member",
                "description": "Who do you want to warn?",
                "required": True
            },{
                "type": 3,
                "name": "reason",
                "description": "Why?",
                "required": True
            }]
        },{
            "type": 1,
            "name": "list",
            "description": "List someone's warns.",
            "options": [{
                "type": 6,
                "name": "member",
                "description": "Whose warns you want to check.",
                "required": True
            }]
        },{
            "type": 1,
            "name": "delete",
            "description": "Delete a warn.",
            "options": [{
                "type": 6,
                "name": "member",
                "description": "Whose warns you want to delete.",
                "required": True
            },{
                "type": 4,
                "name": "number",
                "description": "The warn number you want to delete, e.g. warn #1.",
                "required": True
            }]
        }],
        "nsfw": False
    }

    update_presence_cmd = {
        "name": "update-presence",
        "description": "Only bot owners can use this. Updates the bot's presence.",
        "dm_permission": True,
        "options": [{
            "type": 3,
            "name": "status",
            "description": "The status of the bot",
            "choices": [{
                    "name": "Online",
                    "value": "online"
                },{
                    "name": "Idle",
                    "value": "idle"
                },{
                    "name": "Do Not Disturb",
                    "value": "dnd"
                },{
                    "name": "Invisible (why?)",
                    "value": "invisible"
                }]
            },{
                "type": 4,
                "name": "activity-type",
                "description": "The activity type",
                "choices": [{
                        "name": "Playing {name}",
                        "value": 0
                    },{
                        "name": "Streaming {name} (url can be set to a twitch or youtube link and will appear on the bot's profile)",
                        "value": 1
                    },{
                        "name": "Listening to {name}",
                        "value": 2
                    },{
                        "name": "Watching {name}",
                        "value": 3
                    },{
                        "name": "Custom status: {emoji} {state} (emoji cannot be set)",
                        "value": 4
                    },{
                        "name": "Competing in {name}",
                        "value": 5
                    }],
                "min_value": 0,
                "max_value": 5
            },{
                "type": 3,
                "name": "activity-name",
                "description": "The activity name"
            },{
                "type": 3,
                "name": "activity-state",
                "description": "The activity state"
            },{
                "type": 3,
                "name": "activity-url",
                "description": "The activity URL"
            }],
        "nsfw": False
    }

    settings_cmd = {
        "name": "settings",
        "description": "Change my settings!",
        "dm_permission": False,
        "nsfw": False
    }

    help_cmd = {
        "name": "help",
        "description": "Lists available commands and help I can give you~",
        "dm_permission": True,
        "nsfw": False
    }

    reload_config_cmd = {
        "name": "reload-config",
        "description": "Only bot owners can use this. Reloads (most of) the bot's internal configuration",
        "dm_permission": True,
        "nsfw": False
    }

    cross_verify_cmd = {
        "name": "cross-verify",
        "description": "Never show your ID again!",
        "options": [{
            "type": 1,
            "name": "claim",
            "description": "Claim your verification if you are eligible"
        },{
            "type": 1,
            "name": "set-gender",
            "description": "Set your preferred gender",
            "choices": [{
                "name": "Prefer not to say",
                "value": 0
            },{
                "name": "Male",
                "value": 1
            },{
                "name": "Female",
                "value": 2
            },{
                "name": "Trans Male --> Female",
                "value": 3
            },{
                "name": "Trans Female --> Male",
                "value": 4
            },{
                "name": "Non-Binary",
                "value": 5
            },{
                "name": "Genderfluid",
                "value": 6
            },{
                "name": "Other",
                "value": 7
            }],
            "min_value": 0,
            "max_value": 7
        },{
            "type": 1,
            "name": "verify",
            "description": "Verify a member",
            "options": [{
                "type": 6,
                "name": "member",
                "description": "The member you want to verify. They must be in the server",
                    "required": True
            }]
        },{
            "type": 1,
            "name": "unverify",
            "description": "Remove a member's verification",
            "options": [{
                "type": 6,
                "name": "member",
                "description": "The member you want to unverify. For someone who left, use their ID, e.g. 1134229490034290849",
                "required": True
            },{
                "type": 3,
                "name": "reason",
                "description": "Why are you removing this verification? Other servers will be notified of why",
                "max_length": 250,
                "required": True
            }]
        },{
            "type": 2,
            "name": "servers",
            "description": "Manage cross-verify eligible servers",
            "options": [{
                "type": 1,
                "name": "add",
                "description": "Request to partner with another server for cross-verification",
                "options": [{
                    "type": 3,
                    "name": "server-id",
                    "description": "The ID of the server you want to request, e.g. 1137870796191178892",
                    "max_length": 25,
                    "required": True
                }]
            },{
                "type": 1,
                "name": "remove",
                "description": "Remove a server from being able to cross-verify with you",
                "options": [{
                    "type": 3,
                    "name": "server-id",
                    "description": "The ID of the server you want to remove, e.g. 1137870796191178892",
                    "max_length": 25,
                    "required": True
                }]
            },{
                "type": 1,
                "name": "list",
                "description": "List all cross-verify eligible servers"
            }]
        }],
        "nsfw": False,
        "dm_permission": False
    }

    # test commands
    if include_test_cmds == True:
        print("You are including experimental and broken test commands that are not at all maintained. God help you.")
        test_cmd = {
            "name": "test",
            "description": "test",
            "dm_permission": True,
            "nsfw": False
        }

        test1_cmd = {
            "name": "test1",
            "description": "test command",
            "dm_permission": True,
            "nsfw": False
        }

        onboarding_cmd = {"name": "onboarding",
            "description": "Let me welcome the new members!",
            "dm_permission": True,
            "options": [{
                "type": 2,
                "name": "question",
                "description": "Modify an onboarding question",
                "options": [{
                    "type": 1,
                    "name": "list",
                    "description": "List all onboarding questions."
                },{
                    "type": 1,
                    "name": "create",
                    "description": "Create an onboarding question."
                },{
                    "type": 1,
                    "name": "edit",
                    "description": "Edit an onboarding question."
                },{
                    "type": 1,
                    "name": "delete",
                    "description": "Delete an onboarding question."
                }]
            },{
                "type": 2,
                "name": "option",
                "description": "Modify the options for an onboarding question",
                "options": [{
                    "type": 1,
                    "name": "list",
                    "description": "List all options for an onboardng question."
                },{
                    "type": 1,
                    "name": "create",
                    "description": "Create an option for an onboarding question.",
                    "options": [{
                            "type": 3,
                            "name": "emoji",
                            "description": "The emoji you want to use",
                            "max_length": 200
                        }]
                },{
                    "type": 1,
                    "name": "edit",
                    "description": "Edit an option for an onboarding question."
                },{
                    "type": 1,
                    "name": "delete",
                    "description": "Delete an option for an onboarding question."
                }]}],
            "nsfw": False
        }

        commands.append(test_cmd)
        commands.append(test1_cmd)
        commands.append(onboarding_cmd)

    # append commands to list based on version
    if version < 2: # not set up yet
        commands.append(claiming_cmd)
        commands.append(warnings_cmd)
        commands.append(update_presence_cmd)
        commands.append(settings_cmd)
        commands.append(help_cmd)
    
    elif version < 3:
        commands.append(reload_config_cmd)

    elif version < 5:
        commands.append(cross_verify_cmd)

    else:
        print("No need to set up commands; your setup.lock version is up to date.")
        return

    # make the commands exist
    import requests
    headers = {"Authorization": "Bot " + Credentials.TOKEN, "User-Agent": f"DiscordBot Hanabot/v{BotInfo.git_commit_hash_short}", "Content-Type": "application/json"}

    with open("config.json", "r") as config_file:
        config = json.load(config_file)

    # get application ID
    r = requests.get("https://discord.com/api/v10/users/@me", headers=headers)
    print(r)
    print(r.text)
    application_id = json.loads(r.text)["id"]

    config["application_id"] = application_id
    with open("config.json", "w") as config_file:
        json.dump(config, config_file, indent=4)

    # register all commands
    url = f"https://discord.com/api/v10/applications/{application_id}/commands"
    command_ids = {}

    def register_cmd(url, headers, command):
        r = requests.post(url, headers=headers, data=json.dumps(command))
        print(r)
        print(r.text)
        if r.status_code == 401:
            print("Your bot token was invalid, please set up the .env file first.")
            exit()

        # add to config.json
        name = command["name"]
        ID = json.loads(r.text)["id"]
        command_ids[name] = ID
        return {command["name"]: ID}

    for command in commands:
        registered_command = register_cmd(url, headers, command)
        
        # add to config.json
        name = command["name"]
        config["command_ids"][name] = registered_command[name]

        with open("config.json", "w") as config_file:
            json.dump(config, config_file, indent=4)
        
        sleep(5)

    print("Command IDs:")
    print(command_ids)

# setup the database
def setupdb():
    # connect to the CouchDB server
    server = couchdb.Server(Credentials.COUCHDB_CREDS.URL)
    server.resource.credentials = (Credentials.COUCHDB_CREDS.USERNAME, Credentials.COUCHDB_CREDS.PASSWORD)

    try:
        claiming_db = server.create("claiming")  # Create a new database if it doesn't exist
    except couchdb.http.PreconditionFailed:
        claiming_db = server["claiming"]  # Database already exists

    try:
        guilds_db = server.create("guilds")
    except couchdb.http.PreconditionFailed:
        guilds_db = server["guilds"]

    try:
        members_db = server.create("members")
    except couchdb.http.PreconditionFailed:
        members_db = server["members"]

    try:
        claims_waitlist_db = server.create("claims_waitlist")
    except couchdb.http.PreconditionFailed:
        claims_waitlist_db = server["claims_waitlist"]

    try:
        makeshift_onboarding_db = server.create("makeshift_onboarding")
    except couchdb.http.PreconditionFailed:
        makeshift_onboarding_db = server["makeshift_onboarding"]

    try:
        delete_queue_db = server.create("delete_queue")
    except couchdb.http.PreconditionFailed:
        delete_queue_db = server["delete_queue"]

    try:
        users_db = server.create("users")
    except couchdb.http.PreconditionFailed:
        users_db = server["users"]

    try:
        cross_verify_requests_db = server.create("cross_verify_requests")
    except couchdb.http.PreconditionFailed:
        cross_verify_requests_db = server["cross_verify_requests"]

    # I forgot why this is here ummm
    if version == None:
        update_db_docs.update_entire_db("claiming")

# list all commands then exit
def list_command_ids():
    import requests
    url = f"https://discord.com/api/v10/applications/{BotInfo.application_id}/commands"
    headers = {"Authorization": "Bot " + Credentials.TOKEN, "User-Agent": "DiscordBot CustomLibrary/v0.0", "Content-Type": "application/json"}
    r = requests.get(url, headers=headers)

    print(r)
    if r.status_code == 401:
        print("Your bot token was invalid, please set up the .env file first.")
        exit()

    commands = json.loads(r.text)
    command_ids = {}

    for command in commands:
        command_ids[command["name"]] = command["id"]

    print(command_ids)
    exit()

# update the DB version numbers in config
def update_db_versions():
    with open("config.json", "r") as config_file:
        config = json.load(config_file)

    config["db_versions"] = {
        "claiming": "2",
        "claims_waitlist": "3",
        "guilds": "7",
        "makeshift_onboarding": "1",
        "members": "4",
        "cross_verify_requests": "1",
        "users": "1"
    }

    with open("config.json", "w") as config_file:
        json.dump(config, config_file, indent=4)

# do stuff
if __name__ == "__main__":
    # parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("--include-test-commands", action="store_true", help="Registers test commands too. You probably shouldn't use this because they are not maintained.")
    parser.add_argument("--list-command-ids", action="store_true", help="Lists your bot's command IDs, useful if you forgot or didn't save them.")
    args = parser.parse_args()

    if args.list_command_ids: list_command_ids()

    # lock on running setup again
    lock_file = "setup.lock"
    if os.path.exists(lock_file):
        with open(lock_file, "r") as file:
            version = int(file.readline())
            if version == 5: # current version of setup
                print("The bot is already set up. There is no need to run this file again. But if you do want to run this again for any reason, you can delete the setup.lock file.")
                exit()
    else:
        version == "0"

    if not exists("config.json"): copy("config-template.json", "config.json")

    registercmds(args.include_test_commands)
    setupdb()
    update_db_versions()

    # create the lock file
    with open(lock_file, "w") as file:
        file.write("5")

    # update config version
    with open("config.json", "r") as config_file:
        config = json.load(config_file)

    config["config_version"] = 5

    with open("config.json", "w") as config_file:
        json.dump(config, config_file, indent=4)

    input("Done!\nPlease make sure to check config.json for extra Hanabot configuration. Press ENTER key to exit...")
