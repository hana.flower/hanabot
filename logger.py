from datetime import datetime
from time import time
from enum import Enum
import asyncio

log_file_path = f"logs/log-{round(time())}.txt"

# THANKS CHATGPT!!!!!!
from pathlib import Path

folder_path = "logs"

# Create a Path object
path = Path(folder_path)

# Check if the folder exists
if not path.exists():
    # If not, create the folder
    path.mkdir(parents=True, exist_ok=True)

# get time
async def get_current_time():
    # Get the current date and time
    current_time = datetime.now()

    # Format the current time as an ISO 8601 timestamp with milliseconds
    formatted_time = current_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")[:-4] # -4 index is to format it to ms

    return formatted_time

# enums...?
class ContextType(Enum):
    MISC = "Miscellaneous"
    INTERACTION = "Interaction"
    MESSAGE = "Message"
    NEW_MEMBER = "New Guild Member"
    GUILD = "Guild"

class HandlerType(Enum):
    MISC = "Miscellaneous"
    GATEWAY = "Gateway"
    HTTP = "HTTP API"

    # interaction handlers
    INTERACTION_AGEDOB = "Age & DOB Handler"
    INTERACTION_CLAIMING = "Claiming Handler"
    INTERACTION_COMMON = "Common Handler"
    INTERACTION_ONBOARDING_SETTINGS = "Onboarding Settings Handler"
    INTERACTION_ONBOARDING = "Onboarding Handler"
    INTERACTION_RULES = "Rules Handler"
    INTERACTION_SETTINGS = "Settings Handler"
    INTERACTION_WARNINGS = "Warnings Handler"
    INTERACTION_CROSS_VERIFY = "Cross-Verify Handler"

    # internal APIs
    API_CLAIMS = "Claims API"
    API_CLAIMS_WAITLIST = "Claims Waitlist API"
    API_GUILDS = "Guilds API"
    API_MEMBERS = "Members API"
    API_CROSS_VERIFY = "Cross-Verify API"
    API_USERS = "Users API"

# logger class in order to keep the "flow" (?) consistent
class Logger():
    def __init__(self, ID=None):
        self.ID = ID
        self.log_lock = asyncio.Lock()

    # log a message
    async def log(self, context_type, message, handler_type, ID=None, extra_data=None):
        if ID == None: ID = self.ID

        # fed my code to chatgpt and it told me that this would help keep the order and it seems to work
        async with self.log_lock:
            logged_message = await generate_context(context_type, message, handler_type, ID, extra_data)

            print(logged_message)
            with open(log_file_path, "a") as log_file:
                log_file.write(logged_message)

# generate the context
async def generate_context(context_type, message, handler_type, ID=None, extra_data=None):
    if ID != None:
        context = f"{ContextType(context_type).value} {ID}"
    else:
        context = f"{ContextType(context_type).value}"
    handler = f"{HandlerType(handler_type).value}"

    logged_message = f"[{await get_current_time()}] [{context}]   [{handler}]: {message}\n"
    return logged_message

# write log IF logger is not None
async def log(logger, context_type, message, handler_type, ID=None, extra_data=None):
    if logger == None: return
    else: await logger.log(context_type, message, handler_type, ID, extra_data)

# example usage and testing
async def main():
    await log(ContextType.INTERACTION, "Getting claims for member 545364944258990091 in guild 613425648685547541", HandlerType.GUILDS, ID="933795693162799156")

#asyncio.run(main())
