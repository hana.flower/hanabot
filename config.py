# config parser
# this is NOT the bot's config. that goes in config.json and .env

import json
import regex as re
from dotenv import dotenv_values
dotenv = dotenv_values(".env")

# however that could also be a command for bot owners to use

# functions for reloading the config
def load_config():
    with open("config.json", "r") as config_file:
        config = json.load(config_file)

        if int(config["config_version"]) < 4:
            new_config = False
            if int(config["config_version"]) < 2: # repeat for every that needs to be updates, similar to update db docs
                config["command_ids"]["reload-config"] = "0"
                new_config = True

            if int(config["config_version"]) < 3:
                config["default-presence"] = {
                    "since": "null", 
                    "activities": [{
                        "name": "Hanabot <3",
                        "type": 0,
                        "state": "https://hanabot.com",
                        "since": 1420070400000
                    }],
                    "status": "online",
                    "afk": False
                }
                new_config = True

            if int(config["config_version"]) < 4:
                config["db_versions"]["claims_waitlist"] = "3"

            if int(config["config_version"]) < 5:
                config["db_versions"]["guilds"] = "7"
                config["db_versions"]["members"] = "4"
                config["db_versions"]["cross_verify_requests"] = "1"
                config["db_versions"]["users"] = "1"
                
                config["command_ids"]["cross-verify"] = "0"

            config["config_version"] = 5
            with open("config.json", "w") as config_file:
                json.dump(config, config_file, indent=4)

            if new_config:
                input("New settings are available! Be sure to update config.json in order to ensure the bot works properly. Press ENTER key to exit...")
                exit()

    BotSettings.load_config(config)
    CommandSettingsGlobal.load_config(config)
    Emojis.load_config(config)

# config that doesn't need to be reloaded
with open("config.json", "r") as config_file:
    config = json.load(config_file)

# general bot settings
class BotSettings():
    @classmethod
    def load_config(cls, config):
        cls.default_presence = config["default-presence"]
        cls.dm_broadcast_channel_id = config["dm_broadcast_channel_id"] # we re-define the variable
        cls.owner_ids = config["owner_ids"]

# all individual feature settings classes
class WelcomingSettingsGlobal():
    @classmethod
    def load_config(cls, config):
        welcoming_config = config["welcoming"]
        cls.disable_editing_message = welcoming_config["disable_editing_message"]
        cls.welcome_on_account_kick = welcoming_config["welcome_on_account_kick"]

class CommandSettingsGlobal():
    welcoming = WelcomingSettingsGlobal # references to classes go outside the load config method

    @classmethod
    def load_config(cls, config):
        cls.welcoming.load_config(config) # and this is how we reload them

# emojis
class Emojis():
    @classmethod
    def load_config(cls, config):
        emojis_config = config["emojis"]
        cls.red_cross = emojis_config["red_cross"]
        cls.good_bot_reaction = emojis_config["good_bot_reaction"]
        cls.balls_reaction = emojis_config["balls_reaction"]

    @classmethod
    def get_emoji_reaction(cls, emoji):
        if emoji[:2] == "<:" and emoji[-1] == ">": # static custom emoji
            return emoji[1:-1]

        elif emoji[:3] == "<a:" and emoji[-1] == ">": # animated custom emoji
            return emoji[3:-1]

        else: return emoji # assume it's a unicode emoji. if not then skill issue

    @classmethod
    def get_emoji_id(cls, emoji):
        if emoji[0] == "<": # custom emoji
            match = re.search(r'<:\w+:(\d+)>', emoji)
            if match: return match.group(1)
        else: return None # unicode emojis have no ID

    @classmethod
    def get_emoji_name(cls, emoji):
        if emoji[0] == "<": # custom emoji
            match = re.search(r'<:(\w+):\d+>', emoji)
            if match: return match.group(1)
        else: return emoji # unicode emojis have the name as the emoji itself

    @classmethod
    def get_partial_emoji(cls, emoji):
        if emoji[0] == "<" and emoji[-1] == ">": # custom emoji
            return {"id": cls.get_emoji_id(emoji), "name": cls.get_emoji_name(emoji), "animated": emoji[1] == "a"}

        else: return {"name": emoji} # assume it's a unicode emoji. if not then skill issue

# constants
# command IDs
class CommandIDs():
    IDs = config["command_ids"]
    claiming = IDs["claiming"]
    help_command = IDs["help"]
    onboarding = IDs["onboarding"]
    reload_config = IDs["reload-config"]
    test = IDs["test"]
    test1 = IDs["test1"]
    settings = IDs["settings"]
    update_presence = IDs["update-presence"]
    warnings = IDs["warnings"]
    cross_verify = IDs["cross-verify"]

# db versions
class DBVersions():
    db_versions = config["db_versions"]
    claiming = db_versions["claiming"]
    claims_waitlist = db_versions["claims_waitlist"]
    guilds = db_versions["guilds"]
    makeshift_onboarding = db_versions["makeshift_onboarding"]
    members = db_versions["members"]
    cross_verify_requests = db_versions["cross_verify_requests"]
    users = db_versions["users"]

# secret stuff
class CouchDBCredentials():
    URL = dotenv["COUCHDB_IP"]
    USERNAME = dotenv["COUCHDB_USERNAME"]
    PASSWORD = dotenv["COUCHDB_PASSWORD"]

class Credentials():
    TOKEN = dotenv["TOKEN"]
    COUCHDB_CREDS = CouchDBCredentials()

class BotInfo():
    application_id = config["application_id"]
    # this is used as the version string currently
    try:
        from subprocess import check_output
        git_commit_hash_short = check_output(['git', 'rev-parse', '--short', 'HEAD']).decode('ascii').strip()
    except:
        print("Cannot get git commit hash/version string.")
        git_commit_hash_short = "Unknown"

load_config()
