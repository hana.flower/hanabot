# defining
import requests
import json
import regex as re
import asyncio
from datetime import datetime, timedelta, timezone
from time import time
from math import floor
from math import ceil
import cProfile
import pstats

# custom libraries
from config import *
import gateway
from gateway import websocket_connections
import http_api
import database
import tools
from logger import *

from api import claims as claims_api
from api import cross_verify as cross_verify_api
from api import guilds as guilds_api
from api import members as members_api

# all interaction handlers
from handlers.interactions import hb_common as common

from handlers.interactions import hb_agedob as agedob
from handlers.interactions import hb_claiming as claiming
from handlers.interactions import hb_onboarding as onboarding
from handlers.interactions import hb_rules as rules
from handlers.interactions import hb_settings as settings
from handlers.interactions import hb_warnings as warnings
from handlers.interactions import hb_onboarding_settings as onboarding_settings
from handlers.interactions import hb_cross_verify as cross_verify

# this is used for ban_list_automate
audit_log_entry_received = asyncio.Event()
audit_log_entry_messages = []

async def add_message_to_audit_log_queue(data):
    audit_log_entry_received.set()
    audit_log_entry_messages.append(data)

# interaction rate limiting
bot_rate_limits = []

class BotRateLimit:
    def __init__(self, limit_type, limit_length, scope, command_id=None):
        self.limit_type = limit_type
        if command_id is not None:
            self.command_id = command_id
        self.limited_at = int(time())
        self.reset_at = self.limited_at + limit_length
        self.scope = scope

    def is_rate_limit_exceeded(self, guild_id=None, user_id=None):
        if self.reset_at > time():
            print("Rate limit exceeded")
            if guild_id is not None and user_id is not None:
                if guild_id == self.scope.get("guild_id") and user_id == self.scope.get("user_id"):
                    print("if guild_id != self.scope.get(guild_id) or user_id != self.scope.get(user_id)")
                    return True
            if guild_id is not None and guild_id == self.scope.get("guild_id"):
                print("if guild_id is not None and guild_id != self.scope.get(guild_id)")
                return True
            if user_id is not None and user_id == self.scope.get("user_id"):
                print("if user_id is not None and user_id != self.scope.get(user_id)")
                return True
        return False

async def handle_interaction_rate_limits(command_id, interaction_id, interaction_token, guild_id=None, user_id=None):
    print(bot_rate_limits)
    for rate_limit in bot_rate_limits:
        if rate_limit.command_id == command_id and rate_limit.limit_type == "command":
            exceeded = False

            if guild_id is not None and user_id is not None:
                exceeded = rate_limit.is_rate_limit_exceeded(guild_id, user_id)
            if guild_id is not None:
                exceeded = rate_limit.is_rate_limit_exceeded(guild_id=guild_id)
            if user_id is not None:
                exceeded = rate_limit.is_rate_limit_exceeded(user_id=user_id)

            print(f"Rate limit exceeded: {exceeded}")
            if exceeded == True:
                payload = {
                    "type": 4,
                    "data": {
                        "content": f"This command is being rate limited. Please retry this command at <t:{rate_limit.reset_at}>.",
                        "flags": 64
                    }
                }
                http_api.create_interaction_response(interaction_id, interaction_token, payload)
                return True

async def add_to_rate_limit(limit_type, limit_length, scope, command_id):
    bot_rate_limits.append(BotRateLimit(limit_type, limit_length, scope, command_id=command_id))
    print(bot_rate_limits)


# the actual handling
# interactions
async def handle_interaction(data):
    interaction_id = data["id"]
    interaction_token = data["token"]

    logger = Logger(ID=interaction_id)

    # for slash commands
    if data["type"] == 2:
        command_id = data["data"]["id"]
        
        # get the options
        options_formatted = None
        if "options" in data["data"]:
            options_formatted = await tools.format_options(data["data"]["options"])
            options = options_formatted[0]
            subcommand_name = options_formatted[1]
            subcommand_group_name = options_formatted[2]

        # check if it's a DM
        if data["channel"]["type"] == 1:
            asyncio.create_task(log(logger, ContextType.INTERACTION, f"Received slash command {await tools.format_command(data['data'], options_formatted)} from user {data['user']['id']}.", HandlerType.GATEWAY))
        else:
            asyncio.create_task(log(logger, ContextType.INTERACTION, f"Received slash command ({await tools.format_command(data['data'], options_formatted)}) from user {data['member']['user']['id']} in guild {data['guild_id']}.", HandlerType.GATEWAY))

        # help
        if command_id == CommandIDs.help_command:
            await common.basic_ephemeral_response(interaction_id, interaction_token, "Check [here](https://gitlab.com/hana.flower/hanabot/-/wikis/home) for online help!")
            return

        # claiming
        if command_id == CommandIDs.claiming:
            # 1: claim
            if subcommand_name == "claim":
                await claiming.claim(data, interaction_id, interaction_token, options, logger=logger)
            # edit claim
            elif subcommand_name == "edit":
                await claiming.edit_claim(data, interaction_id, interaction_token, options, logger=logger)
            # 2: unclaiming
            elif subcommand_name == "leave":
                await claiming.unclaim(data, interaction_id, interaction_token, options, logger=logger)
            return

        # test
        elif command_id == CommandIDs.test:
            for websocket_info in websocket_connections["connections"].values():
                await gateway.send_voice_state_update(websocket_info["websocket"])
            return

        # 5: warnings
        elif command_id == CommandIDs.warnings:
            # check permissions
            if tools.check_permission(data["member"]["permissions"], 3) == False:
                asyncio.create_task(log(logger, ContextType.INTERACTION, "Warning failed because the user does not have the Ban Members permission.", HandlerType.INTERACTION_WARNINGS))
                await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Ban Members permission to use this command.")
                return
            
            # /warnings warn
            if subcommand_name == "warn":
                await warnings.warn(data, interaction_id, interaction_token, options, logger=logger)
            # /warnings list
            elif subcommand_name == "list":
                await warnings.warn_list_initial(data, interaction_id, interaction_token, options)
            # /warnings delete
            elif subcommand_name == "delete":
                await warnings.warn_delete(data, interaction_id, interaction_token, options)
            return

        # 7: settings
        elif command_id == CommandIDs.settings:
            # check for Manage Server permission
            if tools.check_permission(data["member"]["permissions"], 6) == False:
                await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Server permission to use this command.")
                return
            await settings.settings_menu_initial(data, interaction_id, interaction_token)
            return

        # 8: onboarding
        elif command_id == CommandIDs.onboarding:
            if await tools.is_bot_owner(data) == False:
                await common.basic_ephemeral_response(interaction_id, interaction_token, "This is currently disabled. Edit the DB entries yourself for now.")
                return
            
            if subcommand_group_name == "question":
                if subcommand_name == "list":
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You want to list all questions.")
                    return
                elif subcommand_name == "create":
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You want to create a question.")
                    return
                elif subcommand_name == "edit":
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You want to edit a question.")
                    return
                elif subcommand_name == "delete":
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You want to delete a question.")
                    return
            
            elif subcommand_group_name == "option":
                if subcommand_name == "list":
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You want to list all options for a question.")
                    return
                elif subcommand_name == "create":
                    if "emoji" in options:
                        # find emojis
                        re_rgi_emoji = re.compile(r'(?:\p{Emoji=Yes}|<a?:\w+:\d+>|<:\w+:\d+>)', re.UNICODE)
                        emojis = re.findall(re_rgi_emoji, options["emoji"])
                        
                        # appropriate response based on emojis found
                        if len(emojis) > 1:
                            await common.basic_ephemeral_response(interaction_id, interaction_token, "Please only input ONE emoji.")
                        elif len(emojis) <= 0:
                            await common.basic_ephemeral_response(interaction_id, interaction_token, "You didn't even input an emoji...")
                        else:
                            emoji = emojis[0]
                            if emoji != options["emoji"]:
                                await common.basic_ephemeral_response(interaction_id, interaction_token, "Please input ONLY an emoji.")
                            await common.basic_ephemeral_response(interaction_id, interaction_token, f"You want to create an option for a question with the emoji {emoji}.")
                        return
                    else:
                        await common.basic_ephemeral_response(interaction_id, interaction_token, f"You want to create an option for a question with no emoji.")

                elif subcommand_name == "edit":
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You want to edit an option for question.")
                    return
                elif subcommand_name == "delete":
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You want to delete an option for question.")
                    return

            #onboarding_settings.onboarding_settings_initial(data, interaction_id, interaction_token)

        # 11: test1
        elif command_id == CommandIDs.test1:
            if await tools.is_bot_owner(data) == False:
                await common.basic_ephemeral_response(interaction_id, interaction_token, "This is currently disabled.")
                return

            is_rate_limited = await handle_interaction_rate_limits(command_id, interaction_id, interaction_token, guild_id=data["guild_id"])
            if is_rate_limited == True: return

            payload = {
                "type": 5,
                "data": {
                    "flags": 64
                }
            }
            http_api.create_interaction_response(interaction_id, interaction_token, payload)

            test_guild = await guilds_api.get_guild_from_cache(data["guild_id"])

            payload = {
                "content": f"The guild is called {test_guild.name} and has the icon https://cdn.discordapp.com/icons/{test_guild.guild_id}/{test_guild.icon}.png and the claiming channel is <#{test_guild.hanabot.channels.claiming}>",
                "flags": 64
            }
            http_api.create_followup_message(interaction_token, payload)

        # update-presence
        elif command_id == CommandIDs.update_presence:
            if await tools.is_bot_owner(data) == False:
                await common.basic_ephemeral_response(interaction_id, interaction_token, "Only bot owners can use this command.")
                return
            else:
                if "options" not in data["data"]:
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You must specify some options.")
                    return
                activities = []
                activity = {}

                # get the info
                status = options.get("status")
                activity_type = options.get("activity-type")
                if activity_type == None: activity_type = -1
                activity_name = options.get("activity-name")
                activity_state = options.get("activity-state")
                activity_url = options.get("activity-url")

                # create activity dict
                if activity_type != None: activity["type"] = activity_type
                if activity_name != None: activity["name"] = activity_name
                if activity_state != None: activity["state"] = activity_state
                if activity_url != None: activity["url"] = activity_url

                if options.get("status") is None:
                    status = "" # default string to send just for the function, it will not do anything

                # no point in passing multiple activities to the gateway so I just only implemented one
                activities.append(activity)

                # check that it's valid
                # there is a bug here where activity type can be 0 and be a valid activity because of how python works
                # I tried my hardest but I couldn't figure out how to fix it LMAO
                if (not(activity_type) or activity_name or activity_state or activity_url) and (activity_type == -1 or activity_name == None):
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You must specify an activity type and name if you are sending an activity.")
                    return

                # send the update presence
                for websocket_info in websocket_connections["connections"].values():
                    await gateway.send_update_presence(websocket_info["shard"], status, activities)
                    
                await common.basic_ephemeral_response(interaction_id, interaction_token, "Done!")
                return

        # reload-config
        elif command_id == CommandIDs.reload_config:
            if await tools.is_bot_owner(data) == False:
                await common.basic_ephemeral_response(interaction_id, interaction_token, "Only bot owners can use this command.")
                return
            load_config() # reloads the config
            await common.basic_ephemeral_response(interaction_id, interaction_token, "Config reloaded!")

        # cross-verify
        elif command_id == CommandIDs.cross_verify:
            if subcommand_group_name == "servers":
                if tools.check_permission(data["member"]["permissions"], 6) == False:
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Server permission to use this command.")
                    return
                if subcommand_name == "add":
                    await cross_verify.add_guild(data, interaction_id, interaction_token, options, logger=logger)
                elif subcommand_name == "remove":
                    await cross_verify.remove_guild(data, interaction_id, interaction_token, options, logger=logger)
                elif subcommand_name == "list":
                    await cross_verify.list_guilds(data, interaction_id, interaction_token, logger=logger)
            
            elif subcommand_name == "verify":
                if tools.check_permission(data["member"]["permissions"], 29) == False:
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Roles permission to use this command.")
                    return
                # this is only disallowed to avoid people from removing their own CROSS_VERIFIED flag
                '''
                if options["member"] == data["member"]["user"]["id"]:
                    await common.basic_ephemeral_response(interaction_id, interaction_token, f"This feels a bit like cheating, doesn't it? Play by the rules and use </cross-verify claim:{CommandIDs.cross_verify}>")
                    return
                '''
                # if the user is not in the server
                if "members" not in data["data"]["resolved"]:
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You can't verify someone who is not in the server.")
                    return
                await cross_verify.verify_member(data, interaction_id, interaction_token, options, logger=logger)
            
            elif subcommand_name == "unverify":
                if tools.check_permission(data["member"]["permissions"], 29) == False:
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Roles permission to use this command.")
                    return
                '''
                if options["member"] == data["member"]["user"]["id"]:
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "Why would you ever want to do that?")
                    return
                '''
                await cross_verify.unverify_member(data, interaction_id, interaction_token, options, logger=logger)
            
            elif subcommand_name == "claim":
                await cross_verify.claim_verification(data, interaction_id, interaction_token, options, logger=logger)

            elif subcommand_name == "sync":
                if tools.check_permission(data["member"]["permissions"], 29) == False:
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Roles permission to use this command.")
                    return
                is_rate_limited = await handle_interaction_rate_limits(command_id, interaction_id, interaction_token, guild_id=data["guild_id"])
                if is_rate_limited == True: return
                await add_to_rate_limit("command", 604800, {"guild_id": data["guild_id"]}, command_id)

                asyncio.create_task(cross_verify.migrate_members(data, interaction_id, interaction_token, websocket_connections["connections"][0]["websocket"], logger=logger))


    # components
    elif data["type"] == 3:
        custom_id = data["data"]["custom_id"]

        # buttons
        if data["data"]["component_type"] == 2:
            # check if it's a DM
            if data["channel"]["type"] == 1:
                asyncio.create_task(log(logger, ContextType.INTERACTION, f"Received button ({custom_id}) from user {data['user']['id']}.", HandlerType.GATEWAY))
            else:
                asyncio.create_task(log(logger, ContextType.INTERACTION, f"Received button ({custom_id}) from user {data['member']['user']['id']} in guild {data['guild_id']}.", HandlerType.GATEWAY))

            # beaslavetohanainthelab
            # you can remove this if you want it's just for my lab
            if custom_id == "beaslavetohanainthelab":
                http_api.add_role("1137870796191178892", data["member"]["user"]["id"], "1155423793112694875", "wants to be my slave")
                edited_data = {"content": "Done! Oh yeah I was too lazy to implement a feature to take off the role and definitely don't just want to torture you so there we go LMAO"}
                http_api.edit_interaction_response_original(interaction_token, edited_data)
                #await common.basic_ephemeral_response(interaction_id, interaction_token, "Done! Oh yeah I was too lazy to implement a feature to take off the role and definitely don't just want to torture you so there we go LMAO")

            # age_and_dob_modal
            elif custom_id == "op6_agedob":
                await agedob.age_and_dob_modal(data, interaction_id, interaction_token, logger=logger)

            # 1: claim
            elif "op1_accept" in custom_id or "op1_decline" in custom_id:
                original_claim_request_message_id = data["message"]["id"]
                original_claim_request_message = json.loads(http_api.get_message(data["message"]["channel_id"], original_claim_request_message_id).text)
                
                # get claimed and master from custom id
                numbers_after_cid = re.search(r'cid(\d+)', custom_id)
                numbers_after_mid = re.search(r'mid(\d+)', custom_id)
                claimed = numbers_after_cid.group(1)
                master = numbers_after_mid.group(1)
                # check consent status
                if "op1_accept" in custom_id:
                    await claiming.claim_success(data, interaction_id, interaction_token, original_claim_request_message_id, claimed, master, logger=logger)
                else:
                    await claiming.claim_decline(data, interaction_id, interaction_token, original_claim_request_message_id, claimed, master, logger=logger)
            
            # 2: unclaim
            elif "op2_accept" in custom_id or "op2_decline" in custom_id:
                numbers_after_id = re.search(r'id(\d+)', custom_id)
                other_user_in_claim = numbers_after_id.group(1)
                # check consent status
                if "op2_accept" in custom_id:
                    await claiming.unclaim_yes(data, interaction_id, interaction_token, other_user_in_claim, logger=logger)
                else:
                    await claiming.unclaim_no(data, interaction_id, interaction_token, other_user_in_claim, logger=logger)

            # 3: rules
            # to-do: optimise it to pass the role ID in the custom ID
            elif "op3_agree" in custom_id or "op3_disagree" in custom_id:
                # sanity check
                guild_info = await guilds_api.get_guild(data["guild_id"])
                member_role = guild_info.roles.member
                if member_role in data["member"]["roles"]:
                    await common.basic_ephemeral_response(interaction_id, interaction_token, "You have already agreed to the rules.")
                    return

                # check consent status
                # this isn't actually agreeing to the rules, it just kicks the member
                if custom_id == "op3_agree":
                    await rules.rules_agree(data, interaction_id, interaction_token, guild_info, logger=logger)
                    return

                # actually agree to the rules
                elif "op3_disagree" in custom_id:
                    # to-do: make onboarding its own feature
                    # am lazy to do now
                    
                    await rules.rules_disagree(data, interaction_id, interaction_token, guild_info, logger=logger)
                    return
                    '''
                    # go to makeshift onboarding if it exists
                    # still messy but whatever only one server has onboarding
                    if guild_info.features.onboarding == True:
                        onboarding_data = database.get_onboarding(data["guild_id"])
                        await agedob.age_and_dob_modal(data, interaction_id, interaction_token) # age DOB modal
                        #await onboarding.makeshift_onboarding_message(data, interaction_id, interaction_token, onboarding_data)
                    else:
                        await rules.rules_disagree(data, interaction_id, interaction_token, guild_info)
                    '''

            # 4: makeshift onboarding
            # finish onboarding
            elif "op4_finish" in custom_id:
                await onboarding.finish_makeshift_onboarding(data, interaction_id, interaction_token, logger=logger)

            # warn list pages
            elif "op5_list" in custom_id:
                await warnings.warn_list_button(data, interaction_id, interaction_token)

            # settings
            elif "op7" in custom_id:
                # call settings module to take care of this button interaction
                await settings.handle_button(data, interaction_id, interaction_token)

            # onboarding settings
            elif "op8" in custom_id:
                # call onboarding module to take care of this button interaction
                onboarding_settings.handle_button(data, interaction_id, interaction_token)

            # cross-verify
            elif "op9" in custom_id:
                # verification requests
                if "op9_request" in custom_id:
                    if tools.check_permission(data["member"]["permissions"], 6) == False:
                        await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Server permission to use this command.")
                        return

                    # get claimed and master from custom id
                    numbers_after_rgid = re.search(r'rgid(\d+)', custom_id)
                    requesting_guild_id = numbers_after_rgid.group(1)
                    if "op9_request_accept" in custom_id:
                        await cross_verify.accept_guild_request(data, interaction_id, interaction_token, requesting_guild_id, logger=logger)
                    else:
                        await cross_verify.decline_guild_request(data, interaction_id, interaction_token, requesting_guild_id, logger=logger)
                
                # removing guild partnerships
                elif "op9_remove" in custom_id:
                    if tools.check_permission(data["member"]["permissions"], 6) == False:
                        await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Server permission to use this command.")
                        return

                    # get claimed and master from custom id
                    numbers_after_gid = re.search(r'gid(\d+)', custom_id)
                    other_guild_id = numbers_after_gid.group(1)
                    if "op9_remove_accept" in custom_id:
                        await cross_verify.accept_remove_guild(data, interaction_id, interaction_token, other_guild_id, logger=logger)
                    else:
                        await cross_verify.decline_remove_guild(data, interaction_id, interaction_token, other_guild_id, logger=logger)


        # string select menus
        elif data["data"]["component_type"] == 3:
            # check if it's a DM
            if data["channel"]["type"] == 1:
                asyncio.create_task(log(logger, ContextType.INTERACTION, f"Received select menu ({custom_id}) with values ({data['data']['values']}) from user {data['user']['id']}.", HandlerType.GATEWAY))
            else:
                asyncio.create_task(log(logger, ContextType.INTERACTION, f"Received select menu ({custom_id}) with values ({data['data']['values']}) from user {data['member']['user']['id']} in guild {data['guild_id']}.", HandlerType.GATEWAY))

            # 4: makeshift onboarding
            # select
            if "op4_select" in custom_id:
                await onboarding.makeshift_onboarding_role_select(data, interaction_id, interaction_token, logger=logger)

            # 7: settings
            # toggles
            if "op7" in custom_id:
                await settings.handle_select_menu(data, interaction_id, interaction_token)


        # role select menus
        elif data["data"]["component_type"] == 6:
            # check if it's a DM
            if data["channel"]["type"] == 1:
                asyncio.create_task(log(logger, ContextType.INTERACTION, f"Received role select menu ({custom_id}) with values ({data['data']['values']}) from user {data['user']['id']}.", HandlerType.GATEWAY))
            else:
                asyncio.create_task(log(logger, ContextType.INTERACTION, f"Received role select menu ({custom_id}) with values ({data['data']['values']}) from user {data['member']['user']['id']} in guild {data['guild_id']}.", HandlerType.GATEWAY))

            # 7: settings
            if "op7" in custom_id:
                await settings.handle_role_select_menu(data, interaction_id, interaction_token)


        # channel select menus
        elif data["data"]["component_type"] == 8:
            # check if it's a DM
            if data["channel"]["type"] == 1:
                asyncio.create_task(log(logger, ContextType.INTERACTION, f"Received channel select menu ({custom_id}) with values ({data['data']['values']}) from user {data['user']['id']}.", HandlerType.GATEWAY))
            else:
                asyncio.create_task(log(logger, ContextType.INTERACTION, f"Received channel select menu ({custom_id}) with values ({data['data']['values']}) from user {data['member']['user']['id']} in guild {data['guild_id']}.", HandlerType.GATEWAY))

            # 7: settings
            if "op7" in custom_id:
                await settings.handle_channel_select_menu(data, interaction_id, interaction_token)


    # modal submit
    elif data["type"] == 5:
        custom_id = data["data"]["custom_id"]

        # check if it's a DM
        if data["channel"]["type"] == 1:
            asyncio.create_task(log(logger, ContextType.INTERACTION, f"Received modal ({custom_id}) from user {data['user']['id']}.", HandlerType.GATEWAY))
        else:
            asyncio.create_task(log(logger, ContextType.INTERACTION, f"Received modal ({custom_id}) from user {data['member']['user']['id']} in guild {data['guild_id']}.", HandlerType.GATEWAY))

        # 6: age and DOB
        if custom_id == "op6_agedob":
            await agedob.age_and_dob_modal_submit(data, interaction_id, interaction_token, logger=logger)
            '''
            # thanks chatgpt
            # idk what this code does lol but probably not important
            for component in data.get("components", []):
                if component.get("type") == 1:
                    for nested_component in component.get("components", []):
                        custom_id = nested_component.get("custom_id")
                        value = nested_component.get("value")
            '''

        # 7: settings
        elif "op7" in custom_id:
            await settings.handle_modal_submit(data, interaction_id, interaction_token)

# getting message content and choosing what to handle
async def handle_message_create(data):
    content = data["content"]
    author_id = data["author"]["id"]
    channel_id = data["channel_id"]
    message_id = data["id"]

    mentions_bot = False

    # parse the mentions
    for mention_key in data["mentions"]:
        if mention_key.get("id") == BotInfo.application_id:
            mentions_bot = True
            break

    # don't want it getting into a loop
    if author_id == BotInfo.application_id:
        return

    # check if the message is a DM
    if "guild_id" not in data.keys():
        await handle_dm(data, channel_id, message_id, data["author"], content)

    # good bot
    if ("good bot" in content) and (mentions_bot):
        await handle_good_bot(data, channel_id, message_id)

    # balls
    elif "balls" in content.lower(): # you don't need to check for mentions bot unless you enable the message content intent
        await balls(data, channel_id, message_id)

# handle dms
async def handle_dm(data, channel_id, message_id, author, content):
    # broadcast the DM to a private channel and react
    http_api.send_message(BotSettings.dm_broadcast_channel_id, {"content": f"@everyone DM received from <@{author['id']}> {await tools.format_username(author)} (CID: {channel_id}): {content}"})
    http_api.create_reaction(channel_id, message_id, "👍")

# good bot
async def handle_good_bot(data, channel_id, message_id):
    http_api.create_reaction(channel_id, message_id, Emojis.get_emoji_reaction(Emojis.good_bot_reaction))
    http_api.send_message(channel_id, {
        "content": "<333",
        "message_reference": {"message_id": message_id}
    })

# balls
async def balls(data, channel_id, message_id):
    http_api.create_reaction(channel_id, message_id, Emojis.get_emoji_reaction(Emojis.balls_reaction))
    http_api.send_message(channel_id, {"content": "balls"})

async def get_ban_reason(guild_id, banned_user_id):
    # call this with asyncio.wait_for and a timeout
    while True:
        await audit_log_entry_received.wait()
        # the event was received
        audit_log_entry_received.clear()

        print(guild_id)
        print(banned_user_id)
        print(audit_log_entry_messages)

        for i, entry in enumerate(audit_log_entry_messages):
            print(entry)
            if entry["action_type"] == 22:
                print("Type is ban add")
                try:
                    if (entry["guild_id"] == guild_id) and (entry["target_id"] == banned_user_id):
                        audit_log_entry_messages.pop(0)
                        return entry.get("reason", None)
                except KeyError:
                    continue
            else:
                audit_log_entry_messages.pop(i)
                continue

# ban list automation
async def ban_list_automate(data):
    banned_user_id = data["user"]["id"]
    guild_id = data["guild_id"]

    reason = "Could not obtain reason"
    # audit log
    try:
        reason = await asyncio.wait_for(get_ban_reason(guild_id, banned_user_id), timeout=10)
        if reason is None:
            reason = "No reason provided"
    except asyncio.TimeoutError:
        print("Timeout occurred: Audit log entry not received within 10 seconds. Most likely, the bot is simply missing the permission.")

    guild_info = await guilds_api.get_guild(guild_id)

    ban_list_id = guild_info.channels.ban_list

    if ban_list_id == "0":
        return

    message_data = {
        "content": f"User <@{banned_user_id}> / {await tools.format_username(data['user'])} (ID {banned_user_id}) banned for reason:\n`{reason}`",
        "allowed_mentions": {
            "users": [banned_user_id]
        }
    }

    http_api.send_message(ban_list_id, message_data)

# account age check
async def kick_new_accounts(data):
    logger = Logger()
    guild_id = data["guild_id"]
    guild_info = await guilds_api.get_guild(guild_id)

    if guild_info.features.accountagecheck == False:
        return

    user_id = data["user"]["id"]

    snowflake_id = int(user_id)
    time_difference = int(round(time(), 3) * 1000) - ((snowflake_id >> 22) + 1420070400000)
    min_age = guild_info.min_acc_age
    reinvite_url = guild_info.reinvite_url

    # Check if the time difference is at least 24 hours
    if time_difference < (min_age * 3600000): # 1 hour in ms is 3600000 ms
        asyncio.create_task(log(logger, ContextType.NEW_MEMBER, f"User {user_id} will be kicked for being too young in guild {guild_id}.", HandlerType.MISC))

        if reinvite_url == "": # change message based on if the reinvite URL is set
            payload = {"content": f"Sorry, your account must be at least {min_age} hours old in order to join the server."}
        else:
            payload = {"content": f"Sorry, your account must be at least {min_age} hours old in order to join the server. You can rejoin later with this link: {reinvite_url}"}

        http_api.send_message(json.loads(http_api.create_dm(user_id).text)["id"], payload)
        http_api.remove_guild_member(guild_id, user_id, "Account is too young")

        return "Account kicked"

    else:
        return

# handle new accounts and welcome message
async def handle_new_accounts(data):
    guild_id = data["guild_id"]
    user_id = data["user"]["id"]
    status = await kick_new_accounts(data)

    # a very long one liner
    # it just checks if the welcome message should be sent based on bot settings
    if (status == "Account kicked" and CommandSettingsGlobal.welcoming.welcome_on_account_kick == False): return
    else:
        # welcome messages
        guild_info = await guilds_api.get_guild(guild_id)
        if guild_info.features.welcoming == True:
            message_data_unformatted = guild_info.messages.welcome
            if message_data_unformatted == "{}": return # no welcome message set
            message_data = tools.format_message(message_data_unformatted, "{user_id}", data["user"]["id"])
            http_api.send_message(guild_info.channels.welcome, message_data)

    # unschedule member for deletion
    mango_query = {
        "selector": {
            "data": {
                "user_id": user_id,
                "guild_id": guild_id
            }
        },
        "limit": 1
    }
    member_doc = database.get_doc("delete_queue", mango_query, skip_version_check=True)

    print(member_doc)

    if member_doc != "No documents found":
        await database.unschedule_doc_for_deletion(data={"user_id": user_id, "guild_id": guild_id})

# guild member remove event
async def handle_member_remove(data):
    # schedule member doc for deletion
    guild_id = data["guild_id"]
    user_id = data["user"]["id"]
    member_doc = await members_api.get_member(user_id, guild_id, return_doc=True, create_if_not_found=False)

    print(member_doc)

    if member_doc != None:
        database.schedule_doc_for_deletion(member_doc["_id"], "members", 3, data={"user_id": user_id, "guild_id": guild_id})
        if member_doc["verification_status"] != {}:
            await cross_verify_api.update_verification_flags(guild_id, user_id, [cross_verify_api.VerificationFlags.USER_LEFT])

    '''
    # schedule member claims for deletion
    await claims_api.get_claim()
    database.schedule_doc_for_deletion(member_doc[0], "claiming", 3)
    '''
