# custom libraries
import http_api
import database
import tools
from config import *
from logger import *
import gateway
from handlers.interactions import hb_common as common

from api import cross_verify as cross_verify_api
from api import guilds as guilds_api

# not custom libraries
import json
import asyncio
from time import time

# this is used for migrate_members
guild_members_chunk_received = asyncio.Event()
guild_members_chunk_messages = []

# 9: cross-verify
# request to a guild/accept a guild's request
async def add_guild(data, interaction_id, interaction_token, options, logger=None):
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    guild_id = data["guild_id"]
    requested_guild_id = options["server-id"] # the guild that partnership was requested with

    # no we're not having the same thing with claiming
    # I HAVE LEARNT MY LESSON HAHAHAHAHAHA
    if guild_id == requested_guild_id:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Cross-verify request failed because user tried to cross-verify with the same guild.", HandlerType.INTERACTION_CROSS_VERIFY))
        http_api.edit_interaction_response_original(interaction_token, {"content": "You can't cross-verify with yourself, what did you expect?\nFun fact: This used to cras- just kidding <3."})
        return

    # database
    guild_info = await guilds_api.get_guild(guild_id)
    requested_guild_info = await guilds_api.get_guild(requested_guild_id, create_if_not_found=False)

    if requested_guild_info == None:
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Cross-verify request failed because Hanabot is not a member of guild {guild_id}.", HandlerType.INTERACTION_CROSS_VERIFY))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Hanabot needs to be in both servers for cross-verifying to work."})
        return

    # sanity checks
    # is the feature enabled
    enabled = guild_info.features.crossverify
    if enabled == False:
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Cross-verify request failed because the feature is disabled in guild {guild_id}.", HandlerType.INTERACTION_CROSS_VERIFY))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Cross-verifying is not enabled in the current server."})
        return

    # is the feature enabled in the requested guild
    requested_enabled = guild_info.features.crossverify
    if enabled == False:
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Cross-verify request failed because the feature is disabled in the requested guild {guild_id}.", HandlerType.INTERACTION_CROSS_VERIFY))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Cross-verifying is not enabled in the requested server."})
        return

    # create or get the request
    verification_request = await cross_verify_api.get_verification_request(requested_guild_id, guild_id, create_if_not_found=False)
    print(verification_request)

    if verification_request != None:
        if verification_request.requesting_guild_id == requested_guild_id:
            asyncio.create_task(log(logger, ContextType.INTERACTION, "The cross-verify request already exists.", HandlerType.INTERACTION_CROSS_VERIFY))
            http_api.edit_interaction_response_original(interaction_token, {"content": f"A verification request to guild {verification_request.guild_id} already exists."})
        else:
            # not sure what case this is
            asyncio.create_task(log(logger, ContextType.INTERACTION, "The cross-verify request already exists, the other case idk", HandlerType.INTERACTION_CROSS_VERIFY))
            http_api.edit_interaction_response_original(interaction_token, {"content": f"Other case idk:\n\nA verification request to guild {verification_request.guild_id} already exists."})
    else:
        # check if the guild has an incoming request
        verification_request = await cross_verify_api.get_verification_request(guild_id, requested_guild_id, create_if_not_found=False)

        if verification_request != None:
            print(verification_request)
            asyncio.create_task(log(logger, ContextType.INTERACTION, "The cross-verify request exists and the user is being asked if they want to accept or decline the request.", HandlerType.INTERACTION_CROSS_VERIFY))
            # to-do: show a modal for the code if a request exists between these two guilds, and the current guild is not the requested one
            payload = {
                "content": f"Are you sure you would like to cross-verify with **{(await guilds_api.get_guild_from_cache(requested_guild_id)).name}**?",
                "components": [
                {
                    "type": 1,
                    "components": [
                        {
                            "type": 2,
                            "label": "Yes!",
                            "style": 3,
                            "custom_id": f"op9_request_accept_rgid{verification_request.requesting_guild_id}"
                        },
                        {
                            "type": 2,
                            "label": "No.",
                            "style": 4,
                            "custom_id": f"op9_request_decline_rgid{verification_request.requesting_guild_id}"
                        }
                    ]
                }
                ]
            }
            http_api.edit_interaction_response_original(interaction_token, payload)
        else:
            verification_request = await cross_verify_api.create_verification_request(requested_guild_id, guild_id)
            print(verification_request)
            asyncio.create_task(log(logger, ContextType.INTERACTION, "Cross-verify request created successfully!", HandlerType.INTERACTION_CROSS_VERIFY))
            http_api.edit_interaction_response_original(interaction_token, {"content": f"Successfully created verification request to guild {verification_request.guild_id}! It will expire in 24 hours.\n\nThe staff of the other server can accept this request using </cross-verify servers add:{CommandIDs.cross_verify}>, and you can cancel this request using </cross-verify servers remove:{CommandIDs.cross_verify}>."})

# accept button
async def accept_guild_request(data, interaction_id, interaction_token, requesting_guild_id, logger=None):
    # respond
    await common.basic_deferred_update_message(interaction_id, interaction_token)

    guild_id = data["guild_id"]

    status = await cross_verify_api.accept_verification_request(guild_id, requesting_guild_id)

    if status == True:
        message_data = {
            "content": f"~~{data['message']['content']}~~\nThe partnership was successful! 🎉",
            "components": [
            {
                "type": 1,
                "components": [
                    {
                        "type": 2,
                        "label": "Yes!",
                        "style": 3,
                        "custom_id": f"op9_request_decline_rgid_waitwhocaresthisbuttonisdisabled1",
                        "disabled": True
                    },
                    {
                        "type": 2,
                        "label": "No.",
                        "style": 4,
                        "custom_id": f"op9_request_decline_rgid_waitwhocaresthisbuttonisdisabled2",
                        "disabled": True
                    }
                ]
            }
            ]
        }
        http_api.edit_interaction_response_original(interaction_token, message_data)
    else:
        http_api.create_followup_message(interaction_token, {"content": "The request could not be accepted for some reason, todo: implement better error checking", "flags": 64})

# decline button
async def decline_guild_request(data, interaction_id, interaction_token, requesting_guild_id, logger=None):
    # respond
    await common.basic_deferred_update_message(interaction_id, interaction_token)

    guild_id = data["guild_id"]

    status = await cross_verify_api.decline_verification_request(guild_id, requesting_guild_id)

    if status == "Request deleted.":
        message_data = {
            "content": f"~~{data['message']['content']}~~\nThe partnership was declined. The other server will not be notified.",
            "components": [
            {
                "type": 1,
                "components": [
                    {
                        "type": 2,
                        "label": "Yes!",
                        "style": 3,
                        "custom_id": f"op9_request_decline_rgid_waitwhocaresthisbuttonisdisabled1",
                        "disabled": True
                    },
                    {
                        "type": 2,
                        "label": "No.",
                        "style": 4,
                        "custom_id": f"op9_request_decline_rgid_waitwhocaresthisbuttonisdisabled2",
                        "disabled": True
                    }
                ]
            }
            ]
        }
        http_api.edit_interaction_response_original(interaction_token, message_data)
    else:
        http_api.create_followup_message(interaction_token, {"content": "The request could not be declined for some reason, todo: implement better error checking", "flags": 64})

# remove a guild
async def remove_guild(data, interaction_id, interaction_token, options, logger=None):
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    guild_id = data["guild_id"]
    other_guild_id = options["server-id"] # the guild that partnership was requested with

    # no we're not having the same thing with claiming
    # I HAVE LEARNT MY LESSON HAHAHAHAHAHA
    if guild_id == other_guild_id:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Cross-verify remove failed because user tried to cross-verify with the same guild.", HandlerType.INTERACTION_CROSS_VERIFY))
        http_api.edit_interaction_response_original(interaction_token, {"content": "You can't cross-verify with yourself, what did you expect?\nFun fact: This used to cras- just kidding <3."})
        return

    # database
    guild_info = await guilds_api.get_guild(guild_id)
    other_guild_info = await guilds_api.get_guild(other_guild_id, create_if_not_found=False)

    # sanity checks
    # actually the feature enabled things don't seem needed, it's mainly to block incoming requests

    # get the request
    verification_request = await cross_verify_api.get_verification_request(other_guild_id, guild_id, create_if_not_found=False)
    print(verification_request)

    if other_guild_id not in guild_info.cross_verify_guilds and verification_request == None:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Cross-verify remove failed because the guilds are not partnered.", HandlerType.INTERACTION_CROSS_VERIFY))
        http_api.edit_interaction_response_original(interaction_token, {"content": "You are not partnered with that server and you do not have an outgoing request."})
        return

    if verification_request != None:
        if verification_request.guild_id == other_guild_id:
            # to-do: show a similar message to /claiming leave (lazy to do rn)
            asyncio.create_task(log(logger, ContextType.INTERACTION, "Not yet implemented.", HandlerType.INTERACTION_CROSS_VERIFY))
            http_api.edit_interaction_response_original(interaction_token, {"content": "Message not yet implemented. Try to add the server first, and then you should get the option to decline."})
            return

    # all checks passed
    asyncio.create_task(log(logger, ContextType.INTERACTION, "The cross-verify request exists and the user is being asked if they want to accept or decline the request.", HandlerType.INTERACTION_CROSS_VERIFY))
    # to-do: show a modal for the code if a request exists between these two guilds, and the current guild is not the requested one
    payload = {
        "content": f"Are you sure you would like to stop your cross-verify partnership with **{(await guilds_api.get_guild_from_cache(other_guild_id)).name}**?\nMembers from this server will **NOT** have their verification removed.",
        "components": [
        {
            "type": 1,
            "components": [
                {
                    "type": 2,
                    "label": "Yes!",
                    "style": 3,
                    "custom_id": f"op9_remove_accept_gid{other_guild_id}"
                },
                {
                    "type": 2,
                    "label": "No.",
                    "style": 4,
                    "custom_id": f"op9_remove_decline_gid{other_guild_id}"
                }
            ]
        }
        ]
    }
    http_api.edit_interaction_response_original(interaction_token, payload)

# accept button
async def accept_remove_guild(data, interaction_id, interaction_token, requesting_guild_id, logger=None):
    # respond
    await common.basic_deferred_update_message(interaction_id, interaction_token)

    guild_id = data["guild_id"]

    status = await cross_verify_api.remove_verification_partnership(guild_id, requesting_guild_id)

    if status == "Partnership removed.":
        message_data = {
            "content": f"~~{data['message']['content']}~~\nSuccessfully removed partnership.",
            "components": [
            {
                "type": 1,
                "components": [
                    {
                        "type": 2,
                        "label": "Yes!",
                        "style": 3,
                        "custom_id": f"op9_remove_accept_gidwaitwhocaresthisbuttonisdisabled1",
                        "disabled": True
                    },
                    {
                        "type": 2,
                        "label": "No.",
                        "style": 4,
                        "custom_id": f"op9_remove_decline_gidwaitwhocaresthisbuttonisdisabled2",
                        "disabled": True
                    }
                ]
            }
            ]
        }
        http_api.edit_interaction_response_original(interaction_token, message_data)
    else:
        http_api.create_followup_message(interaction_token, {"content": "The request could not be accepted for some reason, todo: implement better error checking", "flags": 64})

# decline button
async def decline_remove_guild(data, interaction_id, interaction_token, requesting_guild_id, logger=None):
    payload = {
        "type": 7,
        "data": {
            "content": f"~~{data['message']['content']}~~\nDeclined. This partnership will continue as normal.",
            "components": [
            {
                "type": 1,
                "components": [
                    {
                        "type": 2,
                        "label": "Yes!",
                        "style": 3,
                        "custom_id": f"op9_remove_accept_gidwaitwhocaresthisbuttonisdisabled1",
                        "disabled": True
                    },
                    {
                        "type": 2,
                        "label": "No.",
                        "style": 4,
                        "custom_id": f"op9_remove_decline_gidwaitwhocaresthisbuttonisdisabled2",
                        "disabled": True
                    }
                ]
            }
            ]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# list partnered guilds
async def list_guilds(data, interaction_id, interaction_token, logger=None):
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    guild_id = data["guild_id"]
    guild_info = await guilds_api.get_guild(guild_id)
    cross_verify_guilds = guild_info.cross_verify_guilds
    partnered_guild_names = ""

    for guild_id in cross_verify_guilds:
        guild_name = (await guilds_api.get_guild_from_cache(guild_id)).name
        print(guild_name)
        print(type(guild_name))
        name = f"* {guild_name}\n"
        partnered_guild_names += name

    if partnered_guild_names == "": partnered_guild_names = "This server is not partnered with any other servers."
    
    field = {
        "name": "Servers",
        "value": partnered_guild_names
    }

    embed = {
        "title": "Partnered servers",
        "description": "Your members can cross-verify from each of these servers, and their members can cross-verify from this server.",
        "color": 15502719,
        #"footer": {"text": f"The user will be banned after {warn_limit} warns."},
        "fields": [field]
    }

    payload = {
        "embeds": [embed]
    }
    http_api.edit_interaction_response_original(interaction_token, payload)

# staff verifying a member
async def verify_member(data, interaction_id, interaction_token, options, logger=None):
    # permission check is done initially in handling.py
    # respond
    guild_id = data["guild_id"]
    user_id = options["member"]
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)
    status = await cross_verify_api.verify_member(guild_id, user_id, 0, remove_cross_verified=True, eligibility=await cross_verify_api.check_verification_eligibility(guild_id, user_id))
    http_api.edit_interaction_response_original(interaction_token, {"content": "The user has been verified."})

# claiming your own verification
async def claim_verification(data, interaction_id, interaction_token, options, logger=None):
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    guild_id = data["guild_id"]
    user_id = data["member"]["user"]["id"]

    # check eligibility
    eligibility = await cross_verify_api.check_verification_eligibility(guild_id, user_id)

    # eligibility structure: (is_eligible, is_cross_verification, has_previously_verified)

    if eligibility[0] == False:
        http_api.edit_interaction_response_original(interaction_token, {"content": "You are not eligible to cross-verify in this server."})
    else:
        # generate flags
        flags = 0
        if eligibility[1] == True:
            flags += cross_verify_api.VerificationFlags.CROSS_VERIFIED.value

        await cross_verify_api.verify_member(guild_id, user_id, flags, remove_cross_verified=False, eligibility=eligibility)
        if eligibility[1] == True:
            if eligibility[2] == False:
                http_api.edit_interaction_response_original(interaction_token, {"content": "Congratulations, you have cross-verified!"})
            else:
                http_api.edit_interaction_response_original(interaction_token, {"content": "You have already cross-verified in here before, so your verification has been restored."})
        else:
            http_api.edit_interaction_response_original(interaction_token, {"content": "You have already verified in here before, so your verification has been restored."})

# unverifying a member
async def unverify_member(data, interaction_id, interaction_token, options, logger=None):
    # permission check is done initially in handling.py
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    guild_id = data["guild_id"]
    user_id = options["member"]
    reason = options["reason"]
    status = await cross_verify_api.unverify_member(guild_id, user_id, reason)

    if status[0] == True:
        http_api.edit_interaction_response_original(interaction_token, {"content": "The user has been unverified."})
    else:
        if status[2] in ["Already unverified", "Verification not found"]:
            http_api.edit_interaction_response_original(interaction_token, {"content": "The user is not verified."})

# migrate members who have a role to being registered with hanabot
async def migrate_members(data, interaction_id, interaction_token, websocket, logger=None):
    global guild_members_chunk_message
    print("Migrating members")

    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    guild_id = data["guild_id"]
    # get guild info
    guild_info = await guilds_api.get_guild(guild_id)

    # request guild members
    await gateway.send_request_guild_members(websocket, data["guild_id"])

    received_chunks_count = 0
    chunk_count = 999999 # set it to a really high number to start off, then update later

    while received_chunks_count < chunk_count - 1:
        await guild_members_chunk_received.wait()
        guild_members_chunk_received.clear()

        chunk_data = guild_members_chunk_messages[0]
        if chunk_data["guild_id"] == guild_id:
            guild_members_chunk_messages.pop(0)
            print("Seen members chunk")
            received_chunks_count += 1
            chunk_count = chunk_data["chunk_count"]
            members = chunk_data["members"]

            for member in members:
                if guild_info.roles.major_verified in member["roles"]:
                    await cross_verify_api.verify_member(guild_id, member["user"]["id"], 0) # this will not log the verification
                    print(member["user"]["id"])

    http_api.edit_interaction_response_original(interaction_token, {"content": "Done!"})
