# custom libraries
import http_api
import database
import tools
from logger import *
from handlers.interactions import hb_common as common
from handlers.interactions import hb_onboarding as onboarding

from api import guilds as guilds_api

import asyncio

# uh
from datetime import datetime
# thanks chatgpt
async def calc_placeholder_age():
    birthdate = datetime(1998, 8, 9) # yyyy/mm/dd
    current_date = datetime.now()
    years_difference = current_date.year - birthdate.year
    if current_date.month < birthdate.month or (current_date.month == birthdate.month and current_date.day < birthdate.day):
        years_difference -= 1
    return years_difference

# 6: age and DOB
async def age_and_dob_modal(data, interaction_id, interaction_token, logger=None):
    payload = {
        "type": 9,
        "data": {
            "title": "Enter your age and DOB.",
            "custom_id": "op6_agedob",
            "components": [
                {
                    "type": 1,
                    "components": [
                        {
                            "type": 4,
                            "custom_id": "op6_agedob_modal_age",
                            "label": "Age",
                            "style": 1,
                            "min_length": 1,
                            "max_length": 3,
                            "placeholder": f"e.g. {await calc_placeholder_age()}",
                            "required": True
                        }
                    ]
                },
                {
                    "type": 1,
                    "components": [
                        {
                            "type": 4,
                            "custom_id": "op6_agedob_modal_DOB",
                            "label": "DOB (in EU format, dd/mm/yyyy)",
                            "style": 1,
                            "min_length": 1,
                            "max_length": 10,
                            "placeholder": "e.g. 9/8/1998 (for August 9th 1998)",
                            "required": True
                        }
                    ]
                }
            ]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)
    asyncio.create_task(log(logger, ContextType.INTERACTION, "Age and DOB modal opened.", HandlerType.INTERACTION_AGEDOB))

# 6: age and DOB modal submit
async def age_and_dob_modal_submit(data, interaction_id, interaction_token, logger=None):
    # database
    guild_id = data["guild_id"]
    guild_info = await guilds_api.get_guild(guild_id)
    member_age_broadcast_cid = guild_info.channels.member_age_broadcast

    # iterate through action rows and their components
    for action_row in data["data"]["components"]:
        for component in action_row["components"]:
            if component["custom_id"] == "op6_agedob_modal_age":
                age = component["value"]
            elif component["custom_id"] == "op6_agedob_modal_DOB":
                dob = component["value"]
    
    ID = data["member"]["user"]["id"]

    # notify staff
    message_data = {
        "content": f"<@{ID}> is {age} years old and was born on {dob} (should be in EU format, dd/mm/yyyy)",
        "allowed_mentions": {"users": [ID]}
    }

    if member_age_broadcast_cid != "0": http_api.send_message(member_age_broadcast_cid, message_data)
    
    # to-do: make this edit the new privacy warning (iirc this requires a lot more work than I can be bothered to do because it's a modal)
    if guild_info.features.onboarding == True:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Age and DOB modal submitted and the user has started makeshift onboarding.", HandlerType.INTERACTION_AGEDOB))
        onboarding_data = database.get_onboarding(data["guild_id"])
        await onboarding.makeshift_onboarding_message(data, interaction_id, interaction_token, onboarding_data)
    else:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Age and DOB modal submitted and the user has been assigned the member role.", HandlerType.INTERACTION_AGEDOB))
        http_api.add_role(data["guild_id"], data["member"]["user"]["id"], guild_info.roles.member, "Accepted rules")
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Welcome in!")
