# custom libraries
from config import *
import gateway
import http_api
import database
import tools
from logger import *
from handlers.interactions import hb_common as common

from api import guilds as guilds_api

# not custom libraries
import json
from copy import copy
import regex as re
import asyncio

# this doesn't really need logging past the base interaction logging
# I also just can't be bothered
# to-do: a lot

# generate the footer of the menu
async def gen_footer_action_row(help_url, custom_id=None):
    footer_action_row = {
        "type": 1,
        "components": [
        {
            "type": 2,
            "label": f"Bot version {BotInfo.git_commit_hash_short}",
            "style": 5,
            "url": "https://gitlab.com/hana.flower/hanabot"
        },
        {
            "type": 2,
            "label": "Help!",
            "style": 5,
            "url": help_url
        }]
    }
    if custom_id != None:
        # this little script will always return a correct custom_id
        # op7_3screening_1accage_1minage
        # becomes
        # op7_3screening_1accage

        back_custom_id = ""
        custom_id_list = custom_id.split("_")

        # a few cases where we need to ignore parts of the custom ID
        # this is to make responses to the select menus work
        if custom_id_list[-1] in ["menu", "modal", "set"]:
            custom_id_list.pop(-1)

        for item in custom_id_list[:-1]:
            back_custom_id += (item + "_")
        back_custom_id = back_custom_id[:-1]

        # custom custom ID for the beginning of the settings menus
        if back_custom_id == "op7": back_custom_id = "op7_start"

        footer_action_row["components"].append({
            "type": 2,
            "label": "Back",
            "emoji": {"name": "⬅️"},
            "style": 4,
            "custom_id": back_custom_id
        })
    return footer_action_row

# handle button
async def handle_button(data, interaction_id, interaction_token):
    # check for Manage Server permission
    if tools.check_permission(data["member"]["permissions"], 6) == False:
        await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Server permission to use this command.")
        return

    custom_id = data["data"]["custom_id"]
    # back button going to the start
    if custom_id == "op7_start":
        await settings_menu_initial(data, interaction_id, interaction_token, edit_msg=True)

    # claiming
    elif "op7_1claiming" in custom_id:

        # base
        if custom_id == "op7_1claiming":
            await settings_menu_claiming(data, interaction_id, interaction_token, custom_id=custom_id)
            return # returning every time is faster, right?
        
        # claiming
        if custom_id == "op7_1claiming_1claimedrole":
            await settings_menu_claiming_claimedrole(data, interaction_id, interaction_token, custom_id=custom_id)
            return # right...
        elif custom_id == "op7_1claiming_2masterrole":
            await settings_menu_claiming_masterrole(data, interaction_id, interaction_token, custom_id=custom_id)
            return # well it should be
        elif custom_id == "op7_1claiming_3claimlistchannel":
            await settings_menu_claiming_claimlist(data, interaction_id, interaction_token, custom_id=custom_id)
            return # either way more whitespace = better
        elif custom_id == "op7_1claiming_4claimingchannel":
            await settings_menu_claiming_claiming(data, interaction_id, interaction_token, custom_id=custom_id)
            return
        elif custom_id == "op7_1claiming_5claimlimit":
            await settings_menu_claiming_claimlimit(data, interaction_id, interaction_token)
            return

    # moderation
    elif "op7_2moderation" in custom_id:
        # base
        if custom_id == "op7_2moderation":
            await settings_menu_moderation(data, interaction_id, interaction_token, custom_id=custom_id)
            return

        # warns
        elif "op7_2moderation_1warns" in custom_id:
            # base
            if custom_id == "op7_2moderation_1warns":
                await settings_menu_moderation_warns(data, interaction_id, interaction_token, custom_id=custom_id)
                return
            # warn list
            elif custom_id == "op7_2moderation_1warns_1warnlist":
                await settings_menu_moderation_warns_list(data, interaction_id, interaction_token, custom_id=custom_id)
                return
            # warn limit
            elif custom_id == "op7_2moderation_1warns_2warnlimit":
                await settings_menu_moderation_warns_limit(data, interaction_id, interaction_token)
                return

        # ban list
        elif custom_id == "op7_2moderation_2banlist":
            await settings_menu_moderation_banlist(data, interaction_id, interaction_token, custom_id=custom_id)
            return

    # member screening
    elif "op7_3screening" in custom_id:
        # base
        if custom_id == "op7_3screening":
            await settings_menu_screening(data, interaction_id, interaction_token, custom_id=custom_id)
            return

        # acc age
        elif "op7_3screening_1accage" in custom_id:
            # base
            if custom_id == "op7_3screening_1accage":
                await settings_menu_screening_accage(data, interaction_id, interaction_token, custom_id=custom_id)
                return
            
            elif custom_id == "op7_3screening_1accage_1minage":
                await settings_menu_screening_accage_minage(data, interaction_id, interaction_token)
                return

        # rules
        elif "op7_3screening_2rules" in custom_id:
            # base
            if custom_id == "op7_3screening_2rules":
                await settings_menu_screening_rules(data, interaction_id, interaction_token, custom_id=custom_id)
                return

            elif custom_id == "op7_3screening_2rules_1swapbuttons":
                await settings_menu_screening_rules_swapbuttons(data, interaction_id, interaction_token)
                return
            elif custom_id == "op7_3screening_2rules_2sendmsg":
                await settings_menu_screening_rules_sendmsg(data, interaction_id, interaction_token)
                return
            elif custom_id == "op7_3screening_2rules_3memberrole":
                await settings_menu_screening_rules_memberrole(data, interaction_id, interaction_token)
                return

        # re-invite url
        elif custom_id == "op7_3screening_3reinviteurl":
            await settings_menu_screening_reinviteurl(data, interaction_id, interaction_token)
            return

        # member age check
        elif "op7_3screening_4memberagecheck" in custom_id:
            # base
            if custom_id == "op7_3screening_4memberagecheck":
                await settings_menu_screening_memberage(data, interaction_id, interaction_token, custom_id=custom_id)
                return

            elif custom_id == "op7_3screening_4memberagecheck_1channel":
                await settings_menu_screening_memberage_channel(data, interaction_id, interaction_token, custom_id=custom_id)
                return

    # welcoming
    elif "op7_4welcoming" in custom_id:
        # base
        if custom_id == "op7_4welcoming":
            await settings_menu_welcoming(data, interaction_id, interaction_token, custom_id=custom_id)
            return
        elif custom_id == "op7_4welcoming_1message":
            if CommandSettingsGlobal.welcoming.disable_editing_message == True:
                await common.basic_ephemeral_response(interaction_id, interaction_token, "Editing the welcome message has been disabled by a bot owner.")
                return
            else: await settings_menu_welcoming_message(data, interaction_id, interaction_token)
            return
        elif custom_id == "op7_4welcoming_2welcoming":
            await settings_menu_welcoming_welcoming(data, interaction_id, interaction_token, custom_id=custom_id)
            return
        elif custom_id == "op7_4welcoming_3testmessage":
            await settings_menu_welcoming_testmessage(data, interaction_id, interaction_token)
            return

    # toggle features
    elif "op7_5toggle" in custom_id:
        # base
        if custom_id == "op7_5toggle":
            await settings_menu_toggle(data, interaction_id, interaction_token, custom_id=custom_id)
            return

    # cross-verify
    elif "op7_6crossverify" in custom_id:
        # base
        if custom_id == "op7_6crossverify":
            await settings_menu_crossverify(data, interaction_id, interaction_token, custom_id=custom_id)
            return

        elif custom_id == "op7_6crossverify_1majorrole":
            await settings_menu_crossverify_majorrole(data, interaction_id, interaction_token, custom_id=custom_id)
            return

        elif custom_id == "op7_6crossverify_2logchannel":
            await settings_menu_crossverify_logchannel(data, interaction_id, interaction_token, custom_id=custom_id)
            return # right...

# string select menus
async def handle_select_menu(data, interaction_id, interaction_token):
    # check for Manage Server permission
    if tools.check_permission(data["member"]["permissions"], 6) == False:
        await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Server permission to use this command.")
        return
    
    custom_id = data["data"]["custom_id"]
    if custom_id == "op7_5toggle_menu":
        await settings_togglefeature(data, interaction_id, interaction_token, custom_id=custom_id)

# role select menus
async def handle_role_select_menu(data, interaction_id, interaction_token):
    # check for Manage Server permission
    if tools.check_permission(data["member"]["permissions"], 6) == False:
        await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Server permission to use this command.")
        return
    
    custom_id = data["data"]["custom_id"]
    # set claimed
    if custom_id == "op7_1claiming_1claimedrole_set":
        await settings_setclaimedrole(data, interaction_id, interaction_token, custom_id=custom_id)
    # set master
    elif custom_id == "op7_1claiming_2masterrole_set":
        await settings_setmasterrole(data, interaction_id, interaction_token, custom_id=custom_id)
    # set member role
    elif custom_id == "op7_3screening_2rules_3memberrole_set":
        await settings_setmemberrole(data, interaction_id, interaction_token, custom_id=custom_id)
    # set 18+ verified role
    elif custom_id == "op7_6crossverify_1majorrole_set":
        await settings_setmajorverifiedrole(data, interaction_id, interaction_token, custom_id=custom_id)

# channel select menus
async def handle_channel_select_menu(data, interaction_id, interaction_token):
    # check for Manage Server permission
    if tools.check_permission(data["member"]["permissions"], 6) == False:
        await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Server permission to use this command.")
        return
    
    custom_id = data["data"]["custom_id"]
    # set claim list
    if custom_id == "op7_1claiming_3claimlist_set":
        await settings_setclaimlist(data, interaction_id, interaction_token, custom_id=custom_id)
    # set claiming
    elif custom_id == "op7_1claiming_4claiming_set":
        await settings_setclaiming(data, interaction_id, interaction_token, custom_id=custom_id)
    # set warn list
    elif custom_id == "op7_2moderation_1warns_1warnlist_set":
        await settings_setwarnlist(data, interaction_id, interaction_token)
    # set ban list
    elif custom_id == "op7_2moderation_2banlist_set":
        await settings_setbanlist(data, interaction_id, interaction_token)
    # send rules msg
    elif custom_id == "op7_3screening_2rules_2sendmsg_set":
        await settings_sendrulesmsg(data, interaction_id, interaction_token)
    # set member age broadcast
    elif custom_id == "op7_3screening_4memberagecheck_1channel_set":
        await settings_setmemberagebroadcast(data, interaction_id, interaction_token)
    # set welcoming
    elif custom_id == "op7_4welcoming_2welcoming_set":
        await settings_setwelcoming(data, interaction_id, interaction_token)
    # set verification log
    elif custom_id == "op7_6crossverify_2logchannel_set":
        await settings_setverificationlog(data, interaction_id, interaction_token, custom_id=custom_id)

# modal submits
async def handle_modal_submit(data, interaction_id, interaction_token):
    # check for Manage Server permission 
    if tools.check_permission(data["member"]["permissions"], 6) == False:
        await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Manage Server permission to use this command.")
        return
    
    custom_id = data["data"]["custom_id"]
    # set claim limit
    if custom_id == "op7_1claiming_5claimlimit_modal":
        await settings_setclaimlimit(data, interaction_id, interaction_token)
    # set warn limit
    elif custom_id == "op7_2moderation_1warns_2warnlimit_modal":
        await settings_setwarnlimit(data, interaction_id, interaction_token)
    # set minimum account age
    elif custom_id == "op7_3screening_1accage_1minage_modal":
        await settings_setminaccage(data, interaction_id, interaction_token)
    # set re-invite
    elif custom_id == "op7_3screening_3reinviteurl_modal":
        await settings_setreinviteurl(data, interaction_id, interaction_token)
    # set welcome msg
    elif custom_id == "op7_4welcoming_1message_modal":
        await settings_setwelcomingmessage(data, interaction_id, interaction_token) 


# 7: settings
async def settings_menu_initial(data, interaction_id, interaction_token, edit_msg=False):
    payload = {
        "type": 4,
        "data": {
            "content": "## Hanabot settings\n\nModify the bot's settings for this server.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Claiming",
                    "style": 1,
                    "custom_id": "op7_1claiming"
                },
                {
                    "type": 2,
                    "label": "Moderation",
                    "style": 1,
                    "custom_id": "op7_2moderation"
                },
                {
                    "type": 2,
                    "label": "Member screening",
                    "style": 1,
                    "custom_id": "op7_3screening"
                },
                {
                    "type": 2,
                    "label": "Welcoming",
                    "style": 1,
                    "custom_id": "op7_4welcoming"
                },
                {
                    "type": 2,
                    "label": "Toggle features",
                    "style": 1,
                    "custom_id": "op7_5toggle"
                }]
            },{ # A SECOND ACTION ROW WTF
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Cross-verification",
                    "style": 1,
                    "custom_id": "op7_6crossverify"
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Commands/Settings")] # how is this valid syntax??? anyway, it appends the footer action row to the message
        }
    }
    if edit_msg == True: payload["type"] = 7 # UPDATE_MESSAGE
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 7.1: claiming settings
async def settings_menu_claiming(data, interaction_id, interaction_token, custom_id=None):
    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Claiming\n\nChange claiming settings.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Set claimed role",
                    "style": 1,
                    "custom_id": "op7_1claiming_1claimedrole"
                },
                {
                    "type": 2,
                    "label": "Set master role",
                    "style": 1,
                    "custom_id": "op7_1claiming_2masterrole"
                },
                {
                    "type": 2,
                    "label": "Set claim list channel",
                    "style": 1,
                    "custom_id": "op7_1claiming_3claimlistchannel"
                },
                {
                    "type": 2,
                    "label": "Set claiming channel",
                    "style": 1,
                    "custom_id": "op7_1claiming_4claimingchannel"
                },
                {
                    "type": 2,
                    "label": "Set claim limit",
                    "style": 1,
                    "custom_id": "op7_1claiming_5claimlimit"
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 7.1.1 set claimed role
async def settings_menu_claiming_claimedrole(data, interaction_id, interaction_token, custom_id=None):
    # get info from DB to auto-fill
    guild_info = await guilds_api.get_guild(data["guild_id"])
    claimed_role = guild_info.roles.claimed

    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Claiming  >  Set claimed role\n\nSet the role given to members when they are claimed by someone else.\n\nYou may need to type the role's name if it doesn't appear in the dropdown.",
            "flags": 64,
            "allowed_mentions": {"parse": []},
            "components": [{
                "type": 1,
                "components": [{
                    "type": 6,
                    "placeholder": "Select a role",
                    "custom_id": "op7_1claiming_1claimedrole_set",
                    "min_values": 0,
                    "default_values": [{
                        "id": claimed_role,
                        "type": "role"
                    }]
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_setclaimedrole(data, interaction_id, interaction_token, custom_id=None):
    try:
        role_id = data["data"]["values"][0]
    except IndexError:
        role_id = "0"

    update_dict = {
        "important_role_ids.claimed": role_id
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    payload = {
        "type": 4,
        "data": {
            "content": f"Claimed role set to <@&{role_id}>. Until I can be bothered to automatically migrate users to the new role (if that is possible), you must change everyone's roles individually.",
            "flags": 64,
            "allowed_mentions": {"parse": []} # suppress mentions because it looks neater in the client
        }
    }

    if role_id == "0":
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Claimed role removed.")
        return
    
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 7.1.2 set master role
async def settings_menu_claiming_masterrole(data, interaction_id, interaction_token, custom_id=None):
    # get info from DB to auto-fill
    guild_info = await guilds_api.get_guild(data["guild_id"])
    master_role = guild_info.roles.master

    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Claiming  >  Set master role\n\nSet the role given to members when they claim someone else.\n\nYou may need to type the role's name if it doesn't appear in the dropdown.",
            "flags": 64,
            "allowed_mentions": {"parse": []},
            "components": [{
                "type": 1,
                "components": [{
                    "type": 6,
                    "placeholder": "Select a role",
                    "custom_id": "op7_1claiming_2masterrole_set",
                    "min_values": 0,
                    "default_values": [{
                        "id": master_role,
                        "type": "role"
                    }]
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_setmasterrole(data, interaction_id, interaction_token, custom_id=None):
    try:
        role_id = data["data"]["values"][0]
    except IndexError:
        role_id = "0"

    update_dict = {
        "important_role_ids.master": role_id
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    payload = {
        "type": 4,
        "data": {
            "content": f"Master role set to <@&{role_id}>. Until I can be bothered to automatically migrate users to the new role (if that is possible), you must change everyone's roles individually.",
            "flags": 64,
            "allowed_mentions": {"parse": []} # suppress mentions because it looks neater in the client
        }
    }

    if role_id == "0":
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Master role removed.")
        return
    
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 7.1.3 set claim list
async def settings_menu_claiming_claimlist(data, interaction_id, interaction_token, custom_id=None):
    # get info from DB to auto-fill
    guild_info = await guilds_api.get_guild(data["guild_id"])
    claimlist_id = guild_info.channels.claim_list

    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Claiming  >  Set claim list channel\n\nSet the channel where claims are broadcasted in, or no channel if you do not want them to be broadcast.\n\nYou may need to type the channel's name if it doesn't appear in the dropdown, but note that only text and announcement channels, as well as threads, are valid.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 8,
                    "placeholder": "Select a channel",
                    "custom_id": "op7_1claiming_3claimlist_set",
                    "min_values": 0,
                    "channel_types": [0, 5, 11, 12],
                    "default_values": [{
                        "id": claimlist_id,
                        "type": "channel"
                    }]
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_setclaimlist(data, interaction_id, interaction_token, custom_id=None):
    try:
        channel_id = data["data"]["values"][0]
    except IndexError:
        channel_id = "0"

    update_dict = {
        "important_channel_ids.claim_list": channel_id
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    if channel_id == "0":
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Claim list channel removed. Claims will not be broadcast anywhere.")
        return

    await common.basic_ephemeral_response(interaction_id, interaction_token, f"Claim list channel set to <#{channel_id}>.")

# 7.1.4 set claiming channel
async def settings_menu_claiming_claiming(data, interaction_id, interaction_token, custom_id=None):
    # get info from DB to auto-fill
    guild_info = await guilds_api.get_guild(data["guild_id"])
    claiming_id = guild_info.channels.claiming

    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Claiming  >  Set claiming channel\n\nSet the channel where people are allowed to claim each other in, or no channel if you want the command to be useable everywhere.\n\nYou may need to type the channel's name if it doesn't appear in the dropdown, but note that only text and announcement channels, as well as threads, are valid.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 8,
                    "placeholder": "Select a channel",
                    "custom_id": "op7_1claiming_4claiming_set",
                    "min_values": 0,
                    "channel_types": [0, 5, 11, 12],
                    "default_values": [{
                        "id": claiming_id,
                        "type": "channel"
                    }]
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_setclaiming(data, interaction_id, interaction_token, custom_id=None):
    # to-do: update these to edit the original message with the status of what happened
    # I would do it right now but that's a pain in the ass with default values and whatnot
    # it likely needs a helper function which I am too lazy to do

    try:
        channel_id = data["data"]["values"][0]
    except IndexError:
        channel_id = "0"

    update_dict = {
        "important_channel_ids.claiming": channel_id
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    if channel_id == "0":
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Claiming channel removed. Users can claim each other in any channel now.")
        return

    await common.basic_ephemeral_response(interaction_id, interaction_token, f"Claiming channel set to <#{channel_id}>.")

# 7.1.5: claim limit
async def settings_menu_claiming_claimlimit(data, interaction_id, interaction_token):
    # get info from DB
    guild_info = await guilds_api.get_guild(data["guild_id"])
    claim_limit = guild_info.claim_limit

    payload = {
        "type": 9,
        "data": {
            "title": "Enter the max. number of claims per master.",
            "custom_id": "op7_1claiming_5claimlimit_modal",
            "components": [{
                "type": 1,
                "components": [{
                    "type": 4,
                    "custom_id": "op7_1claiming_5claimlimit_set",
                    "label": "Limit (max. 50)",
                    "style": 1,
                    "min_length": 1,
                    "max_length": 2,
                    "placeholder": "e.g. 3",
                    "value": str(claim_limit),
                    "required": True
                }]
            }]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to modal
async def settings_setclaimlimit(data, interaction_id, interaction_token):
    # iterate through action rows and their components
    for action_row in data["data"]["components"]:
        for component in action_row["components"]:
            if component["custom_id"] == "op7_1claiming_5claimlimit_set":
                claim_limit = component["value"]

    try:
        update_dict = {
            "claim_limit": int(claim_limit)
        }
    except (TypeError, ValueError):
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Please only enter an integer (a whole number).")
        return

    if int(claim_limit) < 1:
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Please enter a value higher than 1.")
        return
    
    if int(claim_limit) > 50: # after roughly 95 claims the message content becomes too long to send
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Please enter a value lower than 50.")
        return

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    await common.basic_ephemeral_response(interaction_id, interaction_token, f"Claim limit set to {claim_limit}.")

# 7.2: moderation settings
async def settings_menu_moderation(data, interaction_id, interaction_token, custom_id=None):
    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Moderation\n\nChange moderation settings.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Warns",
                    "style": 1,
                    "custom_id": "op7_2moderation_1warns"
                },
                {
                    "type": 2,
                    "label": "Ban list",
                    "style": 1,
                    "custom_id": "op7_2moderation_2banlist"
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Moderation", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 7.2.1: warn settings
async def settings_menu_moderation_warns(data, interaction_id, interaction_token, custom_id=None):
    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Moderation  >  Warns\n\nChange warn settings.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Warn list channel",
                    "style": 1,
                    "custom_id": "op7_2moderation_1warns_1warnlist"
                },
                {
                    "type": 2,
                    "label": "Warn limit",
                    "style": 1,
                    "custom_id": "op7_2moderation_1warns_2warnlimit"
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Moderation", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 7.2.1.1: warn list
async def settings_menu_moderation_warns_list(data, interaction_id, interaction_token, custom_id=None):
    # get info from DB
    guild_info = await guilds_api.get_guild(data["guild_id"])
    warnlist_id = guild_info.channels.warn_list

    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Moderation  >  Warns  >  Warn list\n\nSet the channel to broadcast warns to, or no channel to broadcast warns in the same channel the command was executed in.\n\nYou may need to type the channel's name if it doesn't appear in the dropdown, but note that only text and announcement channels, as well as threads, are valid.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 8,
                    "placeholder": "Select a channel",
                    "custom_id": "op7_2moderation_1warns_1warnlist_set",
                    "min_values": 0,
                    "channel_types": [0, 5, 11, 12],
                    "default_values": [{
                        "id": warnlist_id,
                        "type": "channel"
                    }]
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Moderation", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_setwarnlist(data, interaction_id, interaction_token):
    try:
        channel_id = data["data"]["values"][0]
    except IndexError:
        channel_id = "0"

    update_dict = {
        "important_channel_ids.warn_list": channel_id
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    if channel_id == "0":
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Warn list channel removed. Warns will be broadcast in the same channel that the command was run in.")
        return

    await common.basic_ephemeral_response(interaction_id, interaction_token, f"Warn list channel set to <#{channel_id}>.")

# 7.2.1.2: warn limit
async def settings_menu_moderation_warns_limit(data, interaction_id, interaction_token):
    # get info from DB
    guild_info = await guilds_api.get_guild(data["guild_id"])
    warn_limit = guild_info.warn_limit

    payload = {
        "type": 9,
        "data": {
            "title": "Enter the number of warns before a ban.",
            "custom_id": "op7_2moderation_1warns_2warnlimit_modal",
            "components": [
                {
                    "type": 1,
                    "components": [
                        {
                            "type": 4,
                            "custom_id": "op7_2moderation_1warns_2warnlimit_set",
                            "label": "Limit (max. 99)",
                            "style": 1,
                            "min_length": 1,
                            "max_length": 2,
                            "placeholder": "e.g. 3",
                            "value": str(warn_limit),
                            "required": True
                        }
                    ]
                }
            ]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to modal
async def settings_setwarnlimit(data, interaction_id, interaction_token):
    # iterate through action rows and their components
    for action_row in data["data"]["components"]:
        for component in action_row["components"]:
            if component["custom_id"] == "op7_2moderation_1warns_2warnlimit_set":
                warn_limit = component["value"]

    try:
        update_dict = {
            "warn_limit": int(warn_limit)
        }
    except (TypeError, ValueError):
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Please only enter an integer (a whole number).")
        return

    if int(warn_limit) < 2:
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Please enter a value higher than 1. Otherwise that would just instantly ban the member on the first warning, which doesn't really make it a warning.")
        return

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    await common.basic_ephemeral_response(interaction_id, interaction_token, f"Warn limit set to {warn_limit}.")

# 7.2.2: ban list
async def settings_menu_moderation_banlist(data, interaction_id, interaction_token, custom_id=None):
    # get info from DB
    guild_info = await guilds_api.get_guild(data["guild_id"])
    banlist_id = guild_info.channels.ban_list

    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Moderation  >  Ban list\n\nSet the channel to broadcast bans to, or no channel if you do not want bans to be broadcast anywhere.\n\nYou may need to type the channel's name if it doesn't appear in the dropdown, but note that only text and announcement channels, as well as threads, are valid.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 8,
                    "placeholder": "Select a channel",
                    "custom_id": "op7_2moderation_2banlist_set",
                    "min_values": 0,
                    "channel_types": [0, 5, 11, 12],
                    "default_values": [{
                        "id": banlist_id,
                        "type": "channel"
                    }]
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Moderation", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_setbanlist(data, interaction_id, interaction_token):
    try:
        channel_id = data["data"]["values"][0]
    except IndexError:
        channel_id = "0"

    update_dict = {
        "important_channel_ids.ban_list": channel_id
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    if channel_id == "0":
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Ban list channel removed. Bans will not be broadcast.")
        return

    await common.basic_ephemeral_response(interaction_id, interaction_token, f"Ban list channel set to <#{channel_id}>.")

# 7.3: member screening settings
async def settings_menu_screening(data, interaction_id, interaction_token, custom_id=None):
    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Member screening\n\nChange member screening settings.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Account age check",
                    "style": 1,
                    "custom_id": "op7_3screening_1accage"
                },
                {
                    "type": 2,
                    "label": "Rules",
                    "style": 1,
                    "custom_id": "op7_3screening_2rules"
                },
                {
                    "type": 2,
                    "label": "Set re-invite URL",
                    "style": 1,
                    "custom_id": "op7_3screening_3reinviteurl"
                },
                {
                    "type": 2,
                    "label": "Member age check",
                    "style": 1,
                    "custom_id": "op7_3screening_4memberagecheck"
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Member-Screening", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 7.3.1: account age check settings
async def settings_menu_screening_accage(data, interaction_id, interaction_token, custom_id=None):
    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Member screening  >  Account age check\n\nChange account age check settings. You can set the minimum age to 0 to disable the feature.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Set minimum age in hours",
                    "style": 1,
                    "custom_id": "op7_3screening_1accage_1minage"
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Member-Screening", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 7.3.1.1: min. acc age
async def settings_menu_screening_accage_minage(data, interaction_id, interaction_token):
    # get info from DB to auto-fill
    guild_info = await guilds_api.get_guild(data["guild_id"])
    min_age = guild_info.min_acc_age

    payload = {
        "type": 9,
        "data": {
            "title": "How many hours old do accounts have to be?",
            "custom_id": "op7_3screening_1accage_1minage_modal",
            "components": [
                {
                    "type": 1,
                    "components": [
                        {
                            "type": 4,
                            "custom_id": "op7_3screening_1accage_1minage_submithrs",
                            "label": "Age in hours",
                            "style": 1,
                            "min_length": 1,
                            "max_length": 5,
                            "placeholder": "e.g. 24",
                            "value": str(min_age),
                            "required": True
                        }
                    ]
                }
            ]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to modal
async def settings_setminaccage(data, interaction_id, interaction_token):
    # iterate through action rows and their components
    for action_row in data["data"]["components"]:
        for component in action_row["components"]:
            if component["custom_id"] == "op7_3screening_1accage_1minage_submithrs":
                min_age = component["value"]

    try:
        update_dict = {
            "min_acc_age": int(min_age)
        }
    except (TypeError, ValueError):
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Please only enter an integer (a whole number).")
        return

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    await common.basic_ephemeral_response(interaction_id, interaction_token, f"Minimum account age set to {min_age} hours.")

# 7.3.2: rules settings
async def settings_menu_screening_rules(data, interaction_id, interaction_token, custom_id=None):
    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Member screening  >  Rules\n\nChange rules confirmation settings.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Swap agree and disagree",
                    "style": 1,
                    "custom_id": "op7_3screening_2rules_1swapbuttons"
                },
                {
                    "type": 2,
                    "label": "Create rules verification message",
                    "style": 1,
                    "custom_id": "op7_3screening_2rules_2sendmsg"
                },
                {
                    "type": 2,
                    "label": "Set member role",
                    "style": 1,
                    "custom_id": "op7_3screening_2rules_3memberrole"
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Member-Screening", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# generate the rules message
async def generate_rules_message(rules_buttons_swapped):
    if rules_buttons_swapped == True:
        content = f"Thank you for reading the rules! Click on the {Emojis.red_cross} button below to enter.\nDo NOT click on the \u2705 button, or else you will be kicked from the server."
        buttons = [{
            "type": 2,
            "custom_id": "op3_agree",
            "style": 3,
            "emoji": Emojis.get_partial_emoji("\u2705")
        },
        {
            "type": 2,
            "custom_id": "op3_disagree",
            "style": 4,
            "emoji": Emojis.get_partial_emoji(Emojis.red_cross)
        }]
    else:
        content = "Thank you for reading the rules! Click on the \u2705 button below to enter."
        buttons = [{
            "type": 2,
            "custom_id": "op3_disagree",
            "style": 3,
            "emoji": Emojis.get_partial_emoji("\u2705")
        }]

    message_data = {
        "content": content,
        "components": [{
            "type": 1,
            "components": buttons
        }]
    }
    return message_data

# 7.3.2.1 swap buttons
async def settings_menu_screening_rules_swapbuttons(data, interaction_id, interaction_token):
    guild_info = await guilds_api.get_guild(data["guild_id"])

    # chatgpt code time?
    message_link = guild_info.rules_message_link
    # Define a regular expression pattern to match the channel IDs
    pattern = r"/channels/(\d+)/(\d+)/(\d+)"

    # Use re.search to find matches in the URL
    match = re.search(pattern, message_link)

    if match:
        channel_id = match.group(2)  # Get the second matched group
        message_id = match.group(3)  # Get the third matched group

    # send msg
    message_data = generate_rules_message(guild_info.rules_buttons_swapped)

    http_api.edit_message(channel_id, message_id, message_data)
    
    if guild_info.rules_buttons_swapped == False: update_dict = {"rules_buttons_swapped": True}
    else: update_dict = {"rules_buttons_swapped": False}

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    await common.basic_ephemeral_response(interaction_id, interaction_token, "Swapped!")

# 7.3.2.2 send the message
async def settings_menu_screening_rules_sendmsg(data, interaction_id, interaction_token, custom_id=None):
    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Member screening  >  Rules  >  Create rules verification message\n\nSelect a channel to send the rules verification message in.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 8,
                    "placeholder": "Select a channel",
                    "custom_id": "op7_3screening_2rules_2sendmsg_set",
                    "min_values": 1,
                    "channel_types": [0, 5, 11, 12]
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Member-Screening", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_sendrulesmsg(data, interaction_id, interaction_token):
    try:
        channel_id = data["data"]["values"][0]
    except IndexError:
        await common.basic_ephemeral_response(interaction_id, interaction_token, "You seem to have not set a channel, somehow...?")
        return

    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)
    
    guild_info = await guilds_api.get_guild(data["guild_id"])

    # delete old msg
    message_link = guild_info.rules_message_link
    if message_link != "":
        # chatgpt code time?
        # omg more boilerplate?
        
        # Define a regular expression pattern to match the channel IDs
        pattern = r"/channels/(\d+)/(\d+)/(\d+)"

        # Use re.search to find matches in the URL
        match = re.search(pattern, message_link)

        if match:
            old_channel_id = match.group(2)  # Get the second matched group
            old_message_id = match.group(3)  # Get the third matched group
        
        http_api.delete_message(old_channel_id, old_message_id, "Removing old rules message")

    # send msg
    message_data = generate_rules_message(guild_info.rules_buttons_swapped)

    response = http_api.send_message(channel_id, message_data)
    if response.status_code == 200:
        message_id = json.loads(response.text)["id"]
    else:
        http_api.edit_interaction_response_original(interaction_token, {"content": "I do not have permission to send messages there."})
        return

    # update DB
    message_link = f"https://discord.com/channels/{data['guild_id']}/{channel_id}/{message_id}"
    update_dict = {
        "rules_message_link": message_link
    }
    await guilds_api.update_guild(data["guild_id"], update_dict)

    http_api.edit_interaction_response_original(interaction_token, {"content": f"Done! {message_link}"})

# 7.3.2.3 set member role
async def settings_menu_screening_rules_memberrole(data, interaction_id, interaction_token, custom_id=None):
    # get info from DB to auto-fill
    guild_info = await guilds_api.get_guild(data["guild_id"])
    member_role = guild_info.roles.member

    payload = {
        "type": 7,
        "data": {
            "content": f"## Hanabot settings  >  Member screening  >  Rules  >  Set member role\n\nSet the role given to members when they finish the onboarding.\n\nYou may need to type the role's name if it doesn't appear in the dropdown.",
            "flags": 64,
            "allowed_mentions": {"parse": []},
            "components": [{
                "type": 1,
                "components": [{
                    "type": 6,
                    "placeholder": "Select a role",
                    "custom_id": "op7_3screening_2rules_3memberrole_set",
                    "min_values": 0,
                    "default_values": [{
                        "id": member_role,
                        "type": "role"
                    }]
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Member-Screening", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_setmemberrole(data, interaction_id, interaction_token, custom_id=None):
    try:
        role_id = data["data"]["values"][0]
    except IndexError:
        role_id = "0"

    update_dict = {
        "important_role_ids.member": role_id
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    payload = {
        "type": 4,
        "data": {
            "content": f"Member role set to <@&{role_id}>.",
            "flags": 64,
            "allowed_mentions": {"parse": []} # suppress mentions because it looks neater in the client
        }
    }
    
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 7.3.3: re-invite URL
async def settings_menu_screening_reinviteurl(data, interaction_id, interaction_token):
    # get info from DB to auto-fill
    guild_info = await guilds_api.get_guild(data["guild_id"])
    reinvite_url = guild_info.reinvite_url

    payload = {
        "type": 9,
        "data": {
            "title": "Set re-invite URL",
            "custom_id": "op7_3screening_3reinviteurl_modal",
            "components": [
                {
                    "type": 1,
                    "components": [
                        {
                            "type": 4,
                            "custom_id": "op7_3screening_3reinviteurl_submit",
                            "label": "Invite link",
                            "style": 1,
                            "max_length": 40,
                            "placeholder": "e.g. https://discord.gg/xCNnXYp9eR",
                            "required": False
                        }
                    ]
                }
            ]
        }
    }

    # goddamn you discord
    if reinvite_url != "":
        payload["data"]["components"][0]["components"][0]["value"] = reinvite_url
    
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to modal
async def settings_setreinviteurl(data, interaction_id, interaction_token):
    # iterate through action rows and their components
    for action_row in data["data"]["components"]:
        for component in action_row["components"]:
            if component["custom_id"] == "op7_3screening_3reinviteurl_submit":
                reinvite_url = component["value"]

    print(reinvite_url)
    if "discord.gg" not in reinvite_url and reinvite_url != "":
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Please enter a valid discord.gg URL.")
        return

    update_dict = {
        "reinvite_url": reinvite_url
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    if reinvite_url == "": reinvite_url = "nothing." # for the response
    await common.basic_ephemeral_response(interaction_id, interaction_token, f"Re-invite URL set to {reinvite_url}")

# 7.3.4: member age check settings
async def settings_menu_screening_memberage(data, interaction_id, interaction_token, custom_id=None):
    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Member screening  >  Member age check\n\nChange member age check settings.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Set broadcast channel",
                    "style": 1,
                    "custom_id": "op7_3screening_4memberagecheck_1channel"
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Member-Screening#member-age-check", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 7.3.4.1 member age broadcast channel
async def settings_menu_screening_memberage_channel(data, interaction_id, interaction_token, custom_id=None):
    # get info from DB to auto-fill
    guild_info = await guilds_api.get_guild(data["guild_id"])
    broadcast_channel_id = guild_info.channels.member_age_broadcast

    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Member screening  >  Member age check  >  Set broadcast channel\n\nSet the channel where users' answers to their age and DOB will be broadcast. If you do not want this feature, use the toggle features menu instead.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 8,
                    "placeholder": "Select a channel",
                    "custom_id": "op7_3screening_4memberagecheck_1channel_set",
                    "min_values": 1,
                    "channel_types": [0, 5, 11, 12],
                    "default_values": [{
                        "id": broadcast_channel_id,
                        "type": "channel"
                    }]
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Member-Screening#set-broadcast-channel", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_setmemberagebroadcast(data, interaction_id, interaction_token):
    try:
        channel_id = data["data"]["values"][0]
    except IndexError:
        channel_id = "0"

    update_dict = {
        "important_channel_ids.member_age_broadcast": channel_id
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    if channel_id == "0":
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Member age broadcast channel removed.")
        return

    await common.basic_ephemeral_response(interaction_id, interaction_token, f"Member age broadcast set to <#{channel_id}>.")

    guild_info = await guilds_api.get_guild(data["guild_id"])
    welcome_id = guild_info.channels.welcome

# 7.4: welcoming settings
async def settings_menu_welcoming(data, interaction_id, interaction_token, custom_id=None):
    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Welcoming\n\nChange welcoming settings.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Change message",
                    "style": 1,
                    "custom_id": "op7_4welcoming_1message"
                },
                {
                    "type": 2,
                    "label": "Set welcoming channel",
                    "style": 1,
                    "custom_id": "op7_4welcoming_2welcoming"
                },
                {
                    "type": 2,
                    "label": "Test welcome message",
                    "style": 1,
                    "custom_id": "op7_4welcoming_3testmessage"
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Welcoming", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 7.4.1: welcome messages
# to-do: fix this to not use the fucking raw message data LMAO
async def settings_menu_welcoming_message(data, interaction_id, interaction_token):
    payload = {
        "type": 9,
        "data": {
            "title": "Welcome message",
            "custom_id": "op7_4welcoming_1message_modal",
            "components": [
                {
                    "type": 1,
                    "components": [
                        {
                            "type": 4,
                            "custom_id": "op7_4welcoming_1message_submit",
                            "label": "Raw message JSON data",
                            "style": 2,
                            "min_length": 1,
                            "max_length": 4000,
                            "placeholder": "Check the online help which I will post later.",
                            "required": True
                        }
                    ]
                }
            ]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to modal
async def settings_setwelcomingmessage(data, interaction_id, interaction_token):
    # iterate through action rows and their components
    for action_row in data["data"]["components"]:
        for component in action_row["components"]:
            if component["custom_id"] == "op7_4welcoming_1message_submit":
                message_data = component["value"]

    # check for valid data and send a test message
    try:
        message_data = json.loads(message_data)
    except json.decoder.JSONDecodeError:
        await common.basic_ephemeral_response(interaction_id, interaction_token, "That was invalid JSON data. Make sure that it is not indented at all.")
        return

    if "components" in message_data or "attachments" in message_data or "files" in message_data or "payload_json" in message_data:
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Message components or attachments are not allowed in the welcome message... were you trying to find a security breach?\n\n||(good job on finding this if you didn't cheat)||")
        return

    try:
        update_dict = {
            "messages.welcome": message_data
        }
    except (TypeError, ValueError):
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Please only enter an integer (a whole number).")
        return

    interaction_message_data = copy(message_data)
    interaction_message_data["flags"] = 64

    interaction_data = {
        "type": 4,
        "data": interaction_message_data
    }
    test_message = http_api.create_interaction_response(interaction_id, interaction_token, interaction_data)

    if test_message.status_code != 204: # fucking discord make this endpoint return 200 please
        await common.basic_ephemeral_response(interaction_id, interaction_token, "That was invalid message data.")
        return

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    # notify user
    payload = {
        "content": "Welcome message set.",
        "flags": 64
    }

    http_api.create_followup_message(interaction_token, payload)

# 7.4.2 welcoming channel
async def settings_menu_welcoming_welcoming(data, interaction_id, interaction_token, custom_id=None):
    # get info from DB to auto-fill
    guild_info = await guilds_api.get_guild(data["guild_id"])
    welcome_id = guild_info.channels.welcome

    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Welcoming  >  Set welcoming channel\n\nSet the channel where people are welcomed. This feature can be disabled in the toggle features category if you do not want it.\n\nYou may need to type the channel's name if it doesn't appear in the dropdown, but note that only text and announcement channels, as well as threads, are valid. To disable the feature, use the toggle features option instead.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 8,
                    "placeholder": "Select a channel",
                    "custom_id": "op7_4welcoming_2welcoming_set",
                    "min_values": 1,
                    "channel_types": [0, 5, 11, 12],
                    "default_values": [{
                        "id": welcome_id,
                        "type": "channel"
                    }]
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Welcoming", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_setwelcoming(data, interaction_id, interaction_token):
    try:
        channel_id = data["data"]["values"][0]
    except IndexError:
        channel_id = "0"

    update_dict = {
        "important_channel_ids.welcome": channel_id
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    if channel_id == "0":
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Welcoming channel removed.")
        return

    await common.basic_ephemeral_response(interaction_id, interaction_token, f"Welcoming channel set to <#{channel_id}>.")

    guild_info = await guilds_api.get_guild(data["guild_id"])
    welcome_id = guild_info.channels.welcome

# 7.4.3 welcome message test
async def settings_menu_welcoming_testmessage(data, interaction_id, interaction_token):
    # get info from DB
    guild_info = await guilds_api.get_guild(data["guild_id"])
    message_data = guild_info.messages.welcome

    interaction_message_data = copy(message_data)
    interaction_message_data["flags"] = 64

    interaction_data = {
        "type": 4,
        "data": interaction_message_data
    }

    test_message = http_api.create_interaction_response(interaction_id, interaction_token, interaction_data)

    if test_message.status_code != 204: # fucking discord make this endpoint return 200 please
        await common.basic_ephemeral_response(interaction_id, interaction_token, "The message data is invalid or not set yet.")
        return

# toggle features menu
async def gen_features_options(features):
    options = []
    # check all features status
    for feature, status in features.items():
        # ban list
        if feature == "banlist":
            if status == True:
                option = {
                    "label": "Ban list (enabled)",
                    "value": "op7_5toggle_banlist_off",
                    "description": "Enabled"
                }
            else:
                option = {
                    "label": "Ban list (disabled)",
                    "value": "op7_5toggle_banlist_on",
                    "description": "Disabled"
                }
            option["emoji"] = {"name": "🔨"}
        
        # welcoming
        elif feature == "welcoming":
            if status == True:
                option = {
                    "label": "Welcoming (enabled)",
                    "value": "op7_5toggle_welcoming_off",
                    "description": "Enabled"
                }
            else:
                option = {
                    "label": "Welcoming (disabled)",
                    "value": "op7_5toggle_welcoming_on",
                    "description": "Disabled"
                }
            option["emoji"] = {"name": "👋"}

        # claiming
        elif feature == "claiming":
            if status == True:
                option = {
                    "label": "Claiming (enabled)",
                    "value": "op7_5toggle_claiming_off",
                    "description": "Enabled"
                }
            else:
                option = {
                    "label": "Claiming (disabled)",
                    "value": "op7_5toggle_claiming_on",
                    "description": "Disabled",
                }
            option["emoji"] = {"name": "🙇"}

        # member age check
        elif feature == "memberagecheck":
            if status == True:
                option = {
                    "label": "Age and DOB check (enabled)",
                    "value": "op7_5toggle_memberagecheck_off",
                    "description": "Enabled"
                }
            else:
                option = {
                    "label": "Age and DOB check (disabled)",
                    "value": "op7_5toggle_memberagecheck_on",
                    "description": "Disabled"
                }
            option["emoji"] = {"name": "🎂"}

        # cross-verify
        elif feature == "crossverify":
            if status == True:
                option = {
                    "label": "Cross-verification (enabled)",
                    "value": "op7_5toggle_crossverify_off",
                    "description": "Enabled"
                }
            else:
                option = {
                    "label": "Cross-verification (disabled)",
                    "value": "op7_5toggle_crossverify_on",
                    "description": "Disabled"
                }
            option["emoji"] = {"name": "🪪"}

        # default case if the feature is not in this menu
        else: continue

        # add to the list of options
        options.append(option)
    return options

# 7.5: toggle features
async def settings_menu_toggle(data, interaction_id, interaction_token, custom_id=None):
    # guild info from DB
    guild_id = data["guild_id"]
    guild_info = await guilds_api.get_guild(guild_id)

    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Toggle features\n\nTurn the bot's features on or off.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 3,
                    "placeholder": "Select features to toggle",
                    "custom_id": "op7_5toggle_menu",
                    "min_values": 1,
                    "options": await gen_features_options(guild_info.features.to_dict())
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Toggle-Features", custom_id=custom_id)]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_togglefeature(data, interaction_id, interaction_token, custom_id=None):
    # respond to interaction
    await common.basic_deferred_update_message(interaction_id, interaction_token)

    try:
        value = data["data"]["values"][0]
    except IndexError:
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Please select an option.")
        return

    parts = value.split("_")
    feature = parts[2]
    if parts[3] == "on": feature_enabled = True
    else: feature_enabled = False
    
    update_dict = {
        f"features.{feature}": feature_enabled
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    # confirmation message
    feature_formatted = feature

    if feature == "banlist": feature_formatted = "ban list"
    elif feature == "memberagecheck": feature_formatted = "Age and DOB check"
    elif feature == "crossverify": feature_formatted = "cross-verification"

    # base content
    content = "## Hanabot settings  >  Toggle features\n\nTurn the bot's features on or off."

    if feature_enabled == True: content += f"\n\nEnabled feature {feature_formatted}."
    else: content += f"\n\nDisabled feature {feature_formatted}."

    # edit original message
    original_message = data["message"]["id"]

    # guild info from DB
    guild_id = data["guild_id"]
    guild_info = await guilds_api.get_guild(guild_id)

    payload = {
        "content": content,
        "components": [{
            "type": 1,
            "components": [{
                "type": 3,
                "placeholder": "Select features to toggle",
                "custom_id": "op7_5toggle_menu",
                "min_values": 1,
                "options": await gen_features_options(guild_info.features.to_dict())
            }]
        },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Toggle-Features", custom_id=custom_id)]
    }
    http_api.edit_interaction_response_original(interaction_token, payload)

# 7.6: cross-verify settings
async def settings_menu_crossverify(data, interaction_id, interaction_token, custom_id=None):
    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Cross-verification\n\nChange cross-verification settings.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Set 18+ verified role",
                    "style": 1,
                    "custom_id": "op7_6crossverify_1majorrole"
                },
                {
                    "type": 2,
                    "label": "Set verification log channel",
                    "style": 1,
                    "custom_id": "op7_6crossverify_2logchannel"
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming", custom_id=custom_id)] # placeholder help link
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# set major role
async def settings_menu_crossverify_majorrole(data, interaction_id, interaction_token, custom_id=None):
    # get info from DB to auto-fill
    guild_info = await guilds_api.get_guild(data["guild_id"])
    major_role = guild_info.roles.major_verified

    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Cross-verification  >  Set 18+ verified role\n\nSet the role given to members when they are verified as 18+.\n\nYou may need to type the role's name if it doesn't appear in the dropdown.",
            "flags": 64,
            "allowed_mentions": {"parse": []},
            "components": [{
                "type": 1,
                "components": [{
                    "type": 6,
                    "placeholder": "Select a role",
                    "custom_id": "op7_6crossverify_1majorrole_set",
                    "min_values": 0,
                    "default_values": [{
                        "id": major_role,
                        "type": "role"
                    }]
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming", custom_id=custom_id)] # placeholder help link
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_setmajorverifiedrole(data, interaction_id, interaction_token, custom_id=None):
    try:
        role_id = data["data"]["values"][0]
    except IndexError:
        role_id = "0"

    update_dict = {
        "important_role_ids.major_verified": role_id
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    payload = {
        "type": 4,
        "data": {
            "content": f"18+ verified role set to <@&{role_id}>.",
            "flags": 64,
            "allowed_mentions": {"parse": []} # suppress mentions because it looks neater in the client
        }
    }

    if role_id == "0":
        await common.basic_ephemeral_response(interaction_id, interaction_token, "18+ verified role removed.")
        return
    
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# 7.6.2 set verification log channel
async def settings_menu_crossverify_logchannel(data, interaction_id, interaction_token, custom_id=None):
    # get info from DB to auto-fill
    guild_info = await guilds_api.get_guild(data["guild_id"])
    logchannel_id = guild_info.channels.cross_verify_log

    payload = {
        "type": 7,
        "data": {
            "content": "## Hanabot settings  >  Cross-verification  >  Set verification log channel\n\nSet the channel where verifications are broadcast in, or no channel if you do not want them to be broadcast. **It is highly recommended to set one in order to verify who verified.**\n\nYou may need to type the channel's name if it doesn't appear in the dropdown, but note that only text and announcement channels, as well as threads, are valid.",
            "flags": 64,
            "components": [{
                "type": 1,
                "components": [{
                    "type": 8,
                    "placeholder": "Select a channel",
                    "custom_id": "op7_6crossverify_2logchannel_set",
                    "min_values": 0,
                    "channel_types": [0, 5, 11, 12],
                    "default_values": [{
                        "id": logchannel_id,
                        "type": "channel"
                    }]
                }]
            },await gen_footer_action_row("https://gitlab.com/hana.flower/hanabot/-/wikis/Help/Settings/Claiming", custom_id=custom_id)] # placeholder help link
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# respond to select menu
async def settings_setverificationlog(data, interaction_id, interaction_token, custom_id=None):
    try:
        channel_id = data["data"]["values"][0]
    except IndexError:
        channel_id = "0"

    update_dict = {
        "important_channel_ids.cross_verify_log": channel_id
    }

    # database
    await guilds_api.update_guild(data["guild_id"], update_dict)

    if channel_id == "0":
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Verification log channel removed. Verifications will not be logged anywhere.")
        return

    await common.basic_ephemeral_response(interaction_id, interaction_token, f"Verification log channel set to <#{channel_id}>.")

