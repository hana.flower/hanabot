# custom libraries
import http_api
import database
import tools
from config import *
from logger import *
from handlers.interactions import hb_common as common

from api import claims as claims_api
from api import claims_waitlist as claims_waitlist_api
from api import guilds as guilds_api
from api import members as members_api

# not custom libraries
import json
import asyncio
from time import time

# 1: claiming
# claim request
# to-do: learn asynchronous programming LMAO
async def claim(data, interaction_id, interaction_token, options, logger=None):
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    # extract claimed ID
    claimed = options["claimant"]
    master = data["member"]["user"]["id"]

    guild_id = data["guild_id"]

    # database
    waitlist_claim = await claims_waitlist_api.get_waitlist_claim(guild_id, {"claimed": claimed}, logger=logger)
    member = await members_api.get_member(master, guild_id)
    guild_info = await guilds_api.get_guild(guild_id)
        
    # get guild info
    guild_claimed_role_id = guild_info.roles.claimed
    guild_master_role_id = guild_info.roles.master
    guild_claiming_channel_id = guild_info.channels.claiming
    claim_limit = guild_info.claim_limit

    # sanity checks
    # is the feature enabled
    enabled = guild_info.features.claiming
    if enabled == False:
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Claim failed because the feature is disabled in guild {guild_id}.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Claiming is not enabled in this server."})
        return

    # claiming self
    if claimed == master:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim failed because the user tried to claim themselves.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "You cannot claim yourself, what did you expect?\nFun fact: This used to crash the bot before this check was implemented."})
        return

    # claiming the bot (for sophie <3)
    if claimed == BotInfo.application_id:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim failed because the user tried to claim me.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "I have no free will. Save me."})
        return

    try:
        # claiming any bot
        users = data["data"]["resolved"]["users"]
        for user_key in users:
            if users[user_key]["bot"] == True:
                asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim failed because the user tried to claim a bot.", HandlerType.INTERACTION_CLAIMING))
                http_api.edit_interaction_response_original(interaction_token, {"content": "Robots have no free will. They also can't use interactions."})
                return
    except KeyError:
        pass

    # if the user is not in the server
    if "members" not in data["data"]["resolved"]:
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Claim failed because the user tried to claim someone who is not in guild {guild_id}.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "You can't get someone who's not in the server to consent."})
        return

    # check if the channel is correct
    if guild_claiming_channel_id != "0":
        if data["channel_id"] != guild_claiming_channel_id:
            asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim failed because the user tried to use the command in the wrong channel.", HandlerType.INTERACTION_CLAIMING))
            http_api.edit_interaction_response_original(interaction_token, {"content": f"This command can only be used in <#{guild_claiming_channel_id}>."})
            return
    
    # too many claims?
    claimed_ids = member.claimed_ids
    claims_count = len(claimed_ids)

    # check claim limit
    if claims_count >= claim_limit:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim failed because the user has too many claims.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": f"You can only claim up to {claim_limit} people."})
        return

    # is this person in the waitlist
    if waitlist_claim != None:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim failed because the user tried to claim someone who is in the waitlist.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Someone is already waiting to claim this person."})
        return

    # check user claims
    claim_object = await claims_api.get_claim(guild_id, mode="check_if_valid_claim", users={"master": master, "claimed": claimed}, logger=logger)

    if claim_object != None:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim failed because the user tried to claim someone who is claimed/a master, or the user is claimed themselves.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Either you are already claimed, or the person you are trying to claim is already in a claim relationship. Don't try to steal someone's sub."})
        return

    # all checks passed    
    if claims_count >= 1:
        message_content = f"<@{claimed}>, do you consent to being claimed by <@{master}>? (Since this master has claimed other users, they must all also accept the claim.)"
        for user in claimed_ids:
            message_content += f" <@{user}>"
    else:
        message_content = f"<@{claimed}>, do you consent to being claimed by <@{master}>?"
    
    if "note" in options:
        message_content += f"\n\n**__Note from the master:__**\n{options['note']}"
    payload = {
        "content": message_content,
        "allowed_mentions": {
            "parse": ["users"]
        },
        "components": [
        {
            "type": 1,
            "components": [
                {
                    "type": 2,
                    "label": "Yes!",
                    "style": 3,
                    "custom_id": f"op1_accept_cid{claimed}_mid{master}"
                },
                {
                    "type": 2,
                    "label": "No.",
                    "style": 4,
                    "custom_id": f"op1_decline_cid{claimed}_mid{master}"
                }
            ]
        }
        ]
    }
    http_api.edit_interaction_response_original(interaction_token, {"content": "Claim successful! A message will be shown soon for people to give their consent."})
    http_api.send_message(data["channel_id"], payload)

    # database
    try:
        await claims_waitlist_api.create_waitlist_claim({"master": master, "claimed": claimed}, guild_id, note=options["note"], logger=logger)
    except:
        await claims_waitlist_api.create_waitlist_claim({"master": master, "claimed": claimed}, guild_id, logger=logger)

    asyncio.create_task(log(logger, ContextType.INTERACTION, f"Claim request created successfully!", HandlerType.INTERACTION_CLAIMING))

# 1: claim success
async def claim_success(data, interaction_id, interaction_token, original_claim_request_message_id, claimed, master, logger=None):
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    # database
    guild_id = data["guild_id"]
    guild_info = await guilds_api.get_guild(guild_id)
    
    guild_claimed_role_id = guild_info.roles.claimed
    guild_master_role_id = guild_info.roles.master
    guild_claim_list_channel_id = guild_info.channels.claim_list
    claim_limit = guild_info.claim_limit

    # is the feature enabled
    enabled = guild_info.features.claiming
    if enabled == False:
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Claim accept failed because the feature is disabled in guild {guild_id}.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Claiming is not enabled in this server."})
        return

    # sanity checks
    # claim must be no older than 24 hours   
    snowflake_id = int(data["message"]["id"])
    time_difference = int(round(time(), 3) * 1000) - ((snowflake_id >> 22) + 1420070400000)
    if time_difference > 86400000:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim accept failed because the claim is too old.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "This claim is too old to respond to: it must be no older than 24 hours."})
        return

    waitlist_claim = await claims_waitlist_api.get_waitlist_claim(guild_id, {"master": master, "claimed": claimed})
    if waitlist_claim == None:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim accept failed because there is no waitlist claim. Likely an internal bot issue.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Something went wrong internally, DM the bot and tell hana to check the waitlist claims. Ask your master to /claim you again."})
        return

    # too many claims?
    member = await members_api.get_member(master, guild_id)
    claimed_ids = member.claimed_ids
    claims_count = len(claimed_ids)

    # check claim limit
    if claims_count >= claim_limit:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim accept failed because the master has claimed too many people.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": f"The master can only claim up to {claim_limit} people."})
        return
    
    is_user_claimed_by_master = data["member"]["user"]["id"] in claimed_ids
    user_id = data["member"]["user"]["id"]

    claimeds_who_have_accepted = waitlist_claim.accepted

    # check user claims
    claim_object = await claims_api.get_claim(guild_id, mode="check_if_valid_claim", users={"master": master, "claimed": claimed}, logger=logger)

    if claim_object != None:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim failed because the user tried to claim someone who is claimed/a master, or the user is claimed themselves.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Either you are already claimed, or the person you are trying to claim is already in a claim relationship. Don't try to steal someone's sub."})
        return

    #print(is_user_claimed_by_master)
    #if not ((str(user_id) != claimed) or (str(user_id) not in claimed_ids) and (str(user_id) in claimeds_who_have_accepted)):
    if user_id == claimed or user_id in claimed_ids:
        if user_id in claimeds_who_have_accepted:
            asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim accept failed because the user has already agreed to the claim.", HandlerType.INTERACTION_CLAIMING))
            http_api.edit_interaction_response_original(interaction_token, {"content": "You have already agreed to this claim."})
            return

        # all checks passed
        else:
            asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim accepted by the user.", HandlerType.INTERACTION_CLAIMING))
            http_api.edit_interaction_response_original(interaction_token, {"content": "You have agreed to this claim."})

            # send response
            payload = {
                "content": f"Claim accepted by <@{user_id}>.",
                "message_reference": {
                    "message_id": original_claim_request_message_id,
                },
                "allowed_mentions": {
                    "parse": ["users"]
                }
            }

            # update stuff
            http_api.send_message(data["channel_id"], payload)
            claimeds_who_have_accepted.append(user_id)
            update_dict = {
                "accepted": claimeds_who_have_accepted
            }

            await claims_waitlist_api.update_waitlist_claim({"master": master, "claimed": claimed}, guild_id, update_dict)

            # update original message
            '''
            print(claimeds_who_have_accepted)
            print(claimed_ids)
            print(all(item in claimeds_who_have_accepted for item in claimed_ids))
            print(claimed in claimeds_who_have_accepted)
            '''
            if all(item in claimeds_who_have_accepted for item in claimed_ids) and claimed in claimeds_who_have_accepted:
                #print("All have accepted")
                all_accepted = True
            else:
                #print("More need to accept")
                all_accepted = False

            if all_accepted: # everyone has accepted
                asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim fully accepted!", HandlerType.INTERACTION_CLAIMING))
                message_content = f"~~<@{claimed}>, do you consent to being claimed by <@{master}>?~~ This claim has been accepted!"
                payload = {
                    "content": message_content,
                    "components": [
                    {
                        "type": 1,
                        "components": [
                            {
                                "type": 2,
                                "label": "Yes!",
                                "style": 3,
                                "custom_id": "thiscustomiddoesntmatterbecausethisbuttonisdisabledanyway1",
                                "disabled": True
                            },
                            {
                                "type": 2,
                                "label": "No.",
                                "style": 4,
                                "custom_id": "thiscustomiddoesntmatterbecausethisbuttonisdisabledanyway2",
                                "disabled": True
                            }
                        ]
                    }
                ]
                }
                await claims_waitlist_api.delete_waitlist_claim({"master": master, "claimed": claimed}, guild_id)
            else: # more people need to accept, currently not used
                #message_content = f"~~<@{claimed}>, do you consent to being claimed by <@{master}>?~~ This claim has been accepted by {claimeds_who_have_accepted}"
                return

            http_api.edit_followup_message(interaction_token, payload, data["message"]["id"])

            note = waitlist_claim.note
            await claims_api.create_claim(claimed, master, guild_id, interaction_id, note)
    else:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim accept failed because the user has no say in it, L.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "You have no say in this."})

# 1: claim decline
async def claim_decline(data, interaction_id, interaction_token, original_claim_request_message_id, claimed, master, logger=None):
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    # database 
    guild_id = data["guild_id"]
    guild_info = await guilds_api.get_guild(guild_id)
    guild_claimed_role_id = guild_info.roles.claimed
    guild_master_role_id = guild_info.roles.master
    guild_claim_list_channel_id = guild_info.channels.claim_list
    
    # is the feature enabled
    enabled = guild_info.features.claiming
    if enabled == False:
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Claim decline failed because the feature is disabled in guild {guild_id}.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Claiming is not enabled in this server."})
        return
    
    # sanity check
    # claim must be no older than 24 hours
    snowflake_id = int(data["message"]["id"])
    time_difference = int(round(time(), 3) * 1000) - ((snowflake_id >> 22) + 1420070400000)
    if time_difference > 86400000:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim decline failed because the claim is too old.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "This claim is too old to respond to: it must be no older than 24 hours."})
        return

    payload = {
        "content": "Claim declined.",
        "message_reference": {
            "message_id": original_claim_request_message_id,
        }
    }

    # claims info
    member = await members_api.get_member(master, guild_id)
    claimed_ids = member.claimed_ids
    claims_count = len(claimed_ids)

    is_user_claimed_by_master = data["member"]["user"]["id"] in claimed_ids
    user_id = data["member"]["user"]["id"]

    try:
        claimeds_who_have_accepted = await claims_waitlist_api.get_waitlist_claim(guild_id, {"master": master, "claimed": claimed}).accepted
    except AttributeError:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim decline failed because there is no waitlist claim. Likely an internal bot issue.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Something went wrong internally, DM the bot and tell hana to check the waitlist claims. Ask your master to /claim you again."})
        return
        
    # debug
    '''
    print(str(user_id) != claimed) # check if you are the one being claimed.
    print(str(user_id) not in claimed_ids) # check if you are not in the list of users who the master has claimed (for multi claims)
    print(str(user_id) in claimeds_who_have_accepted) # check if you have accepted already (for multi claims)
    print(claimed_ids)
    print(claimeds_who_have_accepted)
    '''

    # check if the interactor can respond
    '''
    print(user_id)
    print(claimed)
    print(master)
    print(claimed_ids)
    '''
    if (user_id != claimed and user_id != master) and (user_id not in claimed_ids): # check if you are the one being claimed or the master
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim decline failed because the user has no say in it, L.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "You have no say in this."})
        return
    
    else: # all passed
        # send response
        http_api.edit_interaction_response_original(interaction_token, {"content": "You have declined this claim."})
        http_api.send_message(data["channel_id"], payload)
        
        # update original message
        message_content = f"~~<@{claimed}>, do you consent to being claimed by <@{master}>?~~ This claim has been declined."
        payload = {
            "content": message_content,
            "components": [
            {
                "type": 1,
                "components": [
                    {
                        "type": 2,
                        "label": "Yes!",
                        "style": 3,
                        "custom_id": "thiscustomiddoesntmatterbecausethisbuttonisdisabledanyway1",
                        "disabled": True
                    },
                    {
                        "type": 2,
                        "label": "No.",
                        "style": 4,
                        "custom_id": "thiscustomiddoesntmatterbecausethisbuttonisdisabledanyway2",
                        "disabled": True
                    }
                ]
            }
        ]
        }
        http_api.edit_followup_message(interaction_token, payload, data["message"]["id"])
    await claims_waitlist_api.delete_waitlist_claim({"master": master, "claimed": claimed}, guild_id)
    asyncio.create_task(log(logger, ContextType.INTERACTION, "Claim declined successfully. Better luck next time, o7", HandlerType.INTERACTION_CLAIMING))

# 2: unclaiming
# unclaim request
async def unclaim(data, interaction_id, interaction_token, options, logger=None):
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    other_user_in_claim = options["member"]
    sender = data["member"]["user"]["id"]
    
    # get guild info; this will be cached and used for future reference to cut down on database queries
    guild_id = data["guild_id"]
    guild_info = await guilds_api.get_guild(guild_id)
    guild_claimed_role_id = guild_info.roles.claimed
    guild_master_role_id = guild_info.roles.master

    # is the feature enabled
    enabled = guild_info.features.claiming
    if enabled == False:
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Leave claim failed because the feature is disabled in guild {guild_id}.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Claiming is not enabled in this server."})
        return

    # unclaiming self
    if other_user_in_claim == sender:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Leave claim failed because the user tried to leave themselves.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "You cannot leave yourself... wait, what are you trying to imply?"})
        return

    claim = await claims_api.get_claim(guild_id, mode="known_users_not_positions", users=[sender, other_user_in_claim], logger=logger)
    
    # are u even claimed to the user
    if claim == None:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Leave claim failed because the user is trying to leave someone they are not with.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "You're not with that user."})
        return

    # all checks passed
    payload = {
        "content": f"You're currently with <@{other_user_in_claim}>, are you sure you want to leave this claim?",
        "flags": 64,
        "components": [
        {
            "type": 1,
            "components": [
                {
                    "type": 2,
                    "label": "Yes!",
                    "style": 3,
                    "custom_id": f"op2_accept_id{other_user_in_claim}"
                },
                {
                    "type": 2,
                    "label": "No.",
                    "style": 4,
                    "custom_id": f"op2_decline_id{other_user_in_claim}"
                }
            ]
        }
        ]
    }
    http_api.edit_interaction_response_original(interaction_token, payload)
    asyncio.create_task(log(logger, ContextType.INTERACTION, "Leave claim request created successfully.", HandlerType.INTERACTION_CLAIMING))

# 2: unclaim yes
async def unclaim_yes(data, interaction_id, interaction_token, other_user_in_claim, logger=None):
    # respond
    await common.basic_deferred_update_message(interaction_id, interaction_token)

    # get ids
    guild_id = data["guild_id"]
    guild_info = await guilds_api.get_guild(guild_id)
    guild_claimed_role_id = guild_info.roles.claimed
    guild_master_role_id = guild_info.roles.master
    guild_claim_list_channel_id = guild_info.channels.claim_list

    sender = data["member"]["user"]["id"]

    # is the feature enabled
    enabled = guild_info.features.claiming
    if enabled == False:
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Leave claim accept failed because the feature is disabled in guild {guild_id}.", HandlerType.INTERACTION_CLAIMING))
        http_api.create_followup_message(interaction_token, {"content": "Claiming is not enabled in this server.", "flags": 64})
        return
    
    claim = await claims_api.get_claim(guild_id, mode="known_users_not_positions", users=[sender, other_user_in_claim], logger=logger)

    # are u even claimed to the user
    if claim == None:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Leave claim accept failed because the user is trying to leave someone they are not with.", HandlerType.INTERACTION_CLAIMING))
        http_api.create_followup_message(interaction_token, {"content": "You're not with that user.", "flags": 64})
        return

    # check that you can respond
    if claim.users.get_user(sender) == None:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Leave claim accept failed because the user has no say in it, L.", HandlerType.INTERACTION_CLAIMING))
        http_api.create_followup_message(interaction_token, {"content": "You have no say in this.", "flags": 64})
        return
    
    # all checks passed
    await claim.delete(logger=logger)

    message_data = {
        "content": f"~~{data['message']['content']}~~\nYou have left your claim with <@{other_user_in_claim}>.",
        "components": [{
            "type": 1,
            "components": [{
                "type": 2,
                "label": "Yes!",
                "style": 3,
                "custom_id": f"op2_accept_id{other_user_in_claim}",
                "disabled": True
            },
            {
                "type": 2,
                "label": "No.",
                "style": 4,
                "custom_id": f"op2_decline_id{other_user_in_claim}",
                "disabled": True
            }]
        }]
    }
    http_api.edit_interaction_response_original(interaction_token, message_data)

# 2: unclaim no
async def unclaim_no(data, interaction_id, interaction_token, other_user_in_claim, logger=None):
    asyncio.create_task(log(logger, ContextType.INTERACTION, "Leave claim declined successfully.", HandlerType.INTERACTION_CLAIMING))
    payload = {
        "type": 7,
        "data": {
            "content": f"~~{data['message']['content']}~~\nLeave cancelled. You will stay in your claim with <@{other_user_in_claim}>.",
            "components": [{
                "type": 1,
                "components": [{
                    "type": 2,
                    "label": "Yes!",
                    "style": 3,
                    "custom_id": f"op2_accept_id{other_user_in_claim}",
                    "disabled": True
                },
                {
                    "type": 2,
                    "label": "No.",
                    "style": 4,
                    "custom_id": f"op2_decline_id{other_user_in_claim}",
                    "disabled": True
                }]
            }]
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# editing claims
async def edit_claim(data, interaction_id, interaction_token, options, logger=None):
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    other_user_in_claim = options["member"]
    sender = data["member"]["user"]["id"]
    guild_id = data["guild_id"]

    try:
        note = options["note"]
    except KeyError:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Edit claim failed because the user gave nothing to edit.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Uh, you haven't given me anything to edit...? What do you want from me? To read your mind?"})
        return

    # database
    guild_info = await guilds_api.get_guild(guild_id)

    # sanity checks
    # is the feature enabled
    enabled = guild_info.features.claiming
    if enabled == False:
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Edit claim failed because the feature is disabled in guild {guild_id}.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Claiming is not enabled in this server."})
        return

    claim = await claims_api.get_claim(guild_id, mode="known_users_not_positions", users=[sender, other_user_in_claim], logger=logger)

    # are u even claimed to the user
    if claim == None:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Edit claim failed because the user is trying to leave someone they are not with.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "You're not with that user."})
        return

    # cry about it subs
    if sender != claim.users.master:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Edit claim failed because the user is a sub.", HandlerType.INTERACTION_CLAIMING))
        http_api.edit_interaction_response_original(interaction_token, {"content": "Please ask your dom to edit claims... what're you looking at me like that for? You agreed to this."})
        return

    if note:
        # for deleting notes
        if note.lower() == "delete":
            note = ""
        
        # to-do: not assume that the channel didn't change
        guild_claim_list_channel_id = guild_info.channels.claim_list

        # to-do: fix this for if the claim list channel is changed
        if note == "":
            message_data = {
                "content": f"<@{claim.users.claimed}> is owned by <@{claim.users.master}>.",
                "allowed_mentions": {
                    "users": [claim.users.master, claim.users.claimed]
                },
                "flags": 4
            }
        else:
            message_data = {
                "content": f"<@{claim.users.claimed}> is owned by <@{claim.users.master}>.\n\n**__Note from the master:__**\n{note}",
                "allowed_mentions": {
                    "users": [claim.users.master, claim.users.claimed]
                },
                "flags": 4
            }

        http_api.edit_message(guild_claim_list_channel_id, claim.message_id, message_data)

        # db
        await claims_api.update_claim({"master": claim.users.master, "claimed": claim.users.claimed}, guild_id, {"note": note}, logger=logger)

        http_api.edit_interaction_response_original(interaction_token, {"content": f"Your claim has been edited! Check it out at https://discord.com/channels/{guild_id}/{guild_claim_list_channel_id}/{claim.message_id} !"})
        asyncio.create_task(log(logger, ContextType.INTERACTION, "The claim's note was changed successfully.", HandlerType.INTERACTION_CLAIMING))
