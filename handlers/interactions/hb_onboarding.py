# custom libraries
import http_api
import database
from logger import *
from handlers.interactions import hb_common as common

# not custom libraries
import json
import regex as re
import asyncio

# I couldn't figure out how to get this niche feature to work well when made public
# because how do we even make settings in discord for this?
# so it will likely stay largely unused or private
# the feature itself is MOSTLY fully functional albeit the code is messy

# 4: makeshift onboarding
# 4: roles message
async def makeshift_onboarding_message(data, interaction_id, interaction_token, onboarding_data, logger=None):
    # empty list of components
    components_list = []
    i = 0

    # currently let's limit it to 4 questions. later I'll make it send in seperate messages or just edit the main message
    if onboarding_data["questions_count"] > 4: # 4 select menus + the button = 5 action rows which is the max that discord allows
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Onboarding could not be loaded because there are more than 4 questions.", HandlerType.INTERACTION_ONBOARDING))
        await common.basic_ephemeral_response(interaction_id, interaction_token, "I couldn't load the onboarding! There are more than 4 questions, and I can't handle that, because of Discord's limitations. Contact a server admin to fix the onboarding.")
        return
    
    # iterate through the questions to populate the component
    for question in onboarding_data["questions"]:
        # populate the options
        options_list = []
        for option in question["options"]:
            option_data = {
                "label": option["label"],
                "value": f"op4_q{i}_rid{option['role_id']}"
            }
            if "emoji" in option: option_data["emoji"] = option["emoji"]
            options_list.append(option_data)

        # placeholder text
        # add a little required indicator
        if question["required"] == True:
            placeholder = f"{question['placeholder']} (required)"
        else:
            placeholder = question["placeholder"]

        # select menu component
        component_data = {
            "type": 3,
            "custom_id": f"op4_select_q{i}",
            "placeholder": placeholder,
            "options": options_list,
            "min_values": 1,
            "max_values": 1
        }

        # action row to populate
        action_row_component = {
            "type": 1,
            "components": []
        }

        action_row_component["components"].append(component_data)

        components_list.append(action_row_component)
        i += 1

    # another action row with the confirm button
    confirm_button_component = {
        "type": 1,
        "components": [
            {
                "type": 2,
                "label": "Confirm",
                "style": 3,
                "custom_id": f"op4_finish_frid{onboarding_data['finish_role_id']}",
                "emoji": {"name": "✅"}
            }
        ]
    }

    components_list.append(confirm_button_component)

    # message data
    payload = {
        "type": 4,
        "data": {
            "flags": 64,
            "content": f"Thanks for agreeing to the rules! Please choose your roles to continue.\nYou can remove a role by selecting it again.\nA Discord limitation out of my control is that you may not get the role when submitting your choices too quickly. Please wait for the confirmation message before submitting another option.\n\nWhen you're done, press Confirm to enter the server! You can check your profile here: <@{data['member']['user']['id']}>",
            "components": components_list,
            "allowed_mentions": {"parse": []} # suppress mentions because it looks neater
        }
    }
    #print(payload)
    http_api.create_interaction_response(interaction_id, interaction_token, payload)
    asyncio.create_task(log(logger, ContextType.INTERACTION, "Onboarding message created successfully.", HandlerType.INTERACTION_ONBOARDING))
    #http_api.add_role(data["guild_id"], data["member"]["user"]["id"], member_role, "Accepted rules")

# 4: choosing roles
async def makeshift_onboarding_role_select(data, interaction_id, interaction_token, logger=None):
    # respond
    await common.basic_deferred_update_message(interaction_id, interaction_token)

    # get role ID from custom id
    numbers_after_rid = re.search(r'rid(\d+)', data["data"]["values"][0])
    role_id = numbers_after_rid.group(1)

    # check if the role is in your roles already. if not, add the role. if it is, remove it
    base_content = f"Thanks for agreeing to the rules! Please choose your roles to continue.\nYou can remove a role by selecting it again.\nA Discord limitation out of my control is that you may not get the role when submitting your choices too quickly. Please wait for the confirmation message before submitting another option.\n\nWhen you're done, press Confirm to enter the server! You can check your profile here: <@{data['member']['user']['id']}>"
    
    if role_id not in data["member"]["roles"]:
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Role {role_id} added.", HandlerType.INTERACTION_ONBOARDING))
        http_api.add_role(data["guild_id"], data["member"]["user"]["id"], role_id, "Submitted onboarding question")

        payload = {
            "content": base_content + f"\n\n**<@&{role_id}> added.**",
            "allowed_mentions": {"parse": []} # suppress mentions because it looks neater
        }
    
    # remove the role if you have it already
    else: 
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Role {role_id} removed.", HandlerType.INTERACTION_ONBOARDING))
        http_api.remove_role(data["guild_id"], data["member"]["user"]["id"], role_id, "Submitted onboarding question")

        payload = {
            "content": base_content + f"\n\n**<@&{role_id}> removed.**",
            "allowed_mentions": {"parse": []} # suppress mentions because it looks neater in the client
        }
    
    # confirmation response
    http_api.edit_interaction_response_original(interaction_token, payload)

# 4: finishing
async def finish_makeshift_onboarding(data, interaction_id, interaction_token, logger=None):
    # respond
    await common.basic_deferred_update_message(interaction_id, interaction_token)

    # get onboarding data
    onboarding_data = database.get_onboarding(data["guild_id"])

    if onboarding_data == "Guild onboarding not found":
        asyncio.create_task(log(logger, ContextType.INTERACTION, f"Onboarding finish failed because guild {data['guild_id']} no longer has the onboarding data.", HandlerType.INTERACTION_ONBOARDING))
        http_api.create_followup_message(interaction_token, {"content": "Something broke and I don't have the onboarding data any more! This is most likely my fault, or a server admin disabled the feature.", "flags": 64})
        return

    # check required questions
    for question in onboarding_data["questions"]:
        if question["required"] != True:
            continue
        else:
            for option in question["options"]:
                # check every role in every option
                role_set = False
                if option["role_id"] in data["member"]["roles"]:
                    #print("Member had {0}".format(option["role_id"]))
                    role_set = True
                    break
                #else:
                    #print("Member did not have {0}".format(option["role_id"]))
        #print(role_set)
        if role_set == False:
            asyncio.create_task(log(logger, ContextType.INTERACTION, "Onboarding finish failed because the user has missed some required questions.", HandlerType.INTERACTION_ONBOARDING))
            http_api.create_followup_message(interaction_token, {"content": "You have missed one or more required options.", "flags": 64})
            return

    # check if u have submitted the form already
    numbers_after_frid = re.search(r'frid(\d+)', data["data"]["custom_id"])
    finish_role_id = numbers_after_frid.group(1)

    # button is disabled so might remove this later
    if finish_role_id in data["member"]["roles"]:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Onboarding finish failed because the user has already finished onboarding.", HandlerType.INTERACTION_ONBOARDING))
        http_api.create_followup_message(interaction_token, {"content": "You have already submitted this form.", "flags": 64})
        return
    else:
        # all checks passed, assign the role
        finish_role_id = onboarding_data["finish_role_id"]
        http_api.add_role(data["guild_id"], data["member"]["user"]["id"], finish_role_id, "Finished onboarding")
    
        # confirmation response

        # disable all components
        components = data["message"]["components"]
        for action_row in components:
            action_row_components = action_row["components"]

            for component in action_row_components:
                component["disabled"] = True

        payload = {
            "content": "Welcome in!",
            "components": components
        }
        http_api.edit_interaction_response_original(interaction_token, payload)
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Onboarding finished successfully.", HandlerType.INTERACTION_ONBOARDING))
