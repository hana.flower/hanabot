# custom libraries
import http_api
from logger import *
from handlers.interactions import hb_agedob as agedob
from handlers.interactions import hb_common as common

# not custom libraries
import json
import asyncio

# 3: rules
# rules "agree"
# ok i really need to change these names to be more accurate
async def rules_agree(data, interaction_id, interaction_token, guild_info, logger=None):
    asyncio.create_task(log(logger, ContextType.INTERACTION, "User will be kicked for not agreeing to the rules.", HandlerType.INTERACTION_RULES))
    reinvite_url = guild_info.reinvite_url
    if reinvite_url != "":
        payload = {"content": f"You didn't agree to the rules! Rejoin at {reinvite_url} and try again."}
    else:
        payload = {"content": f"You didn't agree to the rules!"}
    http_api.send_message(json.loads(http_api.create_dm(data["member"]["user"]["id"]).text)["id"], payload)
    http_api.remove_guild_member(data["guild_id"], data["member"]["user"]["id"], "Didn't agree to rules")

# 3: rules "dis"agree
async def rules_disagree(data, interaction_id, interaction_token, guild_info, logger=None):
    if guild_info.features.memberagecheck == True:
        # skip privacy warning
        if data["data"]["custom_id"] == "op3_disagree_skipprivacy":
            asyncio.create_task(log(logger, ContextType.INTERACTION, "User has agreed to the rules and will continue to the age check.", HandlerType.INTERACTION_RULES))
            await agedob.age_and_dob_modal(data, interaction_id, interaction_token)
        # show privacy warning
        else:
            asyncio.create_task(log(logger, ContextType.INTERACTION, "User has agreed to the rules and will be shown the privacy warning for the age check.", HandlerType.INTERACTION_RULES))
            payload = {
                "type": 4,
                "data": {
                    "content": "Thanks for agreeing to the server rules!\n\nI should let you know that the next screen will ask for some extra information in order to verify you. **This information is NOT stored by Hanabot. It is only sent to the server staff.**\n\nPress the button below to continue!",
                    "components": [{
                        "type": 1,
                        "components": [{
                            "type": 2,
                            "style": 3,
                            "label": "Continue",
                            "custom_id": "op3_disagree_skipprivacy"
                        }]
                    }],
                    "flags": 64
                }
            }
            http_api.create_interaction_response(interaction_id, interaction_token, payload)
    
    else:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "User has agreed to the rules and will be let in the server.", HandlerType.INTERACTION_RULES))
        http_api.add_role(data["guild_id"], data["member"]["user"]["id"], guild_info.roles.member, "Accepted rules")
        await common.basic_ephemeral_response(interaction_id, interaction_token, "Welcome in!")
