# file reserved for common responses
import http_api

# basic ephemeral response; used to cut down on excess and unnecessary lines
# this responds INSTANTLY
async def basic_ephemeral_response(interaction_id, interaction_token, content):
    payload = {
        "type": 4,
        "data": {
            "content": content,
            "flags": 64
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, payload)

# another response that is a type 5 waiting
# the message can be edited with this code:
#edited_data = {"content": "Some edited message data."}
#http_api.edit_interaction_response_original(interaction_token, edited_data)
# so creating a function for that is useless
async def basic_ephemeral_thinking(interaction_id, interaction_token):
    interaction_data = {
        "type": 5,
        "data": {
            "flags": 64
        }
    }
    http_api.create_interaction_response(interaction_id, interaction_token, interaction_data)

# deferred update message
# edit original response later with the message data
# same as editing basic ephemeral thinking
async def basic_deferred_update_message(interaction_id, interaction_token):
    http_api.create_interaction_response(interaction_id, interaction_token, {"type": 6})
