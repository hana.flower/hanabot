# custom libraries
import http_api
import tools
import database
from logger import *
from handlers.interactions import hb_common as common

from api import guilds as guilds_api
from api import members as members_api

# not custom libraries
import json
import regex as re
from math import ceil
from math import floor
from time import time
import asyncio

# somewhat redundant feature but it was a good exercise for me
# feel free to disable in your self-host

# 5: warn
async def warn(data, interaction_id, interaction_token, options, logger=None):
    users = data["data"]["resolved"]["users"]
    for user_key in users:
        warned_user = users[user_key]

    warned_user_id = warned_user["id"]
    warned_user_username = warned_user["username"]
    warned_user_discriminator = warned_user["discriminator"]

    try:
        # check permissions
        if tools.check_permission(data["member"]["permissions"], 3) == False:
            asyncio.create_task(log(logger, ContextType.INTERACTION, "Warning failed because the user does not have the Ban Members permission.", HandlerType.INTERACTION_WARNINGS))
            await common.basic_ephemeral_response(interaction_id, interaction_token, "You must have the Ban Members permission to use this command.")
            return
    except KeyError:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Warning failed because the user... what the fuck...", HandlerType.INTERACTION_WARNINGS))
        await common.basic_ephemeral_response(interaction_id, interaction_token, "You're not in the server...?")
        return

    # check role hierarchy
    # jk you just can't warn someone with the ban members permission too because role hierarchy would be too hard
    try:
        if tools.check_permission(data["data"]["resolved"]["members"][warned_user_id]["permissions"], 3) == True:
            asyncio.create_task(log(logger, ContextType.INTERACTION, "Warning failed because the warned user has the Ban Members permission.", HandlerType.INTERACTION_WARNINGS))
            await common.basic_ephemeral_response(interaction_id, interaction_token, "You cannot warn someone with the Ban Members permission.")
            return
    except KeyError:
        asyncio.create_task(log(logger, ContextType.INTERACTION, "Warning failed because the warned user is not in the guild.", HandlerType.INTERACTION_WARNINGS))
        await common.basic_ephemeral_response(interaction_id, interaction_token, "That user is not in the server.")
        return

    # get guild and member info from DB
    guild_id = data["guild_id"]
    guild_info = await guilds_api.get_guild(guild_id)
    warn_list_id = guild_info.channels.warn_list
    warn_limit = guild_info.warn_limit
    
    member = await members_api.get_member(warned_user_id, guild_id)
    warns = member.warns_to_dict_list()
    warns_count = len(warns)

    # get reason
    reason = options["reason"]

    # change ephemeral response, this is the base message
    message_content = "User has been warned."

    has_ban_perm = tools.check_permission(data["app_permissions"], 3)
    
    result = 0 # placeholder if there was no ban

    # ban if too many warns
    if (warns_count + 1) >= warn_limit:
        # check permission
        if has_ban_perm == False:
            asyncio.create_task(log(logger, ContextType.INTERACTION, "Warning succeeded, but the user could not be banned due to insufficient permissions.", HandlerType.INTERACTION_WARNINGS))
            message_content = "User has been warned, but I do not have the Ban Members permission, so I cannot ban the member."
        
        ban_status = http_api.create_guild_ban(guild_id, warned_user_id, "Too many warns")
        result = ban_status.status_code

        # change response based on status code
        if result == 204:
            asyncio.create_task(log(logger, ContextType.INTERACTION, "Warning succeeded, and the user has been banned.", HandlerType.INTERACTION_WARNINGS))
            message_content = "User has been warned and banned."

        elif result == 403:
            # check for error code
            error_code = json.loads(ban_status.text)["code"]
            if error_code == 50013: # to-do: rely on the interaction's information
                asyncio.create_task(log(logger, ContextType.INTERACTION, "Warning succeeded, but the user could not be banned due to the role hierarchy.", HandlerType.INTERACTION_WARNINGS))
                message_content = "User has been warned, but the ban failed because I do not have permission to ban this member. They most likely have a higher role than me, or I do not have the Ban Members permission."

    # "0" means "send it in the same channel"
    if warn_list_id == "0":
        warn_list_id = data["channel_id"]

    # i don't think notifying of where the ban was broadcasted is important enough to keep
        
    await common.basic_ephemeral_response(interaction_id, interaction_token, message_content)

    # send a message
    message_data = {
        "content": f"<@{warned_user_id}> You have been warned for: \n`{reason}`\nThis is warning number {warns_count + 1} out of {warn_limit}.",
        "allowed_mentions": {
            "users": [warned_user_id]
        }
    }
    try:
        message_id = json.loads(http_api.send_message(warn_list_id, message_data).text).get("id") # to-do: implement functionality if it doesn't get an ID returned (403 forbidden)
    except KeyError:
        message_id = "0"

    warns_list_update_dict = {
        "reason": reason,
        "timestamp": str(floor(time())),
        "message_id": message_id,
        "warn_id": data["id"] # the interaction ID is used as the ID for the warn
    }

    warns.append(warns_list_update_dict)

    # database
    update_dict = {
        "warns": warns
    }

    member.update(update_dict)
    asyncio.create_task(log(logger, ContextType.INTERACTION, "Warning succeeded.", HandlerType.INTERACTION_WARNINGS))

# to-do: don't be too lazy to log the rest of warnings
# 5: warn list
async def warn_list_initial(data, interaction_id, interaction_token, options):
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    # db
    guild_id = data["guild_id"]
    member = await members_api.get_member(options["member"], guild_id)

    guild_info = await guilds_api.get_guild(guild_id)
    
    warn_limit = guild_info.warn_limit
    warns_count = len(member.warns)
    page = 1 # slash command will always show page 1
    pages = ceil(warns_count / 5)
    
    # define fields list and components
    fields = []

    # generate the embed field
    def generate_embed_field(member, i):
        warn = member.warns[i]

        embed_field = {
            "name": f"#{i + 1}: <t:{int(warn.timestamp)}>",
            "value": f"**Reason:** {warn.reason}\n**Warn ID:** {warn.warn_id}"
        }

        return embed_field
    
    # generate the embed
    for i in range(0, warns_count):
        if i >= 5: break # 5 warns per page
        fields.append(generate_embed_field(member, i))

    embed = {
        "title": "Warns",
        "description": f"List of **{warns_count}** warns for <@{options['member']}>",
        "color": 15502719,
        "footer": {"text": f"The user will be banned after {warn_limit} warns."},
        "fields": fields
    }

    # generate an action row
    def generate_action_row(member):
        # it is fine to hardcode some of this since it is just the slash command response
        base_action_row = {
            "type": 1,
            "components": [{
                "type": 2,
                "label": "",
                "style": 2,
                "custom_id": f"op5_list_mid{options['member']}_page{page - 1}",
                "disabled": not(page > 1),
                "emoji": {"name": "⬅️"}
            },
            {
                "type": 2,
                "label": f"Page {page} of {pages}",
                "style": 1,
                "custom_id": "none",
                "disabled": True
            },
            {
                "type": 2,
                "label": "",
                "style": 2,
                "custom_id": f"op5_list_mid{options['member']}_page{page + 1}",
                "disabled": not(page <= pages),
                "emoji": {"name": "➡️"}
            }]
        }

        return base_action_row

    components = generate_action_row(member)

    message_data = {
        "content": "",
        "embeds": [embed], # generated embeds, only one currently
        "components": [components] # generated list of components
    }

    http_api.edit_interaction_response_original(interaction_token, message_data)

# 5: warn list button
async def warn_list_button(data, interaction_id, interaction_token):
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    numbers_after_mid = re.search(r'mid(\d+)', data["data"]["custom_id"])
    member_id = numbers_after_mid.group(1)
    numbers_after_mid = re.search(r'page(\d+)', data["data"]["custom_id"])
    page = int(numbers_after_mid.group(1))

    # db
    guild_id = data["guild_id"]
    member = await members_api.get_member(member_id, guild_id)

    guild_info = await guilds_api.get_guild(guild_id)
    
    warn_limit = guild_info.warn_limit
    warns_count = len(member.warns)

    pages = ceil(warns_count / 5)
    
    # define fields list and components
    fields = []

    # generate the embed field
    def generate_embed_field(member, i):
        warn = member.warns[i]

        embed_field = {
            "name": f"#{i + 1}: <t:{int(warn.timestamp)}>",
            "value": f"**Reason:** {warn.reason}\n**Warn ID:** {warn.warn_id}"
        }

        return embed_field
    
    # generate the embed
    for i in range(0, warns_count - (page - 1) * 5):
        if i >= 5: break # 5 warns per page
        fields.append(generate_embed_field(member, i + ((page - 1) * 5)))

    embed = {
        "title": "Warns",
        "description": f"List of **{warns_count}** warns for <@{member_id}>",
        "color": 15502719,
        "footer": {"text": f"The user will be banned after {warn_limit} warns."},
        "fields": fields
    }

    # generate an action row
    def generate_action_row(member):
        print(page)
        print(pages)
        # it is fine to hardcode some of this since it is just the slash command response
        base_action_row = {
            "type": 1,
            "components": [{
                "type": 2,
                "label": "",
                "style": 2,
                "custom_id": f"op5_list_mid{member_id}_page{page - 1}",
                "disabled": page <= 1,
                "emoji": {"name": "⬅️"}
            },
            {
                "type": 2,
                "label": f"Page {page} of {pages}",
                "style": 1,
                "custom_id": "none",
                "disabled": True
            },
            {
                "type": 2,
                "label": "",
                "style": 2,
                "custom_id": f"op5_list_mid{member_id}_page{page + 1}",
                "disabled": not(page < pages),
                "emoji": {"name": "➡️"}
            }]
        }

        return base_action_row

    components = generate_action_row(member)

    message_data = {
        "content": "",
        "embeds": [embed], # generated embeds, only one currently
        "components": [components] # generated list of components
    }

    http_api.edit_interaction_response_original(interaction_token, message_data)

# 5: warn delete
async def warn_delete(data, interaction_id, interaction_token, options):
    # respond
    await common.basic_ephemeral_thinking(interaction_id, interaction_token)

    # db
    guild_id = data["guild_id"]
    guild_info = await guilds_api.get_guild(guild_id)
    member_id = options["member"]
    member = await members_api.get_member(member_id, guild_id)

    warns = member.warns_to_dict_list()
    warns_count = len(warns)
    index = options["number"]

    # sanity check
    if warns_count < index or index <= 0:
        http_api.edit_interaction_response_original(interaction_token, {"content": "Invalid warn number. You can check the member's warns with `/warnings list`."})
        return

    # delete the warn
    message_id = warns.pop(index - 1)["message_id"] # pop method returns the removed value

    update_dict = {
        "warns": warns
    }
    member.update(update_dict)

    # the message just won't be deleted if the channel is not explicitly set
    if guild_info.channels.warn_list != "0":
        http_api.delete_message(guild_info.channels.warn_list, message_id, "Removing a warn")

    http_api.edit_interaction_response_original(interaction_token, {"content": f"Warn {index} for <@{member_id}> deleted.", "allowed_mentions": {"parse": []}})
